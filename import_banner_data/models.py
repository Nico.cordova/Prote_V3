# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

@python_2_unicode_compatible
class Syaproa(models.Model):
    FACULTY_CHOICES = (('INGE', 'Ingeniería'),
                       )
    id_user = models.IntegerField(null=True)
    facultad  = models.CharField(max_length=100,choices=FACULTY_CHOICES)
    campus = models.CharField(max_length=100)
    unidad_academica = models.CharField(max_length=100)
    periodo = models.IntegerField()
    materia = models.CharField(max_length=100)
    curso = models.IntegerField()
    seccion = models.IntegerField()
    jornada = models.CharField(max_length=100)
    nrc = models.IntegerField()
    listas_cruzadas = models.CharField(max_length=100)
    nrc_ligados = models.CharField(max_length=100)
    id_listas_cruzadas = models.CharField(max_length=100)
    calificable = models.CharField(max_length=100)
    minor = models.CharField(max_length=100)
    rut_Profesor = models.CharField(max_length=20)
    nombre_profesor = models.CharField(max_length=100)
    nombre_asignatura = models.CharField(max_length=100)
    horas_a_pagar = models.IntegerField()
    tipo_actividad = models.CharField(max_length=100)
    modalidad = models.CharField(max_length=100)
    horario = models.CharField(max_length=100)
    vacantes = models.IntegerField()
    inscritos = models.IntegerField()
    edificio = models.CharField(max_length=100)
    sala = models.CharField(max_length=100)
    capacidad_sala = models.CharField(max_length=100)
    restric_cod_programa = models.CharField(max_length=1000)
    restric_desc_programa = models.CharField(max_length=1000)
    restricciones_campus = models.CharField(max_length=1000)
    nivel_del_curso = models.CharField(max_length=100)
    unidad_que_paga = models.CharField(max_length=100)
    semestre_que_paga = models.IntegerField()
    nivel_del_programa = models.CharField(max_length=100)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
    def __str__(self):
        return self

@python_2_unicode_compatible
class Syainsc(models.Model):
    id_user = models.IntegerField(null=True,blank=True)
    n = models.IntegerField()
    campus = models.CharField(max_length=100)
    escuela = models.CharField(max_length=100)
    programa = models.CharField(max_length=100)
    nivel = models.CharField(max_length=100)
    rut = models.CharField(max_length=20)
    nombre = models.CharField(max_length=100)
    estatus = models.CharField(max_length=100)
    tipo_alumno = models.CharField(max_length=100)
    via_de_ingreso = models.CharField(max_length=100)
    ticket_inscripcion = models.IntegerField()
    fecha_inscripcion = models.DateTimeField(null=True,blank=True)
    email = models.CharField(max_length=100)
    fono_celular = models.CharField(max_length=100)
    fono_particular = models.CharField(max_length=100)
    matriculado = models.CharField(max_length=100)
    deuda_finanzas = models.CharField(max_length=100)
    cantidad_nrc_inscritos_inb = models.IntegerField()
    cantidad_nrc_Inscritos_web = models.IntegerField()
    total_nrc_inscritos = models.IntegerField()
    total_de_asignaturas_proyectadas_mas_probable = models.IntegerField()
    total_de_asignaturas_inscritas = models.IntegerField()
    detalle_nrc_inscritos_web_rw = models.CharField(max_length=1000)
    detalle_nrc_inscritos_inb_re = models.CharField(max_length=1000)
    usuario = models.CharField(max_length=100)

    def __str__(self):
        return self

@python_2_unicode_compatible
class Syastdn(models.Model):
    id_user = models.IntegerField(null=True)
    n = models.IntegerField()
    rut = models.CharField(max_length=20)
    nombre_alumno = models.CharField(max_length=200)
    fecha_nacimiento = models.DateTimeField()
    campus = models.CharField(max_length=100)
    unidad_academica = models.CharField(max_length=100)
    cod_programa = models.CharField(max_length=100)
    programa = models.CharField(max_length=100)
    especialidad = models.CharField(max_length=100)
    mencion = models.CharField(max_length=100)
    estado = models.CharField(max_length=100)
    matriculado = models.CharField(max_length=2)
    bloqueo_sin_matricula_bi = models.CharField(max_length=2)
    retencion_financiera = models.CharField(max_length=2)
    periodo_ultima_actualizacion = models.IntegerField()
    periodo_ctlg = models.IntegerField()
    periodo_admision = models.IntegerField()
    tipo_alumno = models.CharField(max_length=100)
    ppa  = models.DecimalField(max_digits=5, decimal_places=1)
    pga_programa = models.DecimalField(max_digits=5, decimal_places=1)
    pga_historico = models.DecimalField(max_digits=5, decimal_places=1)
    nro_asignaturas_aprobadas_programa = models.IntegerField()
    nro_creditos_aprobados_programa = models.IntegerField()
    via_titulacion = models.CharField(max_length=100)
    domicilio = models.CharField(max_length=100)
    fonos_celular = models.CharField(max_length=100)
    fonos_particular = models.CharField(max_length=100)
    correo_unab = models.CharField(max_length=100)
    correo_personal = models.CharField(max_length=100)

    def __str__(self):
        return self

