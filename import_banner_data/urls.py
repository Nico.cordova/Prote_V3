from django.conf.urls import url
from django.contrib.auth import views as auth_views
from import_banner_data import views


urlpatterns = [
    url(r'^$', views.load_data, name='load_data'),
]
