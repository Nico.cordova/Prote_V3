from django import forms
from django.utils.translation import ugettext as _
from import_banner_data.models import Syaproa, Syainsc, Syastdn



class UploadBannerForm(forms.Form):
    syaproa = forms.FileField(label=_('SYAPROA'))
    syainsc = forms.FileField(label=_('SYAINSC'))
    syatdn = forms.FileField(label=_('SYATDN'))