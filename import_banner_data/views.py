from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView
from core.models import Student, Teacher, Project, Carreer, Campus, Academic_record, Faculty, Seat
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.datastructures import MultiValueDictKeyError
from import_banner_data.forms import UploadBannerForm
from django.forms import ModelForm
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.core import serializers
from django.http import JsonResponse
import json
from dashboard.login.views import login_needed




@login_needed
def load_data(request):
    template_name = 'banner/load_data.html'
    form = UploadBannerForm(request.POST, request.FILES)
    return render(request, template_name, {'form': form })
