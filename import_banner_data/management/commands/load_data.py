# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from import_banner_data.models import Syaproa, Syainsc, Syastdn
from django.utils.crypto import get_random_string
from core.models import User, Student, Seat, Carreer, Faculty, Academic_record, Campus, Course, Personal_record,Carreer_code
from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.core.mail import send_mail
from django.db.models import Sum

import unicodedata
import string

def remove_accents(data):
    return ''.join(x for x in unicodedata.normalize('NFKD', data) if x in string.ascii_letters).lower()

class Command(BaseCommand):
    help = 'Load Student Data From Banner'      

    def handle(self, *args, **options):  

        syaproa = Syaproa.objects.filter(
                                    Q(nombre_asignatura="PROYECTO DE TÍTULO I") | 
                                    Q(nombre_asignatura="PROYECTO DE TÍTULO II") | 
                                    Q(nombre_asignatura="SEMINARIO DE TÍTULO")).annotate(Sum('nrc'))
        for data_syaproa in syaproa:

            syainc = Syainsc.objects.filter(
                    Q(detalle_nrc_inscritos_web_rw__icontains=data_syaproa.nrc) | 
                    Q(detalle_nrc_inscritos_inb_re__icontains=data_syaproa.nrc))

            for data_syainc in syainc:

                syatdn = Syastdn.objects.filter(Q(rut=data_syainc.rut))
                
                for data_syatdn in syatdn :

                    rut = data_syainc.rut.replace('-', '').replace('.', '')

                    first_last_name = data_syatdn.nombre_alumno.split(",")

                    username="%s" %(rut) 
                    password=get_random_string(length=12)


                    try:
                        user = User.objects.get(username=rut)  
                        # self.stdout.write( 'Usuario Rut %s Existe' %(rut))
                        
                        # try:
                        #     carreer_code = Carreer_code.objects.get(
                        #                             code=data_syatdn.cod_programa)
                        # except Carreer_code.DoesNotExist:
                        #    self.stdout.write( 'Carrera %s no existe' %(data_syatdn.cod_programa))

                        # try:
                        #     academic_record = Academic_record.objects.get(
                        #             Q(campus=campus.pk) & 
                        #             Q(carreer_code=carreer_code))
                        # except Academic_record.DoesNotExist:
                        #     self.stdout.write( 'Registro Academico para campus %s y \
                        #             carrera %s no existe' %(campus,carreer_code))

                        # try:
                            # course = Course.objects.get(nrc=data_syaproa.nrc)  
                            # self.stdout.write( 'Curso %s ya existe' %(data_syaproa.nrc))
                        # except Course.DoesNotExist:
                            # self.stdout.write( 'Curso %s no existe rut %s' %(data_syaproa.nrc,rut))

                            # course = Course.objects.create(name=data_syaproa.nombre_asignatura,
                            #                                 code=data_syaproa.curso,
                            #                                 subject=data_syaproa.materia,
                            #                                 nrc=data_syaproa.nrc,
                            #                                 carreer_code=carreer_code)



                            

                        #validar que este activo el ramo nuevo, los alumnos pueden tener 2 carreras y 2 pdt       


                    except User.DoesNotExist:

                        try:
                            carreer_code = Carreer_code.objects.get(
                                                    code=data_syatdn.cod_programa)
                        except Carreer_code.DoesNotExist:
                           self.stdout.write( 'Carrera %s no existe' %(data_syatdn.cod_programa))


                        try:
                            campus = Campus.objects.get(name=data_syatdn.campus)  
                        except Campus.DoesNotExist:
                            self.stdout.write( 'Campus %s no Existe' %(data_syatdn.campus))

                        try:
                            course = Course.objects.get(nrc=data_syaproa.nrc)  
                            self.stdout.write( 'Curso %s ya existe' %(data_syaproa.nrc))
                        except Course.DoesNotExist:
                            course = Course.objects.create(name=data_syaproa.nombre_asignatura,
                                                            code=data_syaproa.curso,
                                                            subject=data_syaproa.materia,
                                                            nrc=data_syaproa.nrc,
                                                            carreer_code=carreer_code)
                            # course.save()

                        try:
                            academic_record = Academic_record.objects.get(
                                    Q(campus=campus.pk) & 
                                    Q(carreer_code=carreer_code))
                        except Academic_record.DoesNotExist:
                            self.stdout.write( 'Registro Academico para campus %s y \
                                    carrera %s no existe' %(campus,carreer_code))

                        personal_record = Personal_record.objects.create()
                        personal_record.academic_record.add(academic_record)
                        personal_record.course.add(course.pk)

                        user = User.objects.create_user(
                                            username = username ,
                                            email = data_syatdn.correo_unab,
                                            password=password)  

                        user.first_name = first_last_name[1]
                        user.last_name = first_last_name[0]
                        user.save()   

                        try:
                            student = Student.objects.create(
                                                    user=user,
                                                    rut=rut[0:-1],
                                                    dv=rut[-1],
                                                    pga=data_syatdn.pga_historico,
                                                    personal_record=personal_record)
                            student.save()
                            self.stdout.write( 'Estudiante rut %s \
                                                    creado con exito' %(rut) )
                        except Student.DoesNotExist:
                            self.stdout.write( 'Estudiante rut %s \
                                                    ya Existe' %(rut))
                        # content = "Estimado %s %s , se adjuntan las credenciales para poder acceder a http://http://prote.cloudtic.tech:8080 son User: %s Pass: %s Saludos.-" % (user.first_name,user.last_name,username,password)    
                        # try:
                        #     send_mail('Registro Prote', content, 'no-reply@prote.unab.cl', [data_syatdn.correo_unab], fail_silently=False)
                        #     self.stdout.write('## email enviado  ##')
                        # except Exception as e:
                        #     raise e

            #             # Syastdn.objects.filter(id_user=pk).delete()
            #             # Syainsc.objects.filter(id_user=pk).delete()
            #             # Syaproa.objects.filter(id_user=pk).delete() 
        