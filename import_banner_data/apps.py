from __future__ import unicode_literals

from django.apps import AppConfig


class ImportBannerDataConfig(AppConfig):
    name = 'import_banner_data'
