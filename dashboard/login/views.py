from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils.translation import ugettext as _
import requests
import ast
from django.contrib.auth.models import User
from core.models import *

def login_needed(function):
    def _out(request,*args,**kwargs):
        try:
            if request.session["token"] != None:
                return function(request,*args,**kwargs)
            else:
                return HttpResponseRedirect(reverse_lazy('login_user'))
        except KeyError:
            return HttpResponseRedirect(reverse_lazy('login_user'))
    return _out

def login_user(request):
    template_name = 'dashboard/login.html'
    for key in request.session.keys():
        del request.session[key]
    username = password = ''
    if request.POST:
        username = request.POST['Username']
        password = request.POST['Password']
        post_data = {'username':username,'password':password}
        response = requests.post('http://supreme.cloudtic.tech/rest-auth/login/',data=post_data)
        content = ast.literal_eval(response.content)
        if "key" in content:
            post_data2 = {'username':username}
            header = {"Content-Type":'application/json','Authorization':"Token " + content["key"]}
            response2 = requests.get('http://supreme.cloudtic.tech/rest-auth/user/',headers=header,data=post_data2)
            respuesta = ast.literal_eval(response2.content)
            request.session["name"] = respuesta["first_name"] + " " + respuesta["last_name"]
            request.session["token"] = content["key"]
            post_data3 = {'user_pk':respuesta['pk']}
            request.session["username"] = username
            header2 = {"Content-Type":'application/x-www-form-urlencoded','Authorization':"Token " + content["key"]}
            response3 = requests.post('http://supreme.cloudtic.tech/get/get_user_info/',headers=header2, data=post_data3)
            respuesta3 = ast.literal_eval(response3.content)
            respuesta3 = respuesta3['user_info']
            request.session["rut"] = respuesta3['user_rut']
            request.session["type"] = respuesta3["user_type"]
            user = User_Profile.objects.filter(rut=respuesta3["user_rut"])
            if len(user) != 1:
                user = User_Profile.objects.create(rut=respuesta3["user_rut"],
                                                  nombre=request.session["name"],
                                                  perfil_activo=request.session["type"])
                user.save()
            return HttpResponseRedirect(reverse_lazy('index'))
    return render(request, template_name, {})