from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from dashboard.login import views


  #urls student
urlpatterns = [
    url(r'^user/', include('django.contrib.auth.urls')),
    url(r'^$', views.login_user, name="login_user"),
]


# documentacion para reset password
# http://blog.contraslash.com/custom-password-recovery/


# templates para utilizar
# - reste password Static_Full_Version/forgot_password.html