from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
register = template.Library()




@register.filter(name='addcss')
def addcss(field, css):
    return field.as_widget(attrs={"class":css})

@register.filter(name='set_status_icon')
def set_status_icon(status):
    
    html = ''
    user_status = {
        'enabled' : _('Activo'),
        'disabled' : _('Inactivo'),
    }

    text_status = user_status['disabled']
    label_status = 'danger'

    if status :
        text_status = user_status['enabled']
        label_status = 'primary'
        
    return mark_safe('<span class="badge badge-%s">%s</span>' % (label_status,text_status))
        

