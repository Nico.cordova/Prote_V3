#encoding:utf-8
from django import forms
from django.contrib.auth.models import User
from core.models import *
from localflavor.cl.forms import CLRutField
from django.utils.translation import ugettext as _
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget

from core.models import Carreer


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password')

    # def __init__(self, *args, **kwargs):
    #     super(UserForm, self).__init__(*args, **kwargs)
    #     self.fields['password'].widget = forms.PasswordInput(attrs={'placeholder': _('Password')})


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = "__all__"


class ProjectProposeForm(forms.Form):
    title = forms.CharField(label=_(u"Título"))
    student = forms.ModelMultipleChoiceField(label=_(u"Estudiante"),queryset=Student.objects.all(), required=False)
    description = forms.CharField(label=_(u"Descripción"),widget=forms.Textarea)
    faculty = forms.ModelChoiceField(label=_(u"Facultad"),queryset=Faculty.objects.all())
    carreer = forms.ModelChoiceField(label=_(u"Carrera"),queryset=Carreer.objects.all())
    period = forms.ModelChoiceField(label=_(u"Período"),queryset=Period.objects.all())
    vacancy = forms.IntegerField(label=_(u"Cupos"),min_value=0, max_value=20)
    tags = forms.CharField(label=_(u"Tags"))
    teacher = forms.ModelChoiceField(label=_(u"Profesor"),queryset=Teacher.objects.all(), required=False)
    def __init__(self, *args, **kwargs):
        super(ProjectProposeForm, self).__init__(*args, **kwargs)
        self.fields['carreer'].queryset = Carreer.objects.none()
        self.fields['teacher'].queryset = Teacher.objects.none()
        self.fields['student'].queryset = Student.objects.none()



class WeekForm(forms.Form):
    name = forms.CharField()
    percentage = forms.IntegerField(min_value=0,max_value=100)


class RubricForm(forms.Form):
    title = forms.CharField(label="Título")
    carreer = forms.ModelChoiceField(label="Carrera",queryset=Carreer.objects.all())
    description = forms.CharField(label="Descripción",widget=forms.Textarea)
    cantidadsemanas = forms.IntegerField(label="Cantidad de Sesiones",min_value=0, max_value=20)

class StudentForm(forms.Form):
    first_name = forms.CharField(label=_('Nombre'))
    last_name = forms.CharField(label=_('Apellido'))
    username = forms.CharField(label=_('Nombre de Usuario'),
                                        required=False)
    password = forms.CharField(label=_('Password'),
                                widget=forms.PasswordInput(),
                                required=False)

    email = forms.EmailField()
    carreer = forms.ModelChoiceField(queryset=Carreer.objects.all(),
                                        label=_('Programa'), required=False)
    campus = forms.ModelChoiceField(queryset=Campus.objects.all(),
                                    label=_('Campus'), required=False)
    rut = CLRutField(label=_('RUN'))
    description = forms.CharField(label=_(u"Descripción"), \
        widget=SummernoteWidget(attrs={'width': '100%'}),
        required=False)

    avatar = forms.ImageField(label=_('Avatar (opcional)'),
                                required=False)
    transcript = forms.FileField(label=_('Ficha Curricular (opcional)'),
                                    required=False)
    pga = forms.CharField(label=_('PGA'),max_length=3, required=False)



class SemanaForm(forms.Form):
    tematica = forms.CharField(label="Tematica de la semana")
    objetivo = forms.CharField()


class TeacherForm(forms.Form):
    POST_GRADES_CHOICES = (('PH', 'Doctorado'),
                           ('MA', 'Magister'),
                           ('LI', 'Licenciado'),
                          )
    TEACHER_TYPES_CHOICES = (('DO', 'Docente'),
                            ('SA', u'Secretario Académico'),
                            ('DC', 'Director de Carrera'),
                          )
    first_name = forms.CharField(label=_('Nombre'))
    last_name = forms.CharField(label=_('Apellido'))
    username = forms.CharField(label=_('Nombre de Usuario'))
    password = forms.CharField(label=_('Password'),
                                widget=forms.PasswordInput(),
                                required=False)
    email = forms.EmailField(label=_('Email'))
    post_grade = forms.ChoiceField(label=_('Grado'),
                                    choices=POST_GRADES_CHOICES,
                                    required=False)
    teacher_type = forms.ChoiceField(label=_('Cargo'),
                                        choices=TEACHER_TYPES_CHOICES,
                                        required=False)
    rut = CLRutField()

    carreer = forms.ModelChoiceField(queryset=Carreer.objects.all(),
                                     label=_('Programa'), required=False)
    campus = forms.ModelChoiceField(queryset=Campus.objects.all(),
                                    label=_('Campus'), required=False)
    shift = forms.ModelChoiceField(queryset=Shift.objects.all(),
                                   label=_('Jornada'),
                                   required=False)

    rut = CLRutField(label=_('RUN'))
    description = forms.CharField(label=_(u"Descripción"), \
        widget=SummernoteWidget(attrs={'width': '100%'}),
                                  required=False)

    avatar = forms.ImageField(label=_('Avatar (opcional)'),
                                required=False)
    cv = forms.FileField(label=_('Curriculum Vitae (opcional)'),
                         required=False)


class AbetForm(forms.Form):
    name = forms.CharField(label=_(u"Nombre"))
    carreer = forms.ModelChoiceField(queryset=Carreer.objects.exclude(name="stub").filter(abet=None),
                                     label=_('Carrera'), required=False)
    outcome_desc = forms.CharField(label=_(u"Descripción Outcome"), widget=forms.Textarea)

class CriteriaForm(forms.Form):
    name = forms.CharField(label=_(u"Descripción de Criterio"), widget=forms.Textarea)
    sufficient = forms.CharField(label=_(u"Suficiente"), widget=forms.Textarea)
    inProcess = forms.CharField(label=_(u"En Proceso"), widget=forms.Textarea)
    inscipient = forms.CharField(label=_(u"Insipiente"), widget=forms.Textarea)

class AbetRubricForm(forms.Form):
    name = forms.CharField(label=_(u"Nombre de la rubrica"))
    carreer = forms.ModelChoiceField(queryset=Carreer.objects.exclude(name="stub").exclude(abet=None), label=_('Carrera'))
    outcome = forms.ModelMultipleChoiceField(queryset=Outcome.objects.all(), label=_('Outcome'))
    nota_suf = forms.IntegerField(label="Suficiente", min_value=1, max_value=7)
    nota_enpr = forms.IntegerField(label="En Proceso", min_value=1, max_value=7)
    nota_inci = forms.IntegerField(label="Incipiente", min_value=1, max_value=7)
    subject = forms.ModelChoiceField(queryset=Course.objects.none(), label=_('Asignatura'))
