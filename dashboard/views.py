#-*- coding: UTF-8 -*-
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.template import RequestContext
from django.contrib import messages
from django.contrib.auth.decorators import login_required
import requests
import ast
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from django import forms
from dashboard.forms import TeacherForm, StudentForm, ProjectProposeForm,\
                            RubricForm, SemanaForm, WeekForm, AbetForm, CriteriaForm,\
                            AbetRubricForm

from core.models import *
import datetime
import json
from dashboard.login.views import login_needed


def get_data_carreer(request):
    carreer_pk = request.POST['carreer_pk']
    carreer_obj = Carreer.objects.none()
    teacher_options = '<option value="" selected="selected">---------</option>'
    student_options = ''
    shift_options = '<option value="" selected="selected">---------</option>'
    teachers = []
    students = []
    shifts = Shift.objects.all()
    print shifts
    if carreer_pk:
        # carreer_obj = get_object_or_404(Carreer, pk=carreer_pk)
        # Carreer_codes = carreer_obj.carreer_code_set.all()
        codes = Carreer_code.objects.filter(carreer=carreer_pk)
        for z in codes:
            academic_records = Academic_record.objects.filter(carreer_code=z)
            for x in academic_records:
                # personal_obj = x.personal_record_set.all()
                personal_obj = Personal_record.objects.filter(academic_record=x)
                for y in personal_obj:
                    try:
                        # print y.student
                        if y.student and not y.student.project:
                            students.append(y.student)
                    except :                        
                        try:
                            teachers.append(y.teacher)
                        except:
                            print "no teacher"    
                    
                                                                 
    # teachers = teachers.annotate(Sum('pk'))
                  
    for teacher_obj in teachers:
        print teacher_obj.pk
    for teacher_obj in teachers:
        teacher_options += '<option value="%s">%s</option>' % (
            teacher_obj.pk,
            teacher_obj.user.first_name + " " + teacher_obj.user.last_name
        )
    for student_obj in students:
        student_options += '<option value="%s">%s %s | %d-%s</option>' % (
            student_obj.pk,
            student_obj.user.first_name,
            student_obj.user.last_name,
            student_obj.rut,
            student_obj.dv
        )
    for shift_obj in shifts:
        shift_options += '<option value="%s">%s</option>' % (
            shift_obj.pk,
            shift_obj.name
        )
    response = {}
    response['teacher'] = teacher_options
    response['student'] = student_options
    response['shift'] = shift_options
    print shift_options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

@login_needed
def get_carrer(request):
    faculty_pk = request.POST['faculty_pk']
    faculty_obj = Faculty.objects.none()
    options = '<option value="" selected="selected">---------</option>'
    if faculty_pk:
        faculty_obj = get_object_or_404(Faculty, pk=faculty_pk)
        academic_records = Academic_record.objects.filter(faculty=faculty_obj)
        carrers = []
        for x in academic_records:
            if x.carreer_code.carreer.name not in carrers:
                carrers.append(x.carreer_code.carreer.name)
                options += '<option value="%s">%s</option>' % (
                    x.carreer_code.carreer.pk,
                    x.carreer_code.carreer.name
                    )

    response = {}
    response['carreers'] = options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def index(request):
    projects = Project.objects.all()
    data = {}
    data["Projects"] = projects

    template_name = 'dashboard/index.html'
    return render(request, template_name, data)

#@login_needed
def load_carreer(request):
    template_name = 'dashboard/load_carreer.html'
    return render(request, template_name, {})



def delete_notifications(request):
    messages = Mail.objects.filter(receipts=request.user,mail_type="SA")
    for x in messages:
        x.delete()
    return HttpResponse(
            json.dumps({"da": "Notificaciones Borradas"}),
            content_type="application/json"
        )

#@login_needed
def rubric_list(request):
    rubrics = Rubric.objects.all()
    rubrics = rubrics[::-1]
    template_name = 'dashboard/planning/rubric_list.html'
    return render(request, template_name, {'rubrics':rubrics})



#@login_needed
def rubric_add(request):
    template_name = 'dashboard/planning/add_rubric.html'
    form = RubricForm()
    return render(request, template_name, {'forms':form})


#@login_needed
def bitacora(request):
    template_name = 'dashboard/bitacora/bitacora.html'
    return render(request, template_name)


#@login_needed
def rubric_delete(request, pk):
    rubric = get_object_or_404(Rubric, pk=pk)
    rubric.delete()
    return HttpResponseRedirect(reverse_lazy('planning_rubric_list'))

#@login_needed
def rubric_details(request,pk):
    template_name = 'dashboard/planning/rubric_details.html'
    rubric_obj = get_object_or_404(Rubric, pk=pk)
    sessions = Session.objects.filter(rubric=rubric_obj)
    form = SemanaForm()
    weekform = WeekForm()
    return render(request, template_name, {'forms':form,
                                           'weekform':weekform,
                                           'sessions':sessions,
                                           'rubric':rubric_obj})

#@login_needed
def rubric_create(request):
    response = {}
    cont = 1
    sessions = json.loads(request.POST['sessions'])
    name = request.POST['title']
    description = request.POST['description']
    carreer = get_object_or_404(Carreer, pk=request.POST['carreer'])
    rubrica = Rubric.objects.create(name=name,
                                    date=datetime.datetime.now(),
                                    author=request.user,
                                    description=description,
                                    carreer=carreer,
                                   )

    response['pk'] = rubrica.pk
    rubrica.save()
    for x in sessions:
        session_obj = Session.objects.create(number=cont,
                                  rubric=rubrica)
        if x == "s":
            week = Week.objects.create(session = session_obj)

        if x == "h":
            hito = Hito.objects.create(session=session_obj)
        if x == "a":
            session_obj.abet = 1
        session_obj.save()
        cont += 1
    response['mensaje'] = "Rubrica Creada"
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

#@login_needed
def evaluation_create(request):
    week_obj = get_object_or_404(Week, pk=request.POST['week_pk'])
    evaluation = Evaluation.objects.create(week=week_obj,
                                           name=request.POST['name'],
                                           percentage=int(request.POST['percentage']))
    response = {}
    if request.POST['option'] == "actividad":
        evaluation.type_evaluation = "AT"
        week_obj.total_activity_percentage += evaluation.percentage
        if week_obj.total_activity_percentage > 100:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_activity_percentage
            week_obj.save()
    elif request.POST['option'] == "avance":
        evaluation.type_evaluation = "AV"
        week_obj.total_progress_percentage += evaluation.percentage
        if week_obj.total_progress_percentage > 100:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_progress_percentage
            week_obj.save()
    elif request.POST['option'] == "objetivo":
        evaluation.type_evaluation = "OB"
        week_obj.total_objetive_percentage += evaluation.percentage
        if week_obj.total_objetive_percentage > 100 or evaluation.percentage < 0:
            response['error'] = 0
            evaluation.delete()
            return HttpResponse(
                                json.dumps(response),
                                content_type="application/json"
                                )
        else:
            evaluation.save()
            response['total_percentage'] = week_obj.total_objetive_percentage
            week_obj.save()
    response['week_pk'] = week_obj.pk
    response['evaluation_type'] = evaluation.type_evaluation
    response['evaluation_name'] = evaluation.name
    response['evaluation_pk'] = evaluation.pk
    response['evaluation_percentage'] = evaluation.percentage
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

#@login_needed
def get_carreers(request):
    headers = {"Content-Type":"application/x-www-form-urlencoded","Authorization":"Token "+request.session["token"]}
    response = requests.get("http://auth.cubesoa.cl/get/get_carrers/", headers=headers)
    dic = ast.literal_eval(response.content)
    carreers = dic["carreers"]
    response = []
    print carreers
    for carreer in carreers:
        for key in carreer:
            response.append((carreer[key][0],carreer[key][1]))


    return JsonResponse({"mensaje":"exito",
                         "carreers":response})

#@login_needed
def item_create(request):
    response = {}
    hito_obj = get_object_or_404(Hito, pk=request.POST['hito_pk'])
    if request.POST['option'] == "memoria":
        memory = Memory.objects.create(hito=hito_obj,
                                      item=request.POST['item'],
                                      percentage = int(request.POST['percentage']))
        total_sum = hito_obj.total_memory_percentage + memory.percentage
        if total_sum > 100 or total_sum < 0 :
            response['error'] = 0
            memory.delete()
            return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )
        hito_obj.total_memory_percentage = total_sum
        hito_obj.save()
        memory.save()
        response['total_percentage'] = hito_obj.total_memory_percentage
        response['item_name'] = memory.item
        response['item_pk'] = memory.pk
        response['item_percentage'] = memory.percentage
        response['option'] = "memory"

    elif request.POST['option'] == "tecnica":
        technique = Technique.objects.create(hito=hito_obj,
                                            item=request.POST['item'],
                                            percentage = int(request.POST['percentage']))
        total_sum = hito_obj.total_technique_percentage + technique.percentage
        if total_sum > 100 or total_sum < 0 :
            response['error'] = 0
            technique.delete()
            return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )
        hito_obj.total_technique_percentage = total_sum
        hito_obj.save()
        technique.save()
        response['total_percentage'] = hito_obj.total_technique_percentage
        response['item_name'] = technique.item
        response['item_pk'] = technique.pk
        response['item_percentage'] = technique.percentage
        response['option'] = "technique"

    response['hito_pk'] = hito_obj.pk
    print response
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )



def delete_item(request):
    response = {}
    if request.POST['option'] == "technique":
        technique = get_object_or_404(Technique, pk=request.POST['pk'])
        technique.hito.total_technique_percentage -= technique.percentage
        technique.hito.save()
        response['total_percentage'] = technique.hito.total_technique_percentage
        response['option'] = "TE"
        response['hito_pk'] = technique.hito.pk
        technique.delete()
    elif request.POST['option'] == "memory":
        memory = get_object_or_404(Memory, pk=request.POST['pk'])
        memory.hito.total_memory_percentage -= memory.percentage
        memory.hito.save()
        respone['total_percentage'] = memory.hito.total_memory_percentage
        response['option'] = "ME"
        response['hito_pk'] = memory.hito.pk
        memory.delete()
    elif request.POST['option'] == "presentation":
        subtopic = get_object_or_404(Subtopic, pk=request.POST['pk'])
        subtopic.topic.hito.total_presentation_percentage -= subtopic.percentage
    return HttpResponse(
            json.dumps(response),
            content_type="application/json"
            )


def delete_evaluation(request):
    response = {}
    evaluation = get_object_or_404(Evaluation, pk=request.POST['evaluation_pk'])
    week_obj = evaluation.week
    response['week_pk'] = evaluation.week.pk
    response['type'] = evaluation.type_evaluation
    if response['type'] == "OB":
        week_obj.total_objetive_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_objetive_percentage
    elif response['type'] == "AV":
        week_obj.total_progress_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_progress_percentage
    elif response['type'] == "AT":
        week_obj.total_activity_percentage -= evaluation.percentage
        response['percentage'] = week_obj.total_activity_percentage
    evaluation.delete()
    week_obj.save()
    response['evaluation_pk'] = request.POST['evaluation_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#def edit_item(request):
#    response = {}
#    if request.POST:
#        if request.POST['option'] == "objetive" or request.POST['option'] == "activity" or request.POST['option'] == "progress":
#            item = get_object_or_404(Evaluation, pk=request.POST["pk"])
#            item.name = request.POST['name']
#            item.percentage = request.POST['percentage']
#            item.save()
#            response[ite]
#        return HttpResponse(
#            json.dumps(response),
#            content_type="application/json"
#            )



def add_topic_week(request):
    week = get_object_or_404(Week, pk=request.POST['week_pk'])
    week.topic = request.POST['week_topic']
    response = {}
    response['week_pk'] = week.pk
    response['week_topic'] = week.topic
    week.save()
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

def add_presentation_item(request):
    hito_obj = get_object_or_404(Hito, pk=request.POST['hito_pk'])
    topics = Topic.objects.filter(hito = hito_obj)
    exist_topic = 0
    response = {}
    for x in topics:
        if x.name == request.POST['topic']:
            exist_topic = x.pk
            topic_obj = get_object_or_404(Topic, pk=exist_topic)
            print topic_obj
    if exist_topic == 0:
        topic_obj = Topic.objects.create(hito=hito_obj,
                                    name=request.POST['topic'])
        topic_obj.save()
    subtopic = SubTopic.objects.create(topic = topic_obj,
                                      name=request.POST['subtopic'],
                                      percentage=int(request.POST['percentage']))
    sum_total = hito_obj.total_presentation_percentage + subtopic.percentage
    if sum_total > 100 or subtopic.percentage < 0:
        response['error'] = 0
        subtopic.delete()
        if exist_topic == 0:
            topic_obj.delete()
        return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )
    hito_obj.total_presentation_percentage = sum_total
    hito_obj.save()
    response['total_percentage'] = hito_obj.total_presentation_percentage
    response['topic_name'] = topic_obj.name
    response['topic_pk'] = topic_obj.pk
    response['hito_pk'] = request.POST['hito_pk']
    response['subtopic_percentage'] = subtopic.percentage
    response['subtopic_pk'] = subtopic.pk
    response['subtopic_name'] = subtopic.name
    print response
    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

#@login_needed
def abet_list(request):
    abets = Abet.objects.all()
    abets = abets[::-1]
    form = AbetForm()
    template_name = 'dashboard/planning/abet_list.html'
    return render(request, template_name, {'abets':abets, 'form':form, 'request':request})

#@login_needed
def abet_add(request):
    template_name = 'dashboard/planning/add_abet.html'
    form_a = AbetForm()
    form_c = CriteriaForm()
    return render(request, template_name, {'form_abet':form_a, 'form_criteria':form_c})

#@login_needed
def abet_delete(request, pk):
    abet = get_object_or_404(Abet, pk=pk)
    abet.delete()
    return HttpResponseRedirect(reverse_lazy('planning_abet_list'))

#@login_needed
def abet_details(request, pk):
    template_name = 'dashboard/planning/abet_details.html'
    abet_obj = get_object_or_404(Abet, pk=pk)
    outcomes = Outcome.objects.filter(abet=abet_obj).order_by("letter")
    return render(request, template_name, {'abet':abet_obj, 'outcomes':outcomes})


#@login_needed
def abet_create(request):
    response = {}
    name = request.POST['abet_name']
    if request.POST['abet_carreer'] == '':
        abet = Abet.objects.create(name=name,
                                   author=request.user,
                                   date=datetime.datetime.now(),
                                  )
        abet.save()

    else:
        carreer = get_object_or_404(Carreer, pk=request.POST['abet_carreer'])
        abet = Abet.objects.create(name=name,
                                   author=request.user,
                                   date=datetime.datetime.now(),
                                   carreer=carreer,
                                  )
        abet.save()

    response['pk'] = abet.pk
    response['mensaje'] = "Abet Creado"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def outcome_create(request):
    response = {}
    outcome_desc = request.POST['outcome_desc']
    abet = get_object_or_404(Abet, pk=request.POST['abet_pk'])
    outcome = Outcome.objects.create(abet=abet,
                                     description=outcome_desc,
                                    )
    outcome.set_letter(abet)
    outcome.save()

    outcome_desc_response = outcome_desc[0:54] + "..."
    response['outcome_pk'] = outcome.pk
    response['mensaje'] = "Outcome Creado"
    response['outcome_letter'] = outcome.letter
    response['outcome_desc_response'] = outcome_desc_response

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def criteria_create(request):
    response = {}
    suficient = request.POST['criteria_sufficient']
    inprocess = request.POST['criteria_inProcess']
    inscipient = request.POST['criteria_inscipient']
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    criteria_info = Crit_info.objects.create(suficient=suficient,
                                             inprocess=inprocess,
                                             inscipient=inscipient
                                            )
    criteria_info.save()

    criteria = Criteria.objects.create(outcome=outcome,
                                       description=request.POST['criteria_desc'],
                                       criteria_info=criteria_info
                                      )
    criteria.save()
    response['mensaje'] = "Criterio Creado"
    response['criteria_pk'] = criteria.pk
    response['criteria_desc'] = criteria.description
    response['criteria_suf'] = criteria_info.suficient
    response['criteria_inpr'] = criteria_info.inprocess
    response['criteria_ins'] = criteria_info.inscipient

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def get_criteria_info(request):
    response = {}
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    response["criteria_desc"] = criteria.description
    response["criteria_suf"] = criteria.criteria_info.suficient
    response["criteria_inpr"] = criteria.criteria_info.inprocess
    response["criteria_ins"] = criteria.criteria_info.inscipient

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )
#@login_needed
def criteria_edit(request):
    response = {}
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    crit_info = criteria.criteria_info
    criteria.description = request.POST['criteria_desc']
    crit_info.suficient = request.POST['criteria_sufficient']
    crit_info.inprocess = request.POST['criteria_inProcess']
    crit_info.inscipient = request.POST['criteria_inscipient']

    criteria.save()
    crit_info.save()

    response["criteria_desc"] = criteria.description
    response["criteria_suf"] = criteria.criteria_info.suficient
    response["criteria_inpr"] = criteria.criteria_info.inprocess
    response["criteria_ins"] = criteria.criteria_info.inscipient
    response["criteria_pk"] = criteria.pk

    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def criteria_delete(request):
    criteria = get_object_or_404(Criteria, pk=request.POST['criteria_pk'])
    criteria.delete()
    response = {}
    response["criteria_pk"] = request.POST['criteria_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def outcome_delete(request):
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    outcome.delete()
    response = {}
    response["outcome_pk"] = request.POST['outcome_pk']
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_rubric_list(request):
    form = AbetRubricForm()
    template_name = 'dashboard/planning/abet_rubric_list.html'
    abet_rubrics = AbetRubric.objects.all()
    abets_activos = []
    abets_privados = []
    for rubric in abet_rubrics:
        if rubric.author == request.user and rubric.get_course() == None :
            abets_privados.append(rubric)

        elif rubric.get_course() != None:
            abets_activos.append(rubric)
    return render(request, template_name, {'form':form,
                                            'abets_activos':abets_activos,
                                            'abets_privados':abets_privados})

#@login_needed
def abet_activate(request):
    carreerpk = request.POST['carreerpk']
    carrera = get_object_or_404(Carreer,pk=carreerpk)
    abetpk = request.POST['abet_pk']
    abet = get_object_or_404(Abet,pk=abetpk)
    abet.carreer = carrera
    abet.save()
    response = {}
    response["message"] = "activado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_deactivate(request):
    abetpk = request.POST["abetpk"]
    abet = get_object_or_404(Abet, pk=abetpk)
    abet.carreer = None
    abet.save()
    response = {}
    response["message"] = "desactivado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_rubric_add(request):
    template_name = 'dashboard/planning/abet_rubric_add.html'
    form = AbetRubricForm()
    return render(request, template_name, {'form':form})


#@login_needed
def abet_rubric_create(request):
    name = request.POST["nombre"]
    subject_pk = request.POST["subject_pk"]
    nota_suf = request.POST["nota_suf"]
    nota_enpr = request.POST["nota_enpr"]
    nota_inci = request.POST["nota_inci"]
    carreer = get_object_or_404(Carreer, pk=request.POST["carreer_pk"])
    rubric = AbetRubric.objects.create(
                                       name=name,
                                       author=request.user,
                                       carreer=carreer,
                                       date=datetime.datetime.now(),
                                       nota_suf=nota_suf,
                                       nota_enpr=nota_enpr,
                                       nota_inci=nota_inci
                                      )
    rubric.save()

    response = {}

    if subject_pk != "":
        course_subject = get_object_or_404(Course, pk=subject_pk)
        course_subject.abet_rubric = rubric
        course_subject.save()
        response["titulo"] = "Rubrica: %s.| Para la asignatura %s" % (name, course_subject.subject)

    else:
        response["titulo"] = "Rubrica: %s.| Sin asignatura." % (name)


    response["rubricpk"] = rubric.pk

    return HttpResponse(
    json.dumps(response),
    content_type="application/json"
    )

def get_carreer_subjects(request):
    carreer_pk = request.POST['carreer_pk']
    carreer = get_object_or_404(Carreer, pk=carreer_pk)
    options = '<option value="" selected="selected">---------</option>'
    carreer_code = carreer.carreer_code_set.all()
    subjects = []
    for x in carreer_code:
        aux = x.course_set.all()
        print aux
        for i in aux:
            subjects.append(i)
            ident="opcion_"+str(i.pk)
            options += '<option value="%s" id="%s">%s - %s</option>' % (
                i.pk,
                ident,
                i.name,
                i.carreer_code)


    response = {}
    response['subjects'] = options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def get_carreer_outcomes(request):
    carreer_pk = request.POST['carreer_pk']
    carreer = get_object_or_404(Carreer, pk=carreer_pk)
    options = ''
    abet = get_object_or_404(Abet,carreer=carreer)
    outcomes = abet.outcome_set.all()
    for outcome in outcomes:
        options += '<option value="%s">%s. %s</option>' % (
            outcome.pk,
            outcome.letter,
            outcome.description)

    response = {}
    response['outcomes'] = options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def get_opt_rubric_outcomes(request):
    tipo = ""
    try:
        carreer = get_object_or_404(Carreer, pk=request.POST["carreer_pk"])
        rubric = get_object_or_404(AbetRubric, pk=request.POST["rubric"])
        tipo = "POST"
    except AttributeError:
        print "ESTE ES EL VALOR DE rubric EN OPT:", request
        carreer = get_object_or_404(Carreer, pk=request[1])
        print "Este es el valor de rubric"
        print request[0]
        rubric = get_object_or_404(AbetRubric, pk=request[0])
        tipo = "GET"
    options = ''
    abet = get_object_or_404(Abet,carreer=carreer)
    outcomes_abet = abet.outcome_set.all()
    for outcome in outcomes_abet:
        if outcome not in rubric.outcomes.all():
            options += '<option value="%s">%s. %s</option>' % (
                outcome.pk,
                outcome.letter,
                outcome.description)

    if tipo == "GET":
        return options

    else:
        response = {}
        response["outcomes"] = options
        return HttpResponse(
                json.dumps(response),
                content_type="application/json"
                )

def abet_rubric_add_outcomes(request):
    outcome_pks = request.POST.getlist('keys[]')
    outcomes_aux = []
    for key in outcome_pks:
        outcome = get_object_or_404(Outcome, pk=key)
        outcomes_aux.append(outcome)

    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(AbetRubric, pk=rubricpk)

    for outcome in outcomes_aux:
        rubric.outcomes.add(outcome)

    rubric.save()
    outcomes_html = ""

    for outcome in outcomes_aux:
        criterios_html = ""
        for criteria in outcome.criteria_set.all():
            aux='<tr id="row_criteria_%d">\
                     <td>%s</td>\
                     <td>%s</td>\
                     <td>%s</td>\
                     <td>%s</td>\
                 </tr>' % (criteria.pk, criteria.description, criteria.criteria_info.suficient, criteria.criteria_info.inprocess, criteria.criteria_info.inscipient)
            criterios_html+=aux
        aux='<div class="ibox animated fadeInRight" id="outcome_info_%s">\
                <div class="ibox-title bg-warning">\
                    <h5 style="font-size:20px;">%s. %s...</h5>\
                    <div class="ibox-tools">\
                        <button class="btn btn-sm btn-danger remove_outcome" data-outcomepk="%s">\
                            <i class="fa fa-times" style="color:white"></i>\
                        </button>\
                    </div>\
                </div>\
                <div class="ibox-content">\
                    <table class="table">\
                        <thead>\
                        <tr>\
                            <th>Descripcion Criterio</th>\
                            <th>Suficiente</th>\
                            <th>En Proceso</th>\
                            <th>Inscipiente</th>\
                        </tr>\
                        </thead>\
                        <tbody id="content_table_criteria_%d">\
                            %s\
                        </tbody>\
                    </table>\
                </div>\
            </div>' % (outcome.pk,outcome.letter, outcome.description[0:64], outcome.pk, outcome.pk, criterios_html)
        outcomes_html+=aux
    arg = (rubric.pk,request.POST["carreer_pk"])
    options=get_opt_rubric_outcomes(arg)
    response = {}
    response["outcomes_opt"] = options
    response["outcomes"] = outcomes_html

    return HttpResponse(
                        json.dumps(response),
                        content_type="application/json"
                        )

#@login_needed
def rubric_outcome_remove(request):
    print "llege al views"
    rubric = get_object_or_404(AbetRubric, pk=request.POST['rubric_pk'])
    outcome = get_object_or_404(Outcome, pk=request.POST['outcome_pk'])
    rubric.outcomes.remove(outcome)
    rubric.save()

    arg = (rubric.pk,request.POST["carreer_pk"])

    options=get_opt_rubric_outcomes(arg)

    response = {}

    response["outcomes_opt"] = options
    response["outcome_pk"] = outcome.pk
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def create_retro(request):
    response = {}
    student = get_object_or_404(Student,pk=request.POST["student_pk"])
    project = get_object_or_404(Project,pk=request.POST["project_pk"])
    retro = Retro.objects.create(text=request.POST["retro_al"],
                                student=student,
                                project=project)
    retro.save()
    response["ok"] = "ok"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_rubric_details(request,pk):
    template_name = 'dashboard/planning/abet_rubric_details.html'
    form = AbetRubricForm()
    rubric = get_object_or_404(AbetRubric,pk=pk)
    carreer = rubric.carreer
    return render(request, template_name, {'form':form, 'rubric':rubric, 'carreer':carreer})

#@login_needed
def teacher_projects(request):
    project = Project.objects.filter(guide_teacher=request.user.teacher)
    template_name = 'dashboard/evaluation/teacher_projects.html'
    data = {}
    data['projects'] = project
    return render(request, template_name, data)

def rubric_activate(request):
    rubric = get_object_or_404(Rubric, pk=request.POST['rubric_pk'])
    course = get_object_or_404(Course, pk=request.POST['course_pk'])
    course.rubric = rubric
    course.save()
    response = {}
    response["data"] = "True"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


def charge_student(request):
    project_obj = get_object_or_404(Project, pk=request.POST['project_pk'])
    students = Student.objects.filter(project=project_obj)
    student_options = '<option value="" selected="selected">Seleccione un Alumno</option>'
    for student_obj in students:
         student_options += '<option value="%s">%s | %s-%s</option>' % (
            student_obj.pk,
            student_obj.user.first_name + " " + student_obj.user.last_name,
            student_obj.rut,
            student_obj.dv,
        )
    response = {}
    response["students"] = student_options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def set_grades(request):
    nota_suf = request.POST["nota_suf"]
    nota_enpr = request.POST["nota_enpr"]
    nota_inci = request.POST["nota_inci"]

    rubric = get_object_or_404(AbetRubric,pk=request.POST["rubric_pk"])

    rubric.nota_suf = nota_suf
    rubric.nota_enpr = nota_enpr
    rubric.nota_inci = nota_inci

    rubric.save()

    response = {}

    response["mensaje"] = "notas agregadas con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def charge_session(request):
    student_obj = get_object_or_404(Student, pk=request.POST['student_pk'])
    sessions = student_obj.project.section.course.rubric.session_set.all()
    sessions_options = '<option value="" selected="selected">Seleccione una Sesion</option>'
    for session in sessions:
         sessions_options += '<option value="%s">Sesion %s</option>' % (
            session.pk,
            session.number,
        )
    response = {}
    response["sessions"] = sessions_options
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_rubric_activate(request):
    coursepk = request.POST['course_pk']
    course = get_object_or_404(Course,pk=coursepk)
    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(AbetRubric,pk=rubricpk)
    course.abet_rubric = rubric
    course.save()
    response = {}
    response["message"] = "activado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def rubric_desactivate(request, pk):
    course = get_object_or_404(Course, pk=pk)
    course.rubric = None
    course.save()
    response = {}
    response["data"] = "True"
    return HttpResponseRedirect(reverse_lazy('planning_rubric_list'))


def get_subject(request):
    rubric = get_object_or_404(Rubric, pk=request.POST['rubric_pk'])
    courses = []
    for x in rubric.carreer.carreer_code_set.all():
        course = Course.objects.filter(carreer_code=x)
        for a in course:
            courses.append(a)
            print a
    course_option = '<option value="" selected="selected">---------</option>'
    for i in courses:
        course_option += '<option value="%s">%s</option>' % (
            i.pk,
            i.name
        )
    response = {}
    response["course_option"] = course_option
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )


#@login_needed
def abet_rubric_deactivate(request):
    rubricpk = request.POST['rubric_pk']
    rubric = get_object_or_404(AbetRubric,pk=rubricpk)
    course = get_object_or_404(Course, pk=rubric.course.pk)
    course.abet_rubric = None
    course.save()
    response = {}
    response["message"] = "activado con exito"
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def student_evaluation(request, pk_session, pk_student):
    student = get_object_or_404(Student, pk=pk_student)
    session = get_object_or_404(Session,pk=pk_session)
    data = {}
    data['session'] = session
    data['student'] = student
    template_name = 'dashboard/evaluation/student_evaluation.html'
    return render(request, template_name, data)

def create_evaluation(request):
    response = {}
    len_obj = request.POST['len_obj']
    len_obj = int(len_obj)
    len_act = request.POST['len_act']
    len_act = int(len_act)
    len_pro = request.POST['len_pro']
    len_pro = int(len_pro)
    option = False
    student_obj = get_object_or_404(Student,pk=request.POST['student_pk'])
    session_obj = get_object_or_404(Session, pk=request.POST['session_pk'])
    student_evaluations = StudentEvaluation.objects.filter(student = student_obj, session=session_obj)
    objetive_qualification = 0.0
    activity_qualification = 0.0
    progress_qualification = 0.0
    if len(student_evaluations) != 0:
            option = True
    if option == False:
        student_evaluation_obj = StudentEvaluation.objects.create(
                                        student = student_obj,
                                        session = session_obj,
                                        comment_progress = request.POST["comment_progress"],
                                        comment_activity = request.POST["comment_activity"],
                                        comment_accomplishment = request.POST["comment_accomplishment"]
                                        )
        for i in range(len_obj):
            evaluation_pk = "objetivos[" + str(i) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            evaluation_pk = int(evaluation_pk)
            nota_post = "objetivos[" + str(i) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
            objetive_qualification = objetive_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            ObjetiveEvaluation.objects.create(student_evaluation=student_evaluation_obj,
                                            evaluation = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.objetive_qualification = objetive_qualification
        student_evaluation_obj.save()
        for x in range(len_act):
            evaluation_pk = "actividades[" + str(x) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            evaluation_pk = int(evaluation_pk)
            nota_post = "actividades[" + str(x) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
            activity_qualification = activity_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            ActivityEvaluation.objects.create(student_evaluation=student_evaluation_obj,
                                            evaluation = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.activity_qualification = activity_qualification
        student_evaluation_obj.save()
        for z in range(len_pro):
            evaluation_pk = "avances[" + str(z) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            print evaluation_pk
            print type(evaluation_pk)
            evaluation_pk = int(evaluation_pk)
            nota_post = "avances[" + str(z) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(Evaluation,pk=evaluation_pk)
            progress_qualification = progress_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            ProgressEvaluation.objects.create(student_evaluation=student_evaluation_obj,
                                            evaluation = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.progress_qualification = progress_qualification
        student_evaluation_obj.session_qualification = (progress_qualification + objetive_qualification +activity_qualification)/3
        student_evaluation_obj.save()
        response["True"] = True
    else:
        response["True"] = False
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

def create_evaluation_hito(request):
    response = {}
    len_mem = request.POST['len_mem']
    len_mem = int(len_mem)
    len_tech = request.POST['len_tech']
    len_tech = int(len_tech)
    len_pre = request.POST['len_pre']
    len_pre = int(len_pre)
    option = False
    student_obj = get_object_or_404(Student,pk=request.POST['student_pk'])
    session_obj = get_object_or_404(Session, pk=request.POST['session_pk'])
    student_evaluations = StudentHitoEvaluation.objects.filter(student = student_obj, session=session_obj)
    technique_qualification = 0.0
    memory_qualification = 0.0
    presentation_qualification = 0.0
    if len(student_evaluations) != 0:
            option = True
    if option == False:
        student_evaluation_obj = StudentHitoEvaluation.objects.create(
                                        student = student_obj,
                                        session = session_obj,
                                        comment_progress = request.POST["comment_progress"],
                                        comment_activity = request.POST["comment_activity"],
                                        comment_accomplishment = request.POST["comment_accomplishment"]
                                        )
        for i in range(len_pre):
            evaluation_pk = "presentaciones[" + str(i) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            evaluation_pk = int(evaluation_pk)
            nota_post = "presentaciones[" + str(i) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(SubTopic,pk=evaluation_pk)
            presentation_qualification = presentation_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            PresentationEvaluation.objects.create(student_hito_evaluation=student_evaluation_obj,
                                            presentation = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.presentation_qualification = presentation_qualification
        student_evaluation_obj.save()
        for x in range(len_tech):
            evaluation_pk = "tecnicas[" + str(x) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            evaluation_pk = int(evaluation_pk)
            nota_post = "tecnicas[" + str(x) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(Technique,pk=evaluation_pk)
            technique_qualification = technique_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            TechniqueEvaluation.objects.create(student_hito_evaluation=student_evaluation_obj,
                                            technique = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.technique_qualification = technique_qualification
        student_evaluation_obj.save()
        for z in range(len_mem):
            evaluation_pk = "memorias[" + str(z) + "][name]"
            evaluation_pk = request.POST[evaluation_pk]
            evaluation_pk = int(evaluation_pk)
            nota_post = "memorias[" + str(z) + "][value]"
            nota_post = request.POST[nota_post]
            evaluation_obj = get_object_or_404(Memory,pk=evaluation_pk)
            memory_qualification = memory_qualification + ((float(nota_post)*float(evaluation_obj.percentage))/100)
            MemoryEvaluation.objects.create(student_hito_evaluation=student_evaluation_obj,
                                            memory = evaluation_obj,
                                            nota = int(nota_post)
                                            )
        student_evaluation_obj.memory_qualification = memory_qualification
        student_evaluation_obj.hito_qualification = (technique_qualification + presentation_qualification + memory_qualification)/3
        student_evaluation_obj.save()
        response["True"] = True
    else:
        response["True"] = False
    return HttpResponse(
        json.dumps(response),
        content_type="application/json"
        )

#@login_needed
def abet_rubric_delete(request, pk):
    rubric = get_object_or_404(AbetRubric, pk=pk)
    rubric.delete()

    return HttpResponseRedirect(reverse_lazy('planning_abet_rubric_list'))

#@login_needed
def validate_subject(request):
    headers = {"Content-Type":"application/x-www-form-urlencoded","Authorization":"Token "+request.session["token"]}
    response = requests.post("http://auth.cubesoa.cl/get/Validar_Asignatura/", headers=headers, data={"code":request.POST["code"]})
    dic = ast.literal_eval(response.content)
    print dic
    subject=dic["subjects"]
    html = "<tr class='animated fadeInRight'><td style='text-align:center;vertical-align:middle'><span class='codigo' data-code='"+subject[0]+"'></span>"+subject[0]+"</td><td style='text-align:center;vertical-align:middle'>"+subject[1]+"</td><td style='text-align:center'><span class='btn btn-sm btn-danger eliminar' style='color:white'><i class='fa fa-times'></i></span></td></tr>"

    return JsonResponse({"message":"exito",
                         "html":html})

def bypass_carreer(request):
    unicode_dic = {"\u00ed":"í","\u00f3":"ó","\u00e1":"á"}
    carreer_pk = request.POST["carreer_pk"]
    print carreer_pk
    headers = {"Content-Type":"application/x-www-form-urlencoded","Authorization":"Token "+request.session["token"]}
    response = requests.post("http://auth.cubesoa.cl/get/Carreer_Data/", headers=headers, data={"carreer":carreer_pk})
    dic = ast.literal_eval(response.content)
    if not Faculty.objects.filter(name=dic["faculty_name"]).exists():
        faculty = Faculty.objects.create(name=dic["faculty_name"])
        faculty.save()

    carreer_name = dic["nombre"]
    for key in unicode_dic:
        print key
        print key in carreer_name
        carreer_name=carreer_name.replace(key,unicode_dic[key])
    if not Carreer.objects.filter(name=carreer_name).exists():
        n_carreer = Carreer.objects.create(name=carreer_name)
        n_carreer.save()
    else:
        n_carreer = Carreer.objects.get(name=carreer_name)
    
    if not Carreer_code.objects.filter(carreer=n_carreer,code=dic["advance_cn"],shift="Advance").exists():
        codigo1 = Carreer_code.objects.create(carreer=n_carreer,
                                                code=dic["advance_cn"],
                                                shift="Advance")
        codigo1.save()

    if not Carreer_code.objects.filter(carreer=n_carreer,code=dic["advance_sn"],shift="Advance").exists():
        codigo2 = Carreer_code.objects.create(carreer=n_carreer,
                                                code=dic["advance_sn"],
                                                shift="Advance")
        codigo2.save()

    if not Carreer_code.objects.filter(carreer=n_carreer,code=dic["diurno"],shift="Diurno").exists():
    
        codigo3 = Carreer_code.objects.create(carreer=n_carreer,
                                                code=dic["diurno"],
                                                shift="Diurno")
        codigo3.save()
    if not Carreer_code.objects.filter(carreer=n_carreer,code=dic["vespertino"],shift="Vespertino").exists():
        codigo4 = Carreer_code.objects.create(carreer=n_carreer,
                                                code=dic["vespertino"],
                                                shift="Vespertino")
        codigo4.save()

    codes = request.POST.getlist("codes[]")
    
    codes = "/".join(codes)
    headers2 = {"Content-Type":"application/x-www-form-urlencoded","Authorization":"Token "+request.session["token"]}
    response2 = requests.post("http://auth.cubesoa.cl/get/subject_dump/", headers=headers, data={"codes":codes})
    dic2 = ast.literal_eval(response2.content)
    
    for key in dic2:
        students = dic2[key]["students"]
        teachers = dic2[key]["teachers"]

        for student in students:
            rut = int(student["rut"].split("-")[0])
            dv = student["rut"].split("-")[1]
            if not Student.objects.filter(rut=rut).exists():
                if not User.objects.filter(username=student["user"]).exists():
                    n_user = User.objects.create_user(username=student['user'],password="Unab2020")
                    n_user.save()
                    n_user.first_name = student["nombre"]
                    n_user.last_name = student["apellidos"]
                    n_user.save()
                else:
                    n_user = User.objects.get(username=student["user"])

                print "RUT"
                print rut,dv
                n_student = Student.objects.create(user=n_user,
                                                   rut=rut,
                                                   dv=dv)
                n_student.save()
                if Carreer_code.objects.filter(shift=student["shift"],carreer=n_carreer).exists():
                    carreer_code = Carreer_code.objects.get(shift=student["shift"],carreer=n_carreer)
                

                if Academic_record.objects.filter(carreer_code=carreer_code).exists():
                    ac_record = Academic_record.objects.get(carreer_code=carreer_code)

                n_precord = Personal_record.objects.create()
                n_precord.save()
                n_precord.academic_record.add(ac_record)
                n_precord.save()
                n_student.personal_record = n_precord
                n_student.save()
        
        for teacher in teachers:
            rut = int(teacher["rut"].split("-")[0])
            dv = teacher["rut"].split("-")[1]   

            if not Teacher.objects.filter(rut=rut):
                if not User.objects.filter(username=teacher["user"]).exists():
                    n_user = User.objects.create_user(username=teacher["user"],
                                                    password="Unab2020")
                    n_user.save()
                    n_user.first_name = teacher["nombre"]
                    n_user.last_name = teacher["apellidos"]
                    n_user.save()

                else:
                    n_user = User.objects.get(username=teacher["user"])
                
                print "RUT"
                print rut,dv
                n_teacher=Teacher.objects.create(user=n_user,
                                                 rut=rut,
                                                 dv=dv)
                n_teacher.save()
                if Carreer_code.objects.filter(shift=teacher["shift"],carreer=n_carreer).exists():
                    carreer_code = Carreer_code.objects.get(shift=teacher["shift"],carreer=n_carreer)

                if Academic_record.objects.filter(carreer_code=carreer_code).exists():
                    ac_record = Academic_record.objects.get(carreer_code=carreer_code)

                n_precord = Personal_record.objects.create()
                n_precord.save()
                n_precord.academic_record.add(ac_record)
                n_precord.save()
                n_teacher.personal_record = n_precord
                n_teacher.save()

        
    return JsonResponse({"message":"exito"})
