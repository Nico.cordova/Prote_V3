from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic import TemplateView, ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from django.template import RequestContext
from core.models import Student, Teacher, Project, Carreer, Campus, Academic_record, Faculty, Seat, Personal_record, Course

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.translation import ugettext as _
import datetime
import json

from dashboard.login.views import login_needed


@login_needed
def reports_12(request):
    template_name = 'dashboard/reports/12.html'
    carreer = Carreer.objects.all()
    campus = Campus.objects.all()
    seat = Seat.objects.all()
    return render(request, template_name, {'carreer':carreer,
                                            'campus':campus,
                                            'seat':seat})
