from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from dashboard.login.views import login_needed


# Create your views here.

@login_needed
def mail_view(request):
    template_name = 'dashboard/mail/message_view.html'
    return render(request, template_name, {})

@login_needed
def mail_write(request):
    template_name = 'dashboard/mail/message_write.html'
    return render(request, template_name, {})
