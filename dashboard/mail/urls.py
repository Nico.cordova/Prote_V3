from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from dashboard.mail import views


urlpatterns = [

    url(r'^mail_write/$', views.mail_write, name='mail_write'),
    url(r'^mail_view/$', views.mail_view, name='mail_view'),

]
