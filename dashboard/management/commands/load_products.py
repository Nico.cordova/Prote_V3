# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError
from datetime import datetime
from django.core.mail import send_mail


class Command(BaseCommand):
    help = 'Send email test'

    def handle(self, *args, **options):
        
        try:
            send_mail('Test Django', 'Mensaje de test.', 'no-reply@prote.unab.cl', ['miguel@ewok.cl'], fail_silently=False)
            self.stdout.write('## email enviado  ##')
        except Exception as e:
            raise e