from django.conf.urls import url
from django.contrib.auth import views as auth_views
from dashboard.teacher import views


urlpatterns = [
    url(r'^$', views.teacher_list, name='teacher_list'),
    url(r'^add/$', views.teacher_add, name='teacher_add'),
    url(r'^delete/(?P<pk>\d+)$', views.teacher_delete, name='teacher_delete'),
    url(r'^edit/(?P<pk>\d+)$', views.teacher_update, name='teacher_update'),
    url(r'^view/(?P<pk>\d+)$', views.teacher_view, name='teacher_view'),
    url(r'^carreer/$', views.carreer, name='carreer'),
    url(r'^carreer/add$', views.carreer_add, name='carreer_add'),
    url(r'^carreer/delete/(?P<pk>\d+)$', views.carreer_delete, name='carreer_delete'),
    url(r'^carreer/update/(?P<pk>\d+)$', views.carreer_update, name='carreer_update'),
    url(r'^faculty/$', views.faculty, name='faculty'),
    url(r'^faculty/add$', views.faculty_add, name='faculty_add'),
    url(r'^faculty/delete/(?P<pk>\d+)$', views.faculty_delete, name='faculty_delete'),
    url(r'^faculty/update/(?P<pk>\d+)$', views.faculty_update, name='faculty_update'),
    url(r'^campus/$', views.campus, name='campus'),
    url(r'^campus/add$', views.campus_add, name='campus_add'),
    url(r'^campus/delete/(?P<pk>\d+)$', views.campus_delete, name='campus_delete'),
    url(r'^campus/update/(?P<pk>\d+)$', views.campus_update, name='campus_update'),
    url(r'^seat/$', views.seat, name='seat'),
    url(r'^seat/add$', views.seat_add, name='seat_add'),
    url(r'^seat/delete/(?P<pk>\d+)$', views.seat_delete, name='seat_delete'),
    url(r'^seat/update/(?P<pk>\d+)$', views.seat_update, name='seat_update'),
    url(r'^academic_record/$', views.academic_record, name='academic_record'),
    url(r'^academic_record/add$', views.academic_record_add, name='academic_record_add'),
    url(r'^academic_record/delete/(?P<pk>\d+)$', views.academic_record_delete, name='academic_record_delete'),
    url(r'^academic_record/update/(?P<pk>\d+)$', views.academic_record_update, name='academic_record_update'),
    url(r'^get_nrc/$', views.get_nrc, name='get_nrc'),
    url(r'^api/search/$', views.teacher_api_search, name='teacher_api_search'),
]
