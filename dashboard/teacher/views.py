from django.shortcuts import render
from django.contrib.auth.models import User
from django.views.generic.edit import CreateView
from core.models import Student, Teacher, Project, Carreer, Campus, Academic_record, Faculty, Seat, Personal_record, Course, Carreer_code
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils.datastructures import MultiValueDictKeyError
from dashboard.forms import  TeacherForm
from django.forms import ModelForm
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.core import serializers
from django.db.models import Sum
from core.search import get_query
from django.http import JsonResponse
from sorl.thumbnail import get_thumbnail
import json
from dashboard.login.views import login_needed




#@login_needed    
def teacher_view(request,pk) :
    teacher = get_object_or_404(Teacher, pk=pk)
    template_name = 'dashboard/teacher/teacher_view.html'    
    return render(request, template_name, {'teacher': teacher})

#@login_needed    
def teacher_list(request):
    template_name = 'dashboard/teacher/teacher_list.html'
    data = {}
    object_list = Teacher.objects.all().order_by('-id')
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)

        
    return render(request, template_name, data)

#@login_needed
def teacher_delete(request, pk):
    teacher = get_object_or_404(Teacher, pk=pk)
    user=get_object_or_404(User,pk=teacher.user.pk)
    teacher.delete()
    user.delete();
    messages.success(request, _('El profesor fue eliminado correctamente'))
    return HttpResponseRedirect(reverse('teacher_list'))


#@login_needed
def teacher_update(request,pk):
    teacher = get_object_or_404(Teacher, pk=pk)
    template_name = 'dashboard/teacher/teacher_edit.html'

    obj_ac = []
    for value in teacher.personal_record.academic_record.all():
        obj_ac.append(value.pk)
    obj_c = []

    for value in teacher.personal_record.course.all():
        obj_c.append(value.pk)

    course = Course.objects.all()

 

    obj_ar = []
    for campus in Campus.objects.all() :
        obj = {}
        obj["campus_name"] = campus
        obj["carreers"] = []
        for c in Academic_record.objects.filter(campus=campus.pk) :
            obj_carreer = {}
            obj_carreer["code"] = "%s" % (c.carreer_code.code)
            obj_carreer["name"] = "%s" % (c.carreer_code)
            obj_carreer["id"] = "%s" % (c.pk)                      
            obj["carreers"].append(obj_carreer)
        obj_ar.append(obj)

    data = {
        'username' : teacher.user.username,
        'email' : teacher.user.email,
        'password' : teacher.user.password,
        'first_name' : teacher.user.first_name,
        'last_name' : teacher.user.last_name,
        'post_grade' : teacher.post_grade,
        'teacher_type': teacher.teacher_type,
        'description' : teacher.description,
        'rut' : '%s-%s' % (teacher.rut,teacher.dv),
    }
    form = TeacherForm(data, request.FILES)

   
    if request.method == 'POST':
        if form.is_valid():

            password = request.POST['password']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            post_grade = request.POST['post_grade']
            teacher_type = request.POST['teacher_type']
            description = request.POST['description']
            email = request.POST['email']

            
            # import pdb; pdb.set_trace()
            
            # update profile teacher

            teacher.description = description
            teacher.user.first_name = first_name
            teacher.user.last_name = last_name
            teacher.user.email = email
            teacher.teacher_type = teacher_type
            teacher.post_grade = post_grade


            campus_carreer =  request.POST.getlist('select_campus_carreer')
            course = request.POST.getlist('select_course')

            teacher.personal_record.academic_record.set(campus_carreer)
            teacher.personal_record.course.set(course) 

            try:                
                file_avatar = request.FILES['avatar']            
                fs = FileSystemStorage(location='%s/media/avatar' \
                    % settings.BASE_DIR)
                avatar = fs.save(file_avatar.name, file_avatar)
                teacher.avatar = 'avatar/%s' % file_avatar.name
            except MultiValueDictKeyError:
                pass

            try:
                file_cv = request.FILES['cv']            
                fs = FileSystemStorage(location='%s/media/cv' \
                    % settings.BASE_DIR)
                cv = fs.save(file_cv.name, file_cv)
                teacher.cv = 'cv/%s' % file_cv.name            
            except MultiValueDictKeyError:
                pass


            # new password
            if len(password) > 0:
                teacher.user.set_password(password)

            teacher.user.save()    
            teacher.save()
            messages.success(request, _('El profesor fue actualizado correctamente'))
            return HttpResponseRedirect(reverse('teacher_list'))
        else :        
            print form.errors           
    return render(request, template_name, {'form':form, 
                                            'teacher': teacher,
                                            'obj_ar': obj_ar, 
                                            'course': course,
                                            'obj_ac' : json.dumps(obj_ac),
                                            'obj_c' : json.dumps(obj_c)
                                            })


#@login_needed
def teacher_add(request):
    template_name = 'dashboard/teacher/teacher_add.html'
    course = Course.objects.all()
    obj_ar = []
    for campus in Campus.objects.all() :
        obj = {}
        obj["campus_name"] = campus
        obj["carreers"] = []
        for c in Academic_record.objects.filter(campus=campus.pk) :
            obj_carreer = {}
            obj_carreer["code"] = "%s" % (c.carreer_code.code)
            obj_carreer["name"] = "%s" % (c.carreer_code)
            obj_carreer["id"] = "%s" % (c.pk)                      
            obj["carreers"].append(obj_carreer)
        obj_ar.append(obj)

    if request.method == 'POST':
        form = TeacherForm(request.POST, request.FILES)
        if form.is_valid():
            # create account system                          

            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']

            user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
            
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            
            user.first_name = first_name
            user.last_name = last_name

            user.save()

            post_grade = request.POST['post_grade']
            teacher_type = request.POST['teacher_type']        



            # create profile teacher
            rut = request.POST['rut'].replace('-', '').replace('.', '')
            description = request.POST['description']

            teacher = Teacher.objects.create(user=user,
                                             rut=rut[0:-1],
                                             dv=rut[-1],
                                             description=description,
                                             post_grade=post_grade,
                                             teacher_type=teacher_type)

            campus_carreer = request.POST.getlist('select_campus_carreer')
            course = request.POST.getlist('select_course')

            pr = Personal_record.objects.create()
            pr.academic_record.set(campus_carreer)
            pr.course.set(course)
            pr.save()
            teacher.personal_record = pr   


            try:                
                file_avatar = request.FILES['avatar'] 
                fs = FileSystemStorage(location='%s/media/avatar' \
                    % settings.BASE_DIR)
                avatar = fs.save(file_avatar.name, file_avatar)
                teacher.avatar = 'avatar/%s' % file_avatar.name
            except MultiValueDictKeyError:
               pass  

            try:
                file_cv = request.FILES['cv']            
                fs = FileSystemStorage(location='%s/media/cv' \
                    % settings.BASE_DIR)
                cv = fs.save(file_cv.name, file_cv)   
                teacher.cv = 'cv/%s' % file_cv.name
            except MultiValueDictKeyError:
                pass

            teacher.save()
            messages.success(request, _('El profesor fue agregado correctamente'))
            return HttpResponseRedirect(reverse('teacher_list'))
        else :
            print form.errors    
    else:
        form = TeacherForm()
    return render(request, template_name, {'form': form , 'obj_ar':obj_ar,  'course':course})


#@login_needed   
def carreer(request):
    template_name = 'dashboard/teacher/carreer.html'

    data = {}
    object_list = Carreer.objects.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)        
    return render(request, template_name, data)

#@login_needed
def carreer_add(request):
    Carreer.objects.create(name=request.POST['carreername'])       
    messages.success(request, _('El programa fue agregado correctamente'))
    return HttpResponseRedirect(reverse('carreer'))

#@login_needed
def carreer_delete(request, pk):
    carreer = get_object_or_404(Carreer, pk=pk)
    carreer.delete()
    messages.success(request, _('El programa fue eliminado correctamente'))
    return HttpResponseRedirect(reverse('carreer'))

#@login_needed
def carreer_update(request, pk):
    carreer = get_object_or_404(Carreer, pk=pk)
    carreer.name=request.POST['carreername']
    carreer.save()
    messages.success(request, _('El programa fue actualizado correctamente'))
    return HttpResponseRedirect(reverse('carreer'))    


#@login_needed   
def faculty(request):
    template_name = 'dashboard/teacher/faculty.html'

    data = {}
    object_list = Faculty.objects.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)        
    return render(request, template_name, data)

#@login_needed
def faculty_add(request):
    Faculty.objects.create(name=request.POST['facultyname'])       
    messages.success(request, _('La Facultad fue agregada correctamente'))
    return HttpResponseRedirect(reverse('faculty'))

#@login_needed
def faculty_delete(request, pk):
    faculty = get_object_or_404(Faculty, pk=pk)
    faculty.delete()
    messages.success(request, _('La Facultad fue eliminada correctamente'))
    return HttpResponseRedirect(reverse('faculty'))

#@login_needed
def faculty_update(request, pk):
    faculty = get_object_or_404(Faculty, pk=pk)
    faculty.name=request.POST['facultyname']
    faculty.save()
    messages.success(request, _('La Facultad fue actualizada correctamente'))
    return HttpResponseRedirect(reverse('faculty'))     


#@login_needed   
def campus(request):
    template_name = 'dashboard/teacher/campus.html'

    data = {}
    object_list = Campus.objects.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)        
    return render(request, template_name, data)

#@login_needed
def campus_add(request):
    Campus.objects.create(name=request.POST['campusname'])       
    messages.success(request, _('El campus fue agregado correctamente'))
    return HttpResponseRedirect(reverse('campus'))

#@login_needed
def campus_delete(request, pk):
    campus = get_object_or_404(Campus, pk=pk)
    campus.delete()
    messages.success(request, _('El campus fue eliminado correctamente'))
    return HttpResponseRedirect(reverse('campus'))

#@login_needed
def campus_update(request, pk):
    campus = get_object_or_404(Campus, pk=pk)
    campus.name=request.POST['campusname']
    campus.save()
    messages.success(request, _('El campus fue actualizado correctamente'))
    return HttpResponseRedirect(reverse('campus'))     


#@login_needed   
def seat(request):
    template_name = 'dashboard/teacher/seat.html'

    data = {}
    object_list = Seat.objects.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)        
    return render(request, template_name, data)

#@login_needed
def seat_add(request):
    Seat.objects.create(name=request.POST['seatname'])       
    messages.success(request, _('La sede fue agregada correctamente'))
    return HttpResponseRedirect(reverse('seat'))

#@login_needed
def seat_delete(request, pk):
    seat = get_object_or_404(Seat, pk=pk)
    seat.delete()
    messages.success(request, _('La sede fue eliminada correctamente'))
    return HttpResponseRedirect(reverse('seat'))

#@login_needed
def seat_update(request, pk):
    seat = get_object_or_404(Seat, pk=pk)
    seat.name=request.POST['seatname']
    seat.save()
    messages.success(request, _('La sede fue actualizada correctamente'))
    return HttpResponseRedirect(reverse('seat'))     



class ARForm(ModelForm):
    class Meta:
        model = Academic_record
        fields = ['faculty', 'seat', 'campus', 'carreer_code']
        labels = {
            "faculty": "Facultad",
            "seat": "Sede",
            "campus": "Campus",
            "carreer": "Programa"
        }


#@login_needed   
def academic_record(request):
    template_name = 'dashboard/teacher/academic_record.html'
    
    data = {}
    object_list = Academic_record.objects.all()
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)        
    return render(request, template_name, data)

#@login_needed
def academic_record_add(request,
                template_name = 'dashboard/teacher/academic_record_add.html'):    
    form = ARForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('academic_record')
    messages.success(request, _('El Registro academico fue agregado \
                                                                correctamente'))
    return render(request, template_name, {'form': form})

#@login_needed
def academic_record_delete(request, pk):
    academic_record = get_object_or_404(Academic_record, pk=pk)
    academic_record.delete()
    messages.success(request, _('El Registro academico fue eliminado \
                                                                correctamente'))
    return HttpResponseRedirect(reverse('academic_record'))




#@login_needed
def academic_record_update(request, pk,
                template_name='dashboard/teacher/academic_record_edit.html'):
    academic_record = get_object_or_404(Academic_record, pk=pk)
    carreer = get_object_or_404(Academic_record, pk=academic_record.carreer.pk)
    form = ARForm(request.POST or None, instance=academic_record)
    if form.is_valid():
        form.save()
        messages.success(request, _('El Registro academico fue actualizado \
                                                                correctamente'))
        return redirect('academic_record')
    return render(request, template_name, {'form':form,'academic_record':academic_record})


#@login_needed    
def get_nrc(request) :
    if request.method == 'POST':
        data = request.POST.getlist('data[]')        
        pr = Personal_record.objects.filter(academic_record__in=data)
        nrc = Course.objects.filter(personal_record__in=pr).annotate(nrc_count=Sum('nrc'))
        nrc = nrc.order_by('nrc')
    qs_json = serializers.serialize('json', nrc)
    return JsonResponse(qs_json,safe=False)



#@login_needed
def teacher_api_search(request) :

    results_search = []
    json = {}

    query_string = ''
    found_entries = None
    
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']        
        entry_query = get_query(query_string, ['user__first_name', 'user__last_name','rut'])        
        found_entries = Teacher.objects.filter(entry_query)
        # found_entries = Student.objects.filter(entry_query).order_by('-created')

        for entrie in found_entries :
            avatar = get_thumbnail(entrie.avatar, '50x50', crop='center', quality=99)
            results_search.append({
                'student_id' : entrie.pk,
                'first_name' : entrie.user.first_name,
                'last_name' : entrie.user.last_name,
                'email' : entrie.user.email,
                'rut' : '%s-%s' % (entrie.rut,entrie.dv),
                'avatar' : avatar.url,
            })

    json['status'] = True
    json['error'] = None
    json['data'] = {
        'students' : results_search
    }

    return JsonResponse(json)    