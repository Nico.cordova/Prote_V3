from django.contrib import messages
from django.shortcuts import render_to_response, render, redirect, get_object_or_404
from core.models import *
from django import forms
from django.contrib.auth.decorators import login_required
from dashboard.forms import ProjectProposeForm
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext as _
import json
from dashboard.login.views import login_needed



# Create your views here.

@login_needed
def project_propose(request):
    template_name = 'dashboard/project/project_propose.html'
    if request.method == 'POST':
        print request 
        form = ProjectProposeForm(request.POST)
        author = request.user
        name = request.POST['title']
        vacancy = request.POST['vacancy']
        description = request.POST['description']
        period = get_object_or_404(Period, pk=request.POST['period'])
        if request.POST['teacher'] == '':
            guide_teacher = get_object_or_404(Teacher, rut=0)
        else:
            guide_teacher = get_object_or_404(Teacher, pk=request.POST['teacher'])
        tags = []
        for tag in request.POST["tags"].split(','):
            try:
                new_tag = Tag.objects.get(tag_name=tag)
                tags.append(new_tag)
            except:
                new_tag = Tag.objects.create(tag_name=tag)
                new_tag.save()
                tags.append(new_tag)
        faculty = get_object_or_404(Faculty, pk=request.POST['faculty'])
        carreer = get_object_or_404(Carreer, pk=request.POST['carreer'])
        project = Project.objects.create(name=name,
                                         date=datetime.datetime.now(),
                                         vacancy_total=vacancy,
                                         vacancy_available=vacancy,
                                         description=description,
                                         author=author,
                                         guide_teacher=guide_teacher,
                                         period=period,
                                         faculty=faculty,
                                         carreer=carreer,
                                        )
        project.save()
        for tag in tags:
            project.tags.add(tag)
        for pk in request.POST.getlist('student'):
            student = get_object_or_404(Student, pk=pk)
            postulation = Postulation.objects.create(postulant=student,
                                                     date=datetime.datetime.now(),
                                                     project=project,
                                                     postulation_type="PR"
                                                    )
            postulation.save()
        return HttpResponseRedirect(reverse_lazy('project_propose'))
    else:
        form = ProjectProposeForm()
        projects = Project.objects.all()
        projects = projects[::-1]
        num_proposal = 0
        num_accepted = 0
        num_rejected = 0
        num_revision = 0
        for project in projects:
            if project.author == request.user:
                num_proposal += 1
                if project.status == "ER" or project.status == "EP":
                    num_revision += 1
                elif project.status == "AC":
                    num_accepted += 1
                elif project.status == "RE":
                    num_rejected += 1
        data = {}
        data['rejected'] = num_rejected
        data['accepted'] = num_accepted
        data['revision'] = num_revision
        data['proposals'] = num_proposal
        data['object_list'] = projects
        data['form'] = form
        return render(request, template_name, data)

@login_needed
def project_postulate(request):
    projects = Project.objects.all()
    student = get_object_or_404(Student, user=request.user)
    data = {}
    data = {'Projects': [],
           }
    student_carreer = []
    for x in student.personal_record.academic_record.all():
        student_carreer.append(x.carreer_code.carreer)
    for project in projects:
        if project.status == "AC":
            if project.period.start < datetime.date.today():
                if project.period.finish > datetime.date.today():
                    if project.carreer in student_carreer:
                        data['Projects'].append(project)
    template_name = 'dashboard/project/project_postulate.html'
    return render(request, template_name, data)

@login_needed
def project_postulation(request, pk):
    try:
        student = get_object_or_404(Student, user=request.user)
        courses = student.personal_record.course.all()
    except:
        messages.error(request, "Debes ser un estudiante para postular.")
        return HttpResponseRedirect(reverse_lazy('project_postulate'))
    project = get_object_or_404(Project, pk=pk)
    print project.section
    if student.project != None:
        messages.error(request, "Ya tienes un projecto asignado.")
        return HttpResponseRedirect(reverse_lazy('project_postulate'))

    elif Postulation.objects.filter(postulant=student).filter(project=project).exists():
        messages.error(request, "Ya postulaste a este proyecto")
        return HttpResponseRedirect(reverse_lazy('project_postulate'))

    elif project.section == None:
        new_postulation = Postulation.objects.create(postulant=student,
                                                     date=datetime.datetime.now(),
                                                     project=project)
        new_postulation.save()
        messages.success(request, _('Postulacion a proyecto exitosa'))
        return HttpResponseRedirect(reverse_lazy('project_postulate'))

    elif courses[0] not in project.section.course.all():
        messages.error(request, "Este proyecto no es para tu asignatura.")
        return HttpResponseRedirect(reverse_lazy('project_postulate'))


@login_needed
def project_postulation_approve(request, pk):
    postulation = get_object_or_404(Postulation, pk=pk)
    postulation.project.vacancy_available -= 1
    if postulation.project.vacancy_available == 0:
        postulation.project.status = "EC"
        project_postulations = Postulation.objects.filter(project=postulation.project)
        for post in project_postulations:
            post.delete()
    postulation.project.save()
    print postulation.project.guide_teacher
    postulation.postulant.project = postulation.project
    for x in postulation.postulant.personal_record.course.all():
        aux = x
    section = Section.objects.create(code=555,
                                    course=aux,
                                    n_nrc=222,
                                    teacher=postulation.project.guide_teacher)
    postulation.postulant.save()
    postulation.project.section = section
    postulation.project.save()
    message = "Tu postulacion al proyecto %s fue aprobada!" % (postulation.project.name)
    receipts = set()
    receipts.add(postulation.postulant.user)
    alert_message = Mail.objects.create()
    alert_message.save()
    alert_message.send_system(receipts, message, "'project_propose'")
    postulation.delete()

    return HttpResponseRedirect(reverse_lazy('project_postulate_list'))

@login_needed
def project_postulation_denial(request, pk):
    postulation = get_object_or_404(Postulation, pk=pk)
    message = "Tu postulacion al proyecto %s fue rechazada!" % (postulation.project.name)
    receipts = set()
    receipts.add(postulation.postulant.user)
    alert_message = Mail.objects.create()
    alert_message.save()
    alert_message.send_system(receipts, message, "'project_propose'")
    postulation.delete()

    return HttpResponseRedirect(reverse_lazy('project_postulate_list'))

@login_needed
def project_delete(request, pk):
    project = get_object_or_404(Project, pk=pk)
    project.delete()
    return HttpResponseRedirect(reverse_lazy('project_propose'))

@login_needed
def project_refuse(request, pk):
    project = get_object_or_404(Project, pk=pk)
    project.status = "RE"
    project.save()
    message = "Tu proyecto %s con el profesor guia %s ha sido rechazado!" % (project.name,
                                                                            project.guide_teacher)
    receipts = set()
    receipts.add(project.author)
    alert_message = Mail.objects.create()
    alert_message.save()
    alert_message.send_system(receipts, message, "'project_propose'")
    return HttpResponseRedirect(reverse_lazy('project_propose_list'))

@login_needed
def project_approve(request, pk):
    project = get_object_or_404(Project, pk=pk)
    project.status = "AC"
    project.save()

    postulations_pre = Postulation.objects.filter(project=project).filter(postulation_type="PR")
    for postulation in postulations_pre:
        if not project.section:
            try:
                subjects = postulation.postulant.personal_record.course.all()                
                section = Section.objects.create(code=522,
                                                 course=subjects.first(),
                                                 n_nrc=200,
                                                 teacher=project.guide_teacher
                                                )
            except Exception,e:
                print e
                

            project.section = section
            project.save()
        if project.vacancy_available > 0:
            postulation.postulant.project = project
            postulation.postulant.save()
            message = "El proyecto %s con el profesor %s fue aprobado junto con tu postulacion." % (project.name,
                                                                                                    project.guide_teacher)
            receipt = set()
            receipt.add(postulation.postulant.user)
            alert_message = Mail.objects.create()
            alert_message.save()
            alert_message.send_system(receipt, message)
            project.vacancy_available -= 1
            project.save()
            postulation.delete()

    #Sends alert to the project author
    message = "Tu proyecto %s con el profesor guia %s ha sido aprobado!" % (project.name,
                                                                            project.guide_teacher)
    receipts = set()
    receipts.add(project.author)
    alert_message = Mail.objects.create()
    alert_message.save()
    alert_message.send_system(receipts, message, "'project_propose'")

    return HttpResponseRedirect(reverse_lazy('project_propose_list'))

@login_needed
def project_postulate_list(request):
    template_name = 'dashboard/project/project_postulate_list.html'
    data = {'Projects_AC':[],
           }
    for postulation in Postulation.objects.all():
        if postulation.project not in data['Projects_AC'] and postulation.postulation_type == "PO":
            data['Projects_AC'].append(postulation.project)
    return render(request, template_name, data)

@login_needed
def project_propose_list(request):
    projects = Project.objects.all()
    template_name = 'dashboard/project/project_propose_list.html'
    data = {'Projects_RE': [],
            'Projects_AC': [],
            'Projects_ER': [],
            'object_list': [],
           }
    pro = projects[::-1]
    for x in pro:
        if x.status == "RE":
            data['Projects_RE'].append(x)
        elif x.status == "AC":
            data['Projects_AC'].append(x)
        elif x.status == "ER" or x.status == "EP":
            data['Projects_ER'].append(x)
        else:
            data['object_list'].append(x)
    return render(request, template_name, data)

@login_needed
def project_detail(request):
    if request.method == 'POST':
        project_id = request.POST['project_id']
        response_data = {}
        # project = Project(text=post_text, author=request.user)
        # post.save()
        project = Project.objects.get(pk=project_id)
        postulations = Postulation.objects.filter(project=project)
        response_data['autor'] = project.author.first_name + " " + project.author.last_name
        response_data['descripcion'] = project.description
        response_data['porcentaje'] = (100 * project.vacancy_available) / project.vacancy_total
        response_data['titulo'] = project.name
        response_data['carrera'] = project.carreer.name
        response_data['facultad'] = project.faculty.name
        response_data['profesor'] = project.guide_teacher.user.first_name \
                                    + " " + project.guide_teacher.user.last_name
        response_data['cupos_total'] = project.vacancy_total
        response_data['cupos_disponible'] = project.vacancy_available
        # response_data['postpk'] = post.pk
        # response_data['text'] = post.text
        # response_data['created'] = post.created.strftime('%B %d, %Y %I:%M %p')
        html = ""
        for x in postulations:
            a = """<tr>
                        <td class="col-md-2"><img alt="image" class="img-circle img-responsive" src="/media/""" + str(x.postulant.avatar)+ """ "> </td>
                        <td class="col-md-7"><strong>""" + x.postulant.user.first_name + " " + x.postulant.user.last_name  + """</strong></td>
                        <td class="col-md-3">
                            <a  href="/dashboard/project/project_denial/""" + str(x.pk) + """ " class="btn btn-danger btn-sm"><i class="fa fa-times"></i></a>
                            <a href="/dashboard/project/project_approve/""" + str(x.pk) + """ " class="btn btn-primary btn-sm"><i class="fa fa-check"></i></a>
                        </td>
                    </tr>"""
            html += a
        response_data['postulaciones'] = html
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )
    else:
        return HttpResponse(
            json.dumps({"error": "this isn't happening"}),
            content_type="application/json"
        )

def teacher_designation_request(request, pk_profe, pk_project):
    target = get_object_or_404(Teacher, pk=pk_profe)
    project = get_object_or_404(Project, pk=pk_project)
    new_dessignation = Designation.objects.create(target_teacher=target,
                                                  project=project,
                                                 )
    new_dessignation.save()
    receipt = set()
    receipt.add(target.user)
    message = "Tienes una nueva designacion por revisar del projecto %s" % (project.name)
    alert_message = Mail.objects.create()
    alert_message.save()
    alert_message.send_system(receipt, message)
    project.status = "EP"
    project.save()
    return HttpResponseRedirect(reverse_lazy('project_propose_list'))

def teacher_designation_reject(request, pk):
    designation = get_object_or_404(Designation, pk=pk)
    project = get_object_or_404(Project, pk=designation.project.pk)
    project.status = "ER"
    designation.status = "RE"
    designation.save()
    project.save()
    return HttpResponseRedirect(reverse_lazy('project_propose'))

def teacher_designation_accept(request, pk):
    designation = get_object_or_404(Designation, pk=pk)
    project = get_object_or_404(Project, pk=designation.project.pk)
    project.guide_teacher = designation.target_teacher
    designation.status = "AC"
    designation.save()
    project.status = "AC"
    project.save()
    return HttpResponseRedirect(reverse_lazy('project_propose'))

def teacher_designation(request, pk):
    teachers = Teacher.objects.all()
    project = get_object_or_404(Project, pk=pk)
    template_name = 'dashboard/project/project_teacher_assignment.html'
    return render(request, template_name, {'teachers':teachers , 'project':project})


def my_project(request):
    template_name = 'dashboard/project/project_status.html'
    data = {}
    if request.user.student:
        Project_obj = get_object_or_404(Project, pk=request.user.student.project.pk)
        data['project'] = Project_obj
        data['rubric'] = Project_obj.section.course.rubric
        cont = 0
        total = 0
        sessions = Session.objects.filter(rubric=data['rubric'])
        students = Student.objects.filter(project=Project_obj)
        sessions_obj = []
        for z in sessions:
            sessions_obj.append(z)
            total+=1
        data['students'] = students
        student_evaluations = StudentEvaluation.objects.filter(student=request.user.student)
        student_hito_evaluations = StudentHitoEvaluation.objects.filter(student=request.user.student)
        for x in student_evaluations:
            sessions_obj[int(x.session.number) - 1] = x
            cont += 1
        for z in student_hito_evaluations:
            sessions_obj[int(z.session.number) - 1] = z
            cont += 1
        if total == 0:
            percentage = 0
        else:
            percentage = (float(cont)/total) * 100
        data["retro"] = Retro.objects.filter(student=request.user.student,project=Project_obj)
        data['percentage'] = int(percentage)
        data['sessions'] = sessions_obj

    return render(request, template_name, data)

def teacher_designations_list(request):
    designations = Designation.objects.all()
    template_name = 'dashboard/teacher/teacher_designations_list.html'
    return render(request, template_name, {'designations':designations})
