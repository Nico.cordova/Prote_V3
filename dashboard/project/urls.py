from django.conf.urls import url
from django.contrib.auth import views as auth_views
from dashboard.project import views

urlpatterns = [

    url(r'^delete/(?P<pk>\d+)$', views.project_delete, name='project_delete'),
    url(r'^refuse/(?P<pk>\d+)$', views.project_refuse, name='project_refuse'),
    url(r'^approve/(?P<pk>\d+)$', views.project_approve, name='project_approve'),
    url(r'^propose/$', views.project_propose, name="project_propose"),
    url(r'^my_project/$', views.my_project, name="my_project"),
    url(r'^postulate/$', views.project_postulate, name="project_postulate"),
    url(r'^postulate/(?P<pk>\d+)$', views.project_postulation, name='project_postulation'),
    url(r'^propose_list/$', views.project_propose_list, name="project_propose_list"),
    url(r'^propose_list/teacher_assignment/(?P<pk>\d+)$', views.teacher_designation, name="teacher_dessignment"),
    url(r'^detail/$', views.project_detail, name="project_detail"),
    url(r'^postulate_list/$', views.project_postulate_list, name="project_postulate_list"),
    url(r'^project_denial/(?P<pk>\d+)$', views.project_postulation_denial, name='project_postulation_denial'),
    url(r'^project_approve/(?P<pk>\d+)$', views.project_postulation_approve, name='project_postulation_approve'),
    url(r'^propose_list/assign/(?P<pk_profe>(\d+))/(?P<pk_project>(\d+))/$', views.teacher_designation_request, name='teacher_assignment'),
    url(r'^teacher/designations_list/$', views.teacher_designations_list, name="teacher_designations_list"),
    url(r'^project/designation_accept/(?P<pk>\d+)$', views.teacher_designation_accept, name='project_designation_accept'),
    url(r'^project/designation_reject/(?P<pk>\d+)$', views.teacher_designation_reject, name='project_designation_reject'),
]
