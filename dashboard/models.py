from __future__ import unicode_literals

from django.db import models

from mptt.models import MPTTModel, TreeForeignKey


TYPE_ACCESS_CHOICES = (
    ('G','Global'),
    ('T','Teacher'),
    ('S','Student'),
)
TYPE_ACCESS_DEFAULT = 'G'

class Sidebar(MPTTModel):
    name = models.CharField(max_length=50, unique=True)
    icon = models.CharField(max_length=50, blank=True,null=True)
    href = models.CharField(max_length=144)
    access = models.CharField(
        max_length=1,
        choices=TYPE_ACCESS_CHOICES,
        default=TYPE_ACCESS_DEFAULT
    )
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children', db_index=True)

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']
