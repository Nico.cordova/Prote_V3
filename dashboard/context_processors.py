""" Context proccesor for dashboard app """

from dashboard.models import Sidebar
from core.models import UserProfile, Teacher, Mail, Student
from django.contrib.auth.decorators import login_required
from core.models import Teacher

def global_data(request):

    data = {}
    user_type=None
    if request.user.is_authenticated :
        try:
            user_data = UserProfile.objects.get(pk=request.user.pk)
        except UserProfile.DoesNotExist:
            user_data = {}

        try:

            obj = Teacher.objects.get(user=request.user)
            user_avatar = obj.avatar
            user_id = obj.id
            user_type = Teacher.objects.get(user=request.user).get_teacher_type_display
        except Teacher.DoesNotExist:
            try:
                obj = Student.objects.get(user=request.user)
                user_avatar = obj.avatar
                user_id = obj.id
                user_type=None

            except Student.DoesNotExist:
                user_avatar = 'no-image.jpg'
                user_id = None

        received_mails = Mail.objects.get_received_mail(request.user).all()
        sent_mails = Mail.objects.get_sent_mail(request.user).all()
        system_alerts = Mail.objects.get_system_alerts(request.user).all()
        system_alerts = system_alerts[::-1]

        data = {
            'user_logged': '%s %s' % (user_data.first_name, user_data.last_name),
            'type_user' : user_type,
            'user_id' : user_id,
            'user_avatar': user_avatar,
            'is_teacher' : user_data.is_teacher(),
            'is_student' : user_data.is_student(),
            'is_teacher_up' : user_data.is_teacher_up(),
            'received_mails' : received_mails,
            'sent_mails' : sent_mails,
            'system_alerts' : system_alerts,
        }
    return data


def menu_sidebar(request):
    return {'menu_items' : Sidebar.objects.all()}
