from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.contrib import messages
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.utils.translation import ugettext as _
from django import forms
from dashboard.forms import  StudentForm
from core.models import Student, Campus, Carreer, Academic_record, Personal_record,Course,Teacher
from core.search import get_query
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.db.models import Sum
import json
import os
from sorl.thumbnail import get_thumbnail
from dashboard.login.views import login_needed


@login_needed
def student_update(request,pk) :
    student = get_object_or_404(Student, pk=pk)
    template_name = 'dashboard/student/student_edit.html'
    obj_ac = []
    for value in student.personal_record.academic_record.all():
        obj_ac.append(value.pk)
    obj_c = []
    for value in student.personal_record.course.all():
        obj_c.append(value.pk)
    print obj_c    
    course = Course.objects.all()
        
    obj_ar = []
    for campus in Campus.objects.all() :
        obj = {}
        obj["campus_name"] = campus
        obj["carreers"] = []
        for c in Academic_record.objects.filter(campus=campus.pk) :
            obj_carreer = {}
            obj_carreer["code"] = "%s" % (c.carreer_code.code)
            obj_carreer["name"] = "%s" % (c.carreer_code)
            obj_carreer["id"] = "%s" % (c.pk)                      
            obj["carreers"].append(obj_carreer)
        obj_ar.append(obj)
    data = {
        'email' : student.user.email,
        'first_name' : student.user.first_name,
        'last_name' : student.user.last_name,
        'rut' : '%s-%s' % (student.rut,student.dv),
        'description' : student.description,
    }

    form =  StudentForm(data, request.FILES)   
    if request.method == 'POST':
        if form.is_valid():
            email = request.POST['email']
            password = request.POST['password']
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']           
            
            description = request.POST['description']

            student.description = description
            student.user.first_name = first_name
            student.user.last_name = last_name
            student.user.email = email

            # import pdb; pdb.set_trace()
            campus_carreer =  request.POST.getlist('select_campus_carreer')
            course = request.POST.getlist('select_course')

            student.personal_record.academic_record.set(campus_carreer)
            student.personal_record.course.set(course)
            
            try:                
                file_avatar = request.FILES['avatar']            
                fs = FileSystemStorage(location='%s/media/avatar' \
                    % settings.BASE_DIR)
                avatar = fs.save(file_avatar.name, file_avatar)
                student.avatar = 'avatar/%s' % file_avatar.name
            except MultiValueDictKeyError:
                pass

            try:
                file_transcript = request.FILES['transcript']            
                fs = FileSystemStorage(location='%s/media/transcript' \
                    % settings.BASE_DIR)
                transcript = fs.save(file_transcript.name, file_transcript)
                student.transcript = 'transcript/%s' % file_transcript.name            
            except MultiValueDictKeyError:
                pass

            # new password
            if len(password) > 0 :
                student.user.set_password(password)           

            student.user.save()            
            student.save()            
            messages.success(request,
                                _('El alumno fue actualizado correctamente'))
            
            return HttpResponseRedirect(reverse('student_list'))
        else :        
            print form.errors
    return render(request, template_name, {'form':form,
                                            'student': student,
                                            'obj_ar': obj_ar, 
                                            'course': course, 
                                            'obj_c' : json.dumps(obj_c),
                                            'obj_ac' : json.dumps(obj_ac)})


@login_needed    
def student_view(request,pk) :
    student = get_object_or_404(Student, pk=pk)
    template_name = 'dashboard/student/student_view.html'    
    return render(request, template_name, {'student': student})

@login_needed    
def get_student_campus(request) :
    campus = Campus.objects.all();
    qs_json = serializers.serialize('json', campus)
    return JsonResponse(qs_json,safe=False)

@login_needed    
def get_student_carreer(request) :
    carreer = Carreer.objects.filter(campus=request.POST['pk']);
    qs_json = serializers.serialize('json', carreer)
    return JsonResponse(qs_json,safe=False)

@login_needed
def student_list(request):
    template_name = 'dashboard/student/student_list.html'
    
    # teacher_request = Teacher.personal_record.objects.filter(user=request.user)
    
    data = {}
    object_list = Student.objects.all().order_by('-id')
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    
    try:
        data['object_list'] = paginator.page(page)
    except PageNotAnInteger:
        data['object_list'] = paginator.page(1)
    except EmptyPage:
        data['object_list'] = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


@login_needed
def student_delete(request, pk):
    student = get_object_or_404(Student, pk=pk)
    user=get_object_or_404(User,pk=student.user.pk)
    student.delete()
    user.delete();
    messages.success(request, _('El alumno fue eliminado correctamente'))
    return HttpResponseRedirect(reverse('student_list'))


@login_needed
def student_add(request):
    template_name = 'dashboard/student/student_form.html'
    course = Course.objects.all()
    obj_ar = []
    for campus in Campus.objects.all() :
        obj = {}
        obj["campus_name"] = campus
        obj["carreers"] = []
        for c in Academic_record.objects.filter(campus=campus.pk) :
            obj_carreer = {}
            obj_carreer["code"] = "%s" % (c.carreer_code.code)
            obj_carreer["name"] = "%s" % (c.carreer_code)
            obj_carreer["id"] = "%s" % (c.pk)                      
            obj["carreers"].append(obj_carreer)
        obj_ar.append(obj)
    if request.method == 'POST':
        form = StudentForm(request.POST, request.FILES)
        
        if form.is_valid():
            
            username = request.POST['username']
            email = request.POST['email']
            password = request.POST['password']

            first_name = request.POST['first_name']
            last_name = request.POST['last_name']

            user = User.objects.create_user(username=username,
                                            email=email,
                                            password=password)
            user.first_name = first_name
            user.last_name = last_name

            user.save()
            
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            pga = request.POST['pga']
            ac = request.POST['select_campus_carreer']
            course = request.POST['select_course']

            rut = request.POST['rut'].replace('-', '').replace('.', '')
            description = request.POST['description']

            student = Student.objects.create(user=user,
                                             rut=rut[0:-1],
                                             dv=rut[-1],
                                             description=description,
                                             pga=pga)

            campus_carreer = request.POST.getlist('select_campus_carreer')
            course = request.POST.getlist('select_course')

            pr = Personal_record.objects.create()

            pr.academic_record.set(campus_carreer)
            pr.course.set(course)
            pr.save()
            student.personal_record = pr
            try:                
                file_avatar = request.FILES['avatar'] 
                fs = FileSystemStorage(location='%s/media/avatar' \
                    % settings.BASE_DIR)
                avatar = fs.save(file_avatar.name, file_avatar)
                student.avatar = 'avatar/%s' % file_avatar.name
            except MultiValueDictKeyError:
               pass  

            try:
                file_transcript = request.FILES['transcript']            
                fs = FileSystemStorage(location='%s/media/transcript' \
                    % settings.BASE_DIR)
                transcript = fs.save(file_transcript.name, file_transcript)   
                student.transcript = 'transcript/%s' % file_transcript.name
            except MultiValueDictKeyError:
                pass

            student.save();
            messages.success(request, _('El alumno fue agregado correctamente'))
            return HttpResponseRedirect(reverse('student_list'))
        else:
            print form.errors    
    else:
        form = StudentForm()
    return render(request, template_name, {'form': form,'obj_ar':obj_ar,'course':course})


def student_api_search(request) :

    results_search = []
    json = {}

    query_string = ''
    found_entries = None
    
    if ('q' in request.GET) and request.GET['q'].strip():
        query_string = request.GET['q']        
        entry_query = get_query(query_string, ['user__first_name', 'user__last_name','rut'])        
        found_entries = Student.objects.filter(entry_query)
        # found_entries = Student.objects.filter(entry_query).order_by('-created')

        for entrie in found_entries :
            avatar = get_thumbnail(entrie.avatar, '50x50', crop='center', quality=99)
            results_search.append({
                'student_id' : entrie.pk,
                'first_name' : entrie.user.first_name,
                'last_name' : entrie.user.last_name,
                'email' : entrie.user.email,
                'rut' : '%s-%s' % (entrie.rut,entrie.dv),
                'avatar' : avatar.url,
            })

    json['status'] = True
    json['error'] = None
    json['data'] = {
        'students' : results_search
    }

    return JsonResponse(json)

@login_needed    
def get_nrc(request) :
    if request.method == 'POST':
        data = request.POST.getlist('data[]')        
        pr = Personal_record.objects.filter(academic_record__in=data)
        nrc = Course.objects.filter(personal_record__in=pr).annotate(nrc_count=Sum('nrc'))
        nrc = nrc.order_by('nrc')
    qs_json = serializers.serialize('json', nrc)
    return JsonResponse(qs_json,safe=False)