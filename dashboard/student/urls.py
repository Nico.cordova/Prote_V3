from django.conf.urls import url
from django.contrib.auth import views as auth_views
from dashboard.student import views


  #urls student
urlpatterns = [
    url(r'^$', views.student_list, name='student_list'),
    url(r'^add/$', views.student_add, name='student_add'),

    url(r'^api/search/$', views.student_api_search, name='student_api_search'),
    # url(r'^search/advanced/$', views.student_search_advanced, name='student_search_advanced'),
    url(r'^delete/(?P<pk>\d+)$', views.student_delete, name='student_delete'),
    url(r'^edit/(?P<pk>\d+)$', views.student_update, name='student_update'),
    url(r'^view/(?P<pk>\d+)$', views.student_view, name='student_view'),
    url(r'^get_student_campus/$', views.get_student_campus, name='get_student_campus'),
    url(r'^get_student_carreer/$', views.get_student_carreer, name='get_student_carreer'),
    url(r'^get_nrc/$', views.get_nrc, name='get_nrc'),
]