from django.contrib import admin

from mptt.admin import MPTTModelAdmin, DraggableMPTTAdmin

from dashboard.models import Sidebar


admin.site.register(Sidebar, DraggableMPTTAdmin)

