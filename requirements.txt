Django==1.10.5
MySQL-python==1.2.5
argparse==1.2.1
distribute>=0.6.24
honcho==0.7.1
wsgiref==0.1.2
django-mptt==0.8.7
django-bootstrap-form==3.2.1
django-localflavor==1.4.1
Pillow==4.0.0
django-bootstrap-pagination==1.6.2
django-summernote==0.8.6
sorl-thumbnail==12.4a1
pyyaml  ==3.12