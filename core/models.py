#encoding:utf-8
from __future__ import unicode_literals

from django.shortcuts import get_object_or_404
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible
from core.manager import TeacherManager, ProfileManager, MailManager
import os
import datetime


class User_Profile(models.Model):
    rut = models.CharField(max_length=20,null=True,blank=True)
    nombre = models.CharField(max_length=200,null=True,blank=True)
    perfil_activo = models.CharField(max_length=2)
    def __str__(self):
        return self.nombre

    
    
    
##################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################    
    
""" Tablas Viejas """

TEACHER_TYPES_CHOICES = (
    ('DO', 'Docente'),
    ('SA', 'Secretario Académico'),
    ('DC', 'Director de Carrera'),

)

POST_GRADES_CHOICES = (
    ('PH', 'Doctorado'),
    ('MA', 'Magister'),
    ('LI', 'Licenciado'),

)

POST_GRADES_DEFAULT = 'LI'
TEACHER_TYPES_DEFAULT = 'DO'

MAIL_TYPE_CHOICES = [
    ('SA', 'System Alert'),
    ('PP', 'Peer to Peer')
]

EVALUATION_TYPE_CHOICES = [
    ('OB', 'Objetivo'),
    ('AT', 'Actividad'),
    ('AV', 'Avance')
]

STATUS_MAIL_TYPE_CHOICES = [
    ('AC', 'Active'),
    ('DC', 'Desactive')
]

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)

class UserProfile(User):

    def is_teacher_up(self):

        try:
            teacher = Teacher.objects.get(user=self)

            if teacher.teacher_type in ('DC', 'SA'):
                return True

        except Teacher.DoesNotExist:
            return False

    def is_teacher(self):
        try:
            Teacher.objects.get(user=self)

        except Teacher.DoesNotExist:
            return False

        return True

    def is_student(self):
        try:
            Student.objects.get(user=self)

        except Student.DoesNotExist:
            return False

        return True

    objects = ProfileManager()

    class Meta:
        proxy = True


@python_2_unicode_compatible
class Period(models.Model):
    user_friendly = models.CharField(max_length=60)
    code = models.CharField(max_length=6,blank=True,null=True)
    start = models.DateField(blank=True, null=True)
    finish = models.DateField(blank=True, null=True)
    def __str__(self):
        return self.user_friendly

@python_2_unicode_compatible
class Tag(models.Model):
    tag_name = models.CharField(max_length=45)

    def __str__(self):
        return self.tag_name

@python_2_unicode_compatible
class Faculty(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Seat(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Campus(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Shift(models.Model):
    name = models.CharField(max_length=60)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Carreer(models.Model):
    name = models.CharField(max_length=70)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Carreer_code(models.Model):
    carreer = models.ForeignKey(Carreer)
    code = models.CharField(max_length=45,null=True)
    shift = models.CharField(max_length=45,null=True)
    def __str__(self):
        return ("%s - %s")%(self.carreer,self.shift)

@python_2_unicode_compatible
class Abet(models.Model):
    name = models.CharField(max_length=60, null=True)
    author = models.ForeignKey(User, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    carreer = models.OneToOneField(Carreer, null=True, blank=True)
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Academic_record(models.Model):
    faculty = models.ForeignKey(Faculty,blank=True, null=True)
    seat = models.ForeignKey(Seat,blank=True, null=True)
    campus = models.ForeignKey(Campus,blank=True, null=True)
    carreer_code = models.ForeignKey(Carreer_code,blank=True, null=True)
    def __str__(self):
        return "{0}-{1}-{2}-{3}-{4}".format(self.faculty.name, self.seat.name,
                                    self.campus.name, self.carreer_code.carreer.name, self.carreer_code.shift)

@python_2_unicode_compatible
class Personal_record(models.Model):
    academic_record = models.ManyToManyField(Academic_record, blank=True)
    course = models.ManyToManyField("Course", blank=True)
    def __str__(self):
        return '->%s' % self.id

@python_2_unicode_compatible
class Section(models.Model):
    code = models.CharField(max_length=60,blank=True,null=True)
    course = models.ForeignKey("Course")
    n_nrc = models.CharField(max_length=32)
    teacher = models.ForeignKey("Teacher")
    def __str__(self):
        return self.code
    
@python_2_unicode_compatible
class Retro(models.Model):
    text = models.CharField(max_length=60,blank=True,null=True)
    student = models.ForeignKey("Student")
    project = models.ForeignKey("Project")
    def __str__(self):
        return self.text

@python_2_unicode_compatible
class Rubric(models.Model):
    name = models.CharField(max_length=60)
    author = models.ForeignKey(User, null=True)
    date = models.DateTimeField(blank=True, null=True)
    carreer = models.ForeignKey(Carreer, null=True)
    description = models.CharField(max_length=300, null=True, blank=True)
    status = models.CharField(max_length=2,
                              choices=STATUS_MAIL_TYPE_CHOICES,
                              default="DC")
    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Course(models.Model):
    name = models.CharField(max_length=60,blank=True,null=True)
    subject = models.CharField(max_length=60,null=True,blank=True)
    code = models.CharField(max_length=10,blank=True,null=True)
    carreer_code = models.ForeignKey(Carreer_code,blank=True,null=True)
    abet_rubric = models.OneToOneField("AbetRubric", blank=True, null=True, on_delete=models.SET_NULL)
    rubric = models.OneToOneField("Rubric", blank=True, null=True)
    nrc = models.CharField(max_length=32)
    active = models.BooleanField(default=True)
    def __str__(self):
        return ("%s - %s")%(self.name,self.carreer_code.carreer.name)

@python_2_unicode_compatible
class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rut = models.IntegerField(null=True)
    dv = models.CharField(max_length=1)
    description = models.TextField(max_length=500, blank=True)
    personal_record = models.OneToOneField(Personal_record,null=True,blank=True)
    post_grade = models.CharField(
        max_length=2,
        choices=POST_GRADES_CHOICES,
        default=POST_GRADES_DEFAULT,
    )
    teacher_type = models.CharField(
        max_length=2,
        choices=TEACHER_TYPES_CHOICES,
        default=TEACHER_TYPES_DEFAULT,
    )
    avatar = models.ImageField(upload_to='avatar/',
                                    default='avatar/profile-pictures.png')
    cv = models.FileField(upload_to='cv/',
                                    default='cv/no-cv.pdf')

    objects = TeacherManager()

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

@python_2_unicode_compatible
class Project(models.Model):
    en_revision = 'ER'
    aceptado = 'AC'
    rechazado = 'RE'
    en_curso = 'EC'
    en_espera_de_profe = "EP"
    terminado = 'TE'
    caducado = 'CA'
    PROJECT_STATUS_CHOICES = (
        (en_revision, 'Revision'),
        (en_espera_de_profe, 'En Espera De Profesor'),
        (aceptado, 'Aceptado'),
        (rechazado, 'Rechazado'),
        (en_curso, 'En Curso'),
        (terminado, 'Terminado'),
        (caducado, 'Caducado')

    )

    name = models.CharField(max_length=70)
    date = models.DateTimeField(blank=True, null=True)
    #project_image = models.ImageField(blank=True, null=True,)
    vacancy_total = models.PositiveIntegerField(null=True)
    vacancy_available = models.PositiveIntegerField(null=True)
    section = models.ForeignKey(Section,null=True)
    description = models.TextField(max_length=500)
    author = models.ForeignKey(User, blank=True, null=True)
    guide_teacher = models.ForeignKey(Teacher, blank=True, null=True)
    period = models.ForeignKey(Period, blank=True, null=True, max_length=6)
    tags = models.ManyToManyField(Tag, blank=True)
    faculty = models.ForeignKey(Faculty, null=True)
    carreer = models.ForeignKey(Carreer, null=True)
    status = models.CharField(
        max_length=2,
        choices=PROJECT_STATUS_CHOICES,
        default=en_revision,
    )

    def __str__(self):
        return self.name

@python_2_unicode_compatible
class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    rut = models.IntegerField()
    dv = models.CharField(max_length=1)
    project = models.ForeignKey(Project, blank=True, null=True, on_delete=models.SET_NULL)
    personal_record = models.OneToOneField(Personal_record,null=True,blank=True)

    avatar = models.ImageField(upload_to='avatar/',
                                default='avatar/profile-pictures.png')
    transcript = models.FileField(upload_to='transcript/',
                                    default='transcript/no-transcript.pdf')
    pga = models.DecimalField(max_digits=5, decimal_places=1, default=0)
    def __str__(self):
        return "%s %s | %d-%s" % (self.user.first_name, self.user.last_name,
                                  self.rut, self.dv)

@python_2_unicode_compatible
class Postulation(models.Model):
    pre_project = "PR"
    post_project = "PO"

    POSTULATION_TYPE_CHOICES = ((pre_project, 'Previo'),
                                (post_project, 'Posterior')
                               )
    postulant = models.ForeignKey(Student, blank=True, null=True)
    date = models.DateTimeField(blank=True, null=True)
    project = models.ForeignKey(Project, blank=True, null=True)
    postulation_type = models.CharField(
        max_length=2,
        choices=POSTULATION_TYPE_CHOICES,
        default=post_project,
    )
    def __str__(self):
        return "%s %s | %s" % (self.postulant.user.first_name,
                               self.postulant.user.last_name,
                               self.postulation_type)


class Designation(models.Model):
    waiting_response = 'WR'
    accepted = 'AC'
    rejected = 'RE'

    DESIGNATION_CHOICES = ((waiting_response, 'Esperando respuesta'),
                           (accepted, 'Aceptada'),
                           (rejected, 'Rechazada')
                          )

    target_teacher = models.ForeignKey(Teacher, blank=True, null=True)
    project = models.ForeignKey(Project, blank=True, null=True)
    status = models.CharField(
        max_length=2,
        choices=DESIGNATION_CHOICES,
        default=waiting_response,
    )


@python_2_unicode_compatible
class Mail(models.Model):
    receipts = models.ManyToManyField(User, related_name="mail_receipts", blank=True)
    sender = models.ForeignKey(User, related_name="mail_sender", blank=True, null=True)
    message = models.TextField(max_length=500)
    date_sent = models.DateTimeField(blank=True, null=True)
    url = models.CharField(max_length=100, blank=True, null=True)
    mail_type = models.CharField(
        max_length=2,
        choices=MAIL_TYPE_CHOICES,
        default="PP",
    )
    status = models.CharField(
        max_length=2,
        choices=STATUS_MAIL_TYPE_CHOICES,
        default="AC",
    )
    objects = MailManager()

    def send_system(self, receipts, message, url=''):
        #receipts has to be a QuerySet
        self.message = message
        self.receipts = receipts
        self.date_sent = datetime.datetime.now()
        self.mail_type = "SA"
        self.status = "AC"
        self.url = url
        self.save()

    def send_p2p(self, receipts, sender):
        self.receipts = receipts
        self.sender = sender
        self.date_sent = datetime.datetime.now()
        self.mail_type = "PP"
        self.save()

    def __str__(self):
        if self.mail_type == "SA":
            return "From system"
        elif self.mail_type == "PP":
            return "From User"


@python_2_unicode_compatible
class Session(models.Model):
    rubric = models.ForeignKey(Rubric,blank=True,null=True)
    number = models.IntegerField()
    abet = models.IntegerField(default=0)

    def __str__(self):
        return self.rubric.name

class Hito(models.Model):
    session = models.OneToOneField(Session,null=True,related_name="hito")
    total_technique_percentage = models.IntegerField(default=0)
    total_presentation_percentage = models.IntegerField(default=0)
    total_memory_percentage = models.IntegerField(default=0)

class Technique(models.Model):
    hito = models.ForeignKey(Hito, null=True)
    item = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()


class Memory(models.Model):
    hito = models.ForeignKey(Hito, null=True)
    item = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()


class Topic(models.Model):
    name = models.CharField(max_length=300,blank=True,null=True)
    hito = models.ForeignKey(Hito,null=True)

class SubTopic(models.Model):
    topic = models.ForeignKey(Topic,null=True)
    name = models.CharField(max_length=32,blank=True,null=True)
    percentage = models.IntegerField()


class Week(models.Model):
    topic = models.CharField(max_length=200,null=True,blank=True)
    session = models.OneToOneField(Session,null=True,related_name='week')
    total_objetive_percentage = models.IntegerField(default=0)
    total_progress_percentage = models.IntegerField(default=0)
    total_activity_percentage = models.IntegerField(default=0)

class Evaluation(models.Model):
    name = models.CharField(max_length=300, blank=True, null=True)
    percentage = models.IntegerField()
    week = models.ForeignKey(Week, null=True)
    type_evaluation = models.CharField(max_length=2,
                                       choices=EVALUATION_TYPE_CHOICES,
                                       default="OB",
                                      )

@python_2_unicode_compatible
class Outcome(models.Model):
    abet = models.ForeignKey(Abet, null=True)
    letter = models.CharField(max_length=1)
    description = models.TextField(max_length=300)

    abc = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"]
    def set_letter(self, abet):
        for letter in self.abc:
            if Outcome.objects.filter(abet=abet).filter(letter=letter).exists() == False:
                self.letter = letter
                break

    def __str__(self):
        return "%s. %s" % (self.letter, self.description[0:64])

class Crit_info(models.Model):
    suficient = models.TextField(max_length=300)
    inprocess = models.TextField(max_length=300)
    inscipient = models.TextField(max_length=300)


class Criteria(models.Model):
    outcome = models.ForeignKey(Outcome, null=True)
    description = models.TextField(max_length=300)
    criteria_info = models.OneToOneField(Crit_info, null=True, on_delete=models.CASCADE)


class AbetRubric(models.Model):
    name = models.CharField(max_length=140)
    author = models.ForeignKey(User, null=True)
    date = models.DateTimeField(blank=True, null=True)
    outcomes = models.ManyToManyField(Outcome, blank=True)
    carreer = models.ForeignKey(Carreer, null=True, blank=True)
    nota_suf = models.IntegerField(null=True, blank=True)
    nota_enpr = models.IntegerField(null=True,blank=True)
    nota_inci = models.IntegerField(null=True,blank=True)

    def get_course(self):
        try:
          return self.course

        except:
          return None

    def set_outcomes(self, letters):
        abet = self.carreer.abet
        abet_outcomes = Outcome.objects.filter(abet=abet)

        for letter in letters:
            outcome = abet_outcomes.get(letter=letter)
            self.outcomes.add(outcome)

class StudentEvaluation(models.Model):
    student = models.ForeignKey(Student,blank=True,null=True)
    session = models.ForeignKey(Session,blank=True,null=True)
    comment_progress = models.TextField(max_length=300)
    comment_activity = models.TextField(max_length=300)
    comment_accomplishment = models.TextField(max_length=300)
    objetive_qualification = models.FloatField(default=0)
    activity_qualification = models.FloatField(default=0)
    progress_qualification = models.FloatField(default=0)
    session_qualification = models.FloatField(default=0)


class ObjetiveEvaluation(models.Model):
    student_evaluation = models.ForeignKey(StudentEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.IntegerField(null=True,blank=True)

class ActivityEvaluation(models.Model):
    student_evaluation = models.ForeignKey(StudentEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.IntegerField(null=True,blank=True)

class ProgressEvaluation(models.Model):
    student_evaluation = models.ForeignKey(StudentEvaluation, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation,null=True,blank=True)
    nota = models.IntegerField(null=True,blank=True)

class StudentHitoEvaluation(models.Model):
    student = models.ForeignKey(Student,blank=True,null=True)
    session = models.ForeignKey(Session,blank=True,null=True)
    technique_qualification = models.FloatField(default=0)
    presentation_qualification = models.FloatField(default=0)
    memory_qualification = models.FloatField(default=0)
    hito_qualification = models.FloatField(default=0)
    comment_progress = models.TextField(max_length=300)
    comment_activity = models.TextField(max_length=300)
    comment_accomplishment = models.TextField(max_length=300)

class MemoryEvaluation(models.Model):
    student_hito_evaluation = models.ForeignKey(StudentHitoEvaluation, blank=True, null=True)
    memory = models.ForeignKey(Memory, blank=True,null=True)
    nota = models.IntegerField(null=True,blank=True)

class TechniqueEvaluation(models.Model):
    student_hito_evaluation = models.ForeignKey(StudentHitoEvaluation, blank=True, null=True)
    technique = models.ForeignKey(Technique,blank=True,null=True)
    nota = models.IntegerField(null=True,blank=True)

class PresentationEvaluation(models.Model):
    student_hito_evaluation = models.ForeignKey(StudentHitoEvaluation, blank=True, null=True)
    presentation = models.ForeignKey(SubTopic,blank=True,null=True)
    nota = models.IntegerField(null=True,blank=True)
