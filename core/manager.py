from django.db import models
from django.contrib.auth.models import User
from itertools import chain

class TeacherManager(models.Manager):
    def by_type(self,type):
        return self.filter(teacher_type=type)


class ProfileManager(models.Manager):
    # def get_queryset(self):
    #     return super(FeatureManager, self).get_queryset().filter(
    #         type='f')

    def is_student(self) :
        return self.all()

class MailManager(models.Manager):
    def get_sent_mail(self,user):
        return self.filter(sender=user)

    def get_received_mail(self, user):
        return self.filter(receipts=user).filter(mail_type="PP")

    def get_system_alerts(self, user):
        return self.filter(receipts=user).filter(mail_type="SA")
