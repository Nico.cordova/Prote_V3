from models import Teacher
from autofixture import generators, register, AutoFixture

class TeacherAutoFixture(AutoFixture):
    field_values = {
        'teacher_type': generators.StaticGenerator('DO'),
        'post_grade': generators.StaticGenerator('LI'),
    }

register(Teacher, TeacherAutoFixture)