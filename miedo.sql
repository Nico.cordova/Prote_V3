-- MySQL dump 10.13  Distrib 5.7.20, for Linux (i686)
--
-- Host: localhost    Database: prote
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add user',2,'add_user'),(5,'Can change user',2,'change_user'),(6,'Can delete user',2,'delete_user'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add permission',4,'add_permission'),(11,'Can change permission',4,'change_permission'),(12,'Can delete permission',4,'delete_permission'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add attachment',7,'add_attachment'),(20,'Can change attachment',7,'change_attachment'),(21,'Can delete attachment',7,'delete_attachment'),(22,'Can add kv store',8,'add_kvstore'),(23,'Can change kv store',8,'change_kvstore'),(24,'Can delete kv store',8,'delete_kvstore'),(25,'Can add sidebar',9,'add_sidebar'),(26,'Can change sidebar',9,'change_sidebar'),(27,'Can delete sidebar',9,'delete_sidebar'),(28,'Can add shift',10,'add_shift'),(29,'Can change shift',10,'change_shift'),(30,'Can delete shift',10,'delete_shift'),(31,'Can add student evaluation',11,'add_studentevaluation'),(32,'Can change student evaluation',11,'change_studentevaluation'),(33,'Can delete student evaluation',11,'delete_studentevaluation'),(34,'Can add abet rubric',12,'add_abetrubric'),(35,'Can change abet rubric',12,'change_abetrubric'),(36,'Can delete abet rubric',12,'delete_abetrubric'),(37,'Can add tag',13,'add_tag'),(38,'Can change tag',13,'change_tag'),(39,'Can delete tag',13,'delete_tag'),(40,'Can add retro',14,'add_retro'),(41,'Can change retro',14,'change_retro'),(42,'Can delete retro',14,'delete_retro'),(43,'Can add technique',15,'add_technique'),(44,'Can change technique',15,'change_technique'),(45,'Can delete technique',15,'delete_technique'),(46,'Can add progress evaluation',16,'add_progressevaluation'),(47,'Can change progress evaluation',16,'change_progressevaluation'),(48,'Can delete progress evaluation',16,'delete_progressevaluation'),(49,'Can add rubric',17,'add_rubric'),(50,'Can change rubric',17,'change_rubric'),(51,'Can delete rubric',17,'delete_rubric'),(52,'Can add mail',18,'add_mail'),(53,'Can change mail',18,'change_mail'),(54,'Can delete mail',18,'delete_mail'),(55,'Can add faculty',19,'add_faculty'),(56,'Can change faculty',19,'change_faculty'),(57,'Can delete faculty',19,'delete_faculty'),(58,'Can add personal_record',20,'add_personal_record'),(59,'Can change personal_record',20,'change_personal_record'),(60,'Can delete personal_record',20,'delete_personal_record'),(61,'Can add academic_record',21,'add_academic_record'),(62,'Can change academic_record',21,'change_academic_record'),(63,'Can delete academic_record',21,'delete_academic_record'),(64,'Can add section',22,'add_section'),(65,'Can change section',22,'change_section'),(66,'Can delete section',22,'delete_section'),(67,'Can add hito',23,'add_hito'),(68,'Can change hito',23,'change_hito'),(69,'Can delete hito',23,'delete_hito'),(70,'Can add abet',24,'add_abet'),(71,'Can change abet',24,'change_abet'),(72,'Can delete abet',24,'delete_abet'),(73,'Can add carreer_code',25,'add_carreer_code'),(74,'Can change carreer_code',25,'change_carreer_code'),(75,'Can delete carreer_code',25,'delete_carreer_code'),(76,'Can add memory',26,'add_memory'),(77,'Can change memory',26,'change_memory'),(78,'Can delete memory',26,'delete_memory'),(79,'Can add criteria',27,'add_criteria'),(80,'Can change criteria',27,'change_criteria'),(81,'Can delete criteria',27,'delete_criteria'),(82,'Can add objetive evaluation',28,'add_objetiveevaluation'),(83,'Can change objetive evaluation',28,'change_objetiveevaluation'),(84,'Can delete objetive evaluation',28,'delete_objetiveevaluation'),(85,'Can add memory evaluation',29,'add_memoryevaluation'),(86,'Can change memory evaluation',29,'change_memoryevaluation'),(87,'Can delete memory evaluation',29,'delete_memoryevaluation'),(88,'Can add period',30,'add_period'),(89,'Can change period',30,'change_period'),(90,'Can delete period',30,'delete_period'),(91,'Can add project',31,'add_project'),(92,'Can change project',31,'change_project'),(93,'Can delete project',31,'delete_project'),(94,'Can add activity evaluation',32,'add_activityevaluation'),(95,'Can change activity evaluation',32,'change_activityevaluation'),(96,'Can delete activity evaluation',32,'delete_activityevaluation'),(97,'Can add session',33,'add_session'),(98,'Can change session',33,'change_session'),(99,'Can delete session',33,'delete_session'),(100,'Can add crit_info',34,'add_crit_info'),(101,'Can change crit_info',34,'change_crit_info'),(102,'Can delete crit_info',34,'delete_crit_info'),(103,'Can add teacher',35,'add_teacher'),(104,'Can change teacher',35,'change_teacher'),(105,'Can delete teacher',35,'delete_teacher'),(106,'Can add sub topic',36,'add_subtopic'),(107,'Can change sub topic',36,'change_subtopic'),(108,'Can delete sub topic',36,'delete_subtopic'),(109,'Can add presentation evaluation',37,'add_presentationevaluation'),(110,'Can change presentation evaluation',37,'change_presentationevaluation'),(111,'Can delete presentation evaluation',37,'delete_presentationevaluation'),(112,'Can add carreer',38,'add_carreer'),(113,'Can change carreer',38,'change_carreer'),(114,'Can delete carreer',38,'delete_carreer'),(115,'Can add outcome',39,'add_outcome'),(116,'Can change outcome',39,'change_outcome'),(117,'Can delete outcome',39,'delete_outcome'),(118,'Can add course',40,'add_course'),(119,'Can change course',40,'change_course'),(120,'Can delete course',40,'delete_course'),(121,'Can add postulation',41,'add_postulation'),(122,'Can change postulation',41,'change_postulation'),(123,'Can delete postulation',41,'delete_postulation'),(124,'Can add evaluation',42,'add_evaluation'),(125,'Can change evaluation',42,'change_evaluation'),(126,'Can delete evaluation',42,'delete_evaluation'),(127,'Can add campus',43,'add_campus'),(128,'Can change campus',43,'change_campus'),(129,'Can delete campus',43,'delete_campus'),(130,'Can add week',44,'add_week'),(131,'Can change week',44,'change_week'),(132,'Can delete week',44,'delete_week'),(133,'Can add seat',45,'add_seat'),(134,'Can change seat',45,'change_seat'),(135,'Can delete seat',45,'delete_seat'),(136,'Can add student hito evaluation',46,'add_studenthitoevaluation'),(137,'Can change student hito evaluation',46,'change_studenthitoevaluation'),(138,'Can delete student hito evaluation',46,'delete_studenthitoevaluation'),(139,'Can add user profile',2,'add_userprofile'),(140,'Can change user profile',2,'change_userprofile'),(141,'Can delete user profile',2,'delete_userprofile'),(142,'Can add student',47,'add_student'),(143,'Can change student',47,'change_student'),(144,'Can delete student',47,'delete_student'),(145,'Can add technique evaluation',48,'add_techniqueevaluation'),(146,'Can change technique evaluation',48,'change_techniqueevaluation'),(147,'Can delete technique evaluation',48,'delete_techniqueevaluation'),(148,'Can add topic',49,'add_topic'),(149,'Can change topic',49,'change_topic'),(150,'Can delete topic',49,'delete_topic'),(151,'Can add designation',50,'add_designation'),(152,'Can change designation',50,'change_designation'),(153,'Can delete designation',50,'delete_designation'),(154,'Can add syastdn',52,'add_syastdn'),(155,'Can change syastdn',52,'change_syastdn'),(156,'Can delete syastdn',52,'delete_syastdn'),(157,'Can add syaproa',53,'add_syaproa'),(158,'Can change syaproa',53,'change_syaproa'),(159,'Can delete syaproa',53,'delete_syaproa'),(160,'Can add syainsc',54,'add_syainsc'),(161,'Can change syainsc',54,'change_syainsc'),(162,'Can delete syainsc',54,'delete_syainsc');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$30000$d5dcNhLGsEmi$pVXDXZfWMwpcE1db9M7XFqz9W6CGwAdw8kWl+iJKdYM=','2018-01-30 16:52:55.674297',1,'nico','','','',1,1,'2018-01-29 19:58:09.648855'),(2,'pbkdf2_sha256$30000$aIfkv2xFdv1o$5r1cEUzrNr1qNZU3OMIxfWXDAP8298c0wX3aS/GU108=','2018-02-01 13:41:55.937799',0,'eduardo.quiroga','Eduardo','Quiroga Aguilera','',0,1,'2018-01-29 20:00:58.969695'),(106,'pbkdf2_sha256$30000$3vu6d112ANjL$HRHBmzSpCtodpPd8DOdxSYNvbzEqzpEnokf2cuKfrls=',NULL,0,'l.aguileravera',' LUIS I.','AGUILERA VERA','',0,1,'2018-01-30 17:29:21.961943'),(107,'pbkdf2_sha256$30000$hhqDDJe375QQ$UhHhHQIiU1hsPnrzCfS1N78CnCgvtIQubEzXRyrEDVk=',NULL,0,'rodrig.alvarez',' RODRIGO A.','ALVAREZ VIVIANO','',0,1,'2018-01-30 17:31:36.130794'),(108,'pbkdf2_sha256$30000$u63aUeLbmg3j$gdVBnuHuOazGcS2iTlIN51KXiXlfveg15ULO568uj2Q=',NULL,0,'patric.ramirez',' PATRICIO A.','RAMIREZ VILCHES','',0,1,'2018-01-30 17:35:19.495301'),(109,'pbkdf2_sha256$30000$VOqElWETMl3v$E2svCMAt9TUVRA86KA7mss796RO5/7sNgppjVZQ21BY=',NULL,0,'m.abarcatapia',' MAURICIO O.','ABARCA TAPIA','',0,1,'2018-01-30 17:35:56.832692'),(110,'pbkdf2_sha256$30000$nggbjKfZoBuA$cv43JJUEwEg53DnHpknKBOWQfzu32pN9R1S9M1/UuEU=',NULL,0,'e.cceresosorio',' ENRIQUE A.','CACERES OSORIO','',0,1,'2018-01-30 17:37:26.216104'),(111,'pbkdf2_sha256$30000$SbfXGEMHYbwx$sILJQSyk412dVySjzsZMeMNp+sj38KMmgN0UIJ1GCgQ=',NULL,0,'l.iniguezsanchez',' LUIS F.','INIGUEZ SANCHEZ','',0,1,'2018-01-30 17:38:56.332567'),(112,'pbkdf2_sha256$30000$slyDaGyd8x43$ZhlULTZB9lOw36w2zYlsaoiU/c8pxqvHZsPsqHmItHQ=',NULL,0,'v.jofremiranda',' VICTOR MANUEL','JOFRE MIRANDA','',0,1,'2018-01-30 17:41:09.797116'),(113,'pbkdf2_sha256$30000$mGit8ffQUEKT$ttIUwHIBw8I0bj1ueEjwPogn5WfrT9/6Sdvn1mTkfsQ=',NULL,0,'po.por.definir','Por','Por Definir','',0,1,'2018-01-30 17:41:09.883237'),(114,'pbkdf2_sha256$30000$mItFmqpLOcUF$Mtc2Qw/CFccTRimnk9ggm2ZhcaEhP5ODNmWue4tkaqY=',NULL,0,'ju.tapia.farias','Juan','Tapia Farias','',0,1,'2018-01-30 17:41:09.976631'),(115,'pbkdf2_sha256$30000$Jku1xOOFIEdz$pY8+TX6vqOGZvLqdJUuuzZu5y5hCNG/w/Mcw2kHwG9Y=',NULL,0,'ed.quiroga.aguilera','Eduardo','Quiroga Aguilera','',0,1,'2018-01-30 17:41:10.050866'),(116,'pbkdf2_sha256$30000$hF0Ap5zxsWFv$dLTVhSejROxQErHOvcngbtjI4B+RwluZxm+/gPWwz3U=',NULL,0,'ro.caballero.vivanco','Rodrigo','Caballero Vivanco','',0,1,'2018-01-30 17:41:10.119691'),(117,'pbkdf2_sha256$30000$ZpFTBvZKSyaX$UHcAqkL59p2scZ0Pz9DWZBIyjL+Z0FaPYSdPweuAzTE=',NULL,0,'he.puente.triantafilo','Hector','Puente Triantafilo','',0,1,'2018-01-30 17:41:10.191222'),(118,'pbkdf2_sha256$30000$CqLEb8vFvdh0$OuBY723jb8yI4O5NT9kYbhOuJ/HQAR9gzm7iBJHpXoE=',NULL,0,'vi.aranda.chacon','Vicente','Aranda Chacon','',0,1,'2018-01-30 17:41:10.278598'),(119,'pbkdf2_sha256$30000$3ceeZgo26iiD$HmP/WpEWRoMuC4mMtG9Az0kuoHI2h8eR+I1x9ChxqXE=',NULL,0,'gu.gatica.gonzalez','Gustavo','Gatica Gonzalez','',0,1,'2018-01-30 17:41:10.368817'),(120,'pbkdf2_sha256$30000$ZltaxlG0kKD7$H/Xk94R0EwFBkfk3CYJ0U1RCpiFq2nZ9ehbMScV/HfE=',NULL,0,'je.meza.jaque','Jessica','Meza Jaque','',0,1,'2018-01-30 17:41:10.454268'),(121,'pbkdf2_sha256$30000$2R8Ntov2iAP6$PJ33kaNUEDBOEv1dc7PH7VBFwwJxgqbPRlc/OQjiTVM=',NULL,0,'jo.souza.ordenes','Jose','Souza Ordenes','',0,1,'2018-01-30 17:41:10.538184'),(122,'pbkdf2_sha256$30000$52CxwBPXS8B7$eHZjZyb+VCLGOFPgbVv9BBIkh/6mA2uOUzZry3H43hc=',NULL,0,'e.arnguizsanhueza',' ENZO D.','ARANGUIZ SANHUEZA','',0,1,'2018-01-30 17:41:10.632340'),(123,'pbkdf2_sha256$30000$7McNtX6nvVIk$qwXznvn8ZozlBUL624r1v9qInd3+tXrRv1mXfBy6BO0=',NULL,0,'d.rodrguezsaavedra',' DAVIS A.','CORNEJO SAAVEDRA','',0,1,'2018-01-30 17:41:10.716186'),(124,'pbkdf2_sha256$30000$xy1ejVjDpAFT$erJXCq7dBwAYoOZpvW1cn4N1M0s+wb1oILbkTTqcISI=',NULL,0,'n.floresmuoz',' NICOLAS E.','FLORES MUNOZ','',0,1,'2018-01-30 17:41:10.801122'),(125,'pbkdf2_sha256$30000$GsZxspRSnydg$AsNUjh7cM6ARqP0fzxZWmkxjh3BQK9to1s1b/JXunko=','2018-01-30 17:59:11.341613',0,'c.fuentescorts',' CARLOS P.','FUENTES CORTES','',0,1,'2018-01-30 17:41:10.885596'),(126,'pbkdf2_sha256$30000$6XbD9hQWBmHw$bXSUS+qveXfIxZZFFCz/ZlqqLkHajYKo27TmFqSGMF0=',NULL,0,'f.galleguillosvalden',' FRANCES ALEJANDRO','GALLEGUILLOS VALDENEGRO','',0,1,'2018-01-30 17:41:10.967847'),(127,'pbkdf2_sha256$30000$Bp8x5hgYI6A3$9Kt11bzO2fHFkp+ae9cXuXqieqKuoBYxV/CLxg8MaCM=',NULL,0,'s.garayvallejos',' SEBASTIAN A.','GARAY VALLEJOS','',0,1,'2018-01-30 17:41:11.052569'),(128,'pbkdf2_sha256$30000$Jsuh65RhDdSo$0am0fpslDEyxta5+I6t/jX5FHroJpA2mXkuyBlG++/4=',NULL,0,'a.gonzalezjara',' ALEX M.','GONZALEZ JARA','',0,1,'2018-01-30 17:41:11.135781'),(129,'pbkdf2_sha256$30000$eACvNr2UjCbn$snBF8oX5Pif6a3IIYtcjg+wiweHexHEZeriptkSiLIk=',NULL,0,'c.herreratapia',' CRISTOPHER A.','HERRERA TAPIA','',0,1,'2018-01-30 17:41:11.224285'),(130,'pbkdf2_sha256$30000$wZhTzlMuOR5o$TWgJtfVj+GbtneyLIW8AAei51WG8pWu2MqPTLeWH+i0=',NULL,0,'c.mancillagonzlez',' CARLOS A.','MANCILLA GONZALEZ','',0,1,'2018-01-30 17:41:11.301765'),(131,'pbkdf2_sha256$30000$WvYOT460HlgI$AKSjTlxOXQntFrl47MMlaSl0tVUu3aCKEDOSi6HMcwE=',NULL,0,'a.mendezjimenez',' ANIBAL ESTEBAN','MENDEZ JIMENEZ','',0,1,'2018-01-30 17:41:11.371809'),(132,'pbkdf2_sha256$30000$Kpj8LZEWTJDS$Aa9zFcmJuTo5jZI8Hup5JZGQt0vOZRx3a6rcb9gqgyU=',NULL,0,'c.ordoezvargas',' CRISTIAN S.','ORDONEZ VARGAS','',0,1,'2018-01-30 17:41:11.444064'),(133,'pbkdf2_sha256$30000$bvNAc2o3SEP1$eX+OLKK5qd1lpnwgBPxQ+KC7cKA9R1GEoFSjjOHyrts=',NULL,0,'j.ovalleovalle',' JUAN I.','OVALLE OVALLE','',0,1,'2018-01-30 17:41:11.523644'),(134,'pbkdf2_sha256$30000$GymQ8dBJ9Ja1$g9m7zmBNTJfPgty+sMEn6oYjbktVlnXSpIpDuavwQYY=',NULL,0,'f.pealozamadrid',' FELIPE X.','PENALOZA MADRID','',0,1,'2018-01-30 17:41:11.598836'),(135,'pbkdf2_sha256$30000$NIJ7ur2zTW12$d6Lgih3v+aeJ/0GkhX+RJAMyvdx8YHWCxlKDh7dSAHw=',NULL,0,'d.pradosoto',' DARIO J.','PRADO SOTO','',0,1,'2018-01-30 17:41:11.671925'),(136,'pbkdf2_sha256$30000$n1pEYTe3mtT9$NYalyVK4yp0CAQfITSM21vXMjOsS3EAEvg+AF9o0/0k=',NULL,0,'c.ramrezcisternas',' CARLOS G.','RAMIREZ CISTERNAS','',0,1,'2018-01-30 17:41:11.739774'),(137,'pbkdf2_sha256$30000$1cNvrOuBoO9I$7bgwpGuKpgj57TSlIH4xTki3WLkIHtfdkr/ct07gZJQ=',NULL,0,'n.rosrojas',' NICOLAS E.','RIOS ROJAS','',0,1,'2018-01-30 17:41:11.808826'),(138,'pbkdf2_sha256$30000$iCRwlsExI3yd$tnJ3XapE6QeoEpDTM7ks6DOrAM4vPmjPicn8leSYMdo=',NULL,0,'d.urrasalinas',' DIEGO I.','URRA SALINAS','',0,1,'2018-01-30 17:41:11.878120'),(139,'pbkdf2_sha256$30000$o22PNgFPGmNB$M0bNfSgfc+dMN/BPItawVyzDGzEnRPqZCJv1Pq/AMXY=',NULL,0,'c.vsquezcaballero',' CAMILA P.','VASQUEZ CABALLERO','',0,1,'2018-01-30 17:41:11.967867'),(140,'pbkdf2_sha256$30000$RH4JdTyGT6kW$kh3F3RPBkgAx1twDYCh31t/j/tgUIPx0rFoRQLI7vZQ=',NULL,0,'v.vsqueztoledo',' VICTOR H.','VASQUEZ TOLEDO','',0,1,'2018-01-30 17:41:12.040261'),(141,'pbkdf2_sha256$30000$Dai0s6r6U48l$236JBK3+Anq+h205+IAn4ZZuO0fvGp0gvrV3U6XEvUM=',NULL,0,'d.espinozamaldonado',' DIEGO I.','ESPINOZA MALDONADO','',0,1,'2018-01-30 17:41:12.113296'),(142,'pbkdf2_sha256$30000$KXDUA4BpS69b$TFLzGazAuzaJCNaBv0XaYps3bzDP1Qf4C3eF6fzcc4g=',NULL,0,'i.montecinossolar',' IVAN N.','MONTECINOS SOLAR','',0,1,'2018-01-30 17:41:12.198156'),(143,'pbkdf2_sha256$30000$UnaLAxp6amqs$37Fm16Q/M5kXZzmq5Pz2+YuwFz2SBA0mtmNEiF1iam0=',NULL,0,'s.abarcagonzalez1',' SEBASTIAN E.','ABARCA GONZALEZ','',0,1,'2018-01-30 17:41:12.285516'),(144,'pbkdf2_sha256$30000$g13FR087Nsef$5wPZphnGIKQxiWIQDpnMi3Lahlbs1qhO+8PFxFjbN+c=',NULL,0,'fra.armijo',' FRANCISCO J.','ARMIJO LOPEZ','',0,1,'2018-01-30 17:41:12.377061'),(145,'pbkdf2_sha256$30000$ggzWs2Kijl3c$fnqLiaueKqmGz83DTq7SDJ4mq5OvMBzbLxidl1Zc3po=',NULL,0,'k.espinozacabrera',' KAROLINE A.','ESPINOZA CABRERA','',0,1,'2018-01-30 17:41:12.458264'),(146,'pbkdf2_sha256$30000$Sgoa8cGuuo3R$9Gcx++WzRIBWdh1Mq9T6l0qBYkr5EH04eIR9Ebm/kVg=',NULL,0,'m.lizamaacevedo',' MANUEL A.','LIZAMA ACEVEDO','',0,1,'2018-01-30 17:41:12.548744'),(147,'pbkdf2_sha256$30000$DAh0PvNHDTKv$IaDmVMPwhWeEKgWPZuNiXo2XbASM9I/D44m1f5tmmYk=',NULL,0,'m.maltezfigueroa',' MARIA JOSE','MALTEZ FIGUEROA','',0,1,'2018-01-30 17:41:12.636467'),(148,'pbkdf2_sha256$30000$6sDKRUErI5jd$+ROH+SALjBPZloFa3IqACHUw4FMsNonZcIhVxvxpsJI=',NULL,0,'jorges.munoz',' JORGE SEBASTIAN','MUNOZ GOMEZ','',0,1,'2018-01-30 17:41:12.726135'),(149,'pbkdf2_sha256$30000$QrOMNTGplvwK$rnplBfq0sjUDmLfWeY7mxSAMJYP05K5iO6+3riKMNGU=',NULL,0,'l.muozlobos',' LEONARDO ESTEBAN','MUNOZ LOBOS','',0,1,'2018-01-30 17:41:12.810349'),(150,'pbkdf2_sha256$30000$lz1io6XKFTkn$aMmiRp7KHfvw89HziX1JIyPj8sfHswJorlHN+JMXHPo=',NULL,0,'j.obandogaray',' JHON PAUL','OBANDO GARAY','',0,1,'2018-01-30 17:41:12.894275'),(151,'pbkdf2_sha256$30000$zNIQaoH4oIjI$gr5HaS3pXU3WqMRaRWId2yZub6voP0OUmEdrRKrjLL8=',NULL,0,'p.reyesgonzalez',' PABLO J.','REYES GONZALEZ','',0,1,'2018-01-30 17:41:12.979785'),(152,'pbkdf2_sha256$30000$wGnPEjyx2G7i$YcabvTyWmZeZ0/HqTv+4jU7T5m9RUEqQmXvWCc5bccU=',NULL,0,'S.SALGADO.SEBASTIAN',' SEBASTIAN I.','SALGADO SALGADO','',0,1,'2018-01-30 17:41:13.053665'),(153,'pbkdf2_sha256$30000$e24gueKadDBa$Jt03cKLwbDVVSR6IU5i/4VzjAW2LfRjX2fYpNv2qJT8=',NULL,0,'j.santisecheverria',' JAIME ALBERTO','SANTIS ECHEVERRIA','',0,1,'2018-01-30 17:41:13.122163'),(154,'pbkdf2_sha256$30000$Wf7Zow5XrFp7$Ab+3HStcSms0JoCIzo3UH2v3vohu1TKf0vuUYXwiZ7Y=',NULL,0,'m.sotovidal',' MARCELO ALEJANDRO','SOTO VIDAL','',0,1,'2018-01-30 17:41:13.191858'),(155,'pbkdf2_sha256$30000$rJw4qvl592Qn$6LRT9XglNnl5ETi708qRBr05nM/H4VyHvEOeQhskjJw=',NULL,0,'m.villanuevagajardo',' MAURICIO HERNAN','VILLANUEVA GAJARDO','',0,1,'2018-01-30 17:41:13.261149');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_user_perm_permission_id_1fbb5f2c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_abet`
--

DROP TABLE IF EXISTS `core_abet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_abet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) DEFAULT NULL,
  `author_id` int(11),
  `date` datetime(6),
  `carreer_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `carreer_id` (`carreer_id`),
  KEY `core_abet_author_id_9d364d53_fk_auth_user_id` (`author_id`),
  CONSTRAINT `core_abet_author_id_9d364d53_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `core_abet_carreer_id_c47a0478_fk_core_carreer_id` FOREIGN KEY (`carreer_id`) REFERENCES `core_carreer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_abet`
--

LOCK TABLES `core_abet` WRITE;
/*!40000 ALTER TABLE `core_abet` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_abet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_abetrubric`
--

DROP TABLE IF EXISTS `core_abetrubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_abetrubric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(140) NOT NULL,
  `author_id` int(11),
  `date` datetime(6),
  `nota_enpr` int(11),
  `nota_inci` int(11),
  `nota_suf` int(11),
  `carreer_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_abetrubric_author_id_872f60e5_fk_auth_user_id` (`author_id`),
  KEY `core_abetrubric_carreer_id_48cce770_fk_core_carreer_id` (`carreer_id`),
  CONSTRAINT `core_abetrubric_author_id_872f60e5_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `core_abetrubric_carreer_id_48cce770_fk_core_carreer_id` FOREIGN KEY (`carreer_id`) REFERENCES `core_carreer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_abetrubric`
--

LOCK TABLES `core_abetrubric` WRITE;
/*!40000 ALTER TABLE `core_abetrubric` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_abetrubric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_abetrubric_outcomes`
--

DROP TABLE IF EXISTS `core_abetrubric_outcomes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_abetrubric_outcomes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `abetrubric_id` int(11) NOT NULL,
  `outcome_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_abetrubric_outcomes_abetrubric_id_1f5f6eb3_uniq` (`abetrubric_id`,`outcome_id`),
  KEY `core_abetrubric_outcomes_outcome_id_88659e5b_fk_core_outcome_id` (`outcome_id`),
  CONSTRAINT `core_abetrubric_out_abetrubric_id_1726556b_fk_core_abetrubric_id` FOREIGN KEY (`abetrubric_id`) REFERENCES `core_abetrubric` (`id`),
  CONSTRAINT `core_abetrubric_outcomes_outcome_id_88659e5b_fk_core_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `core_outcome` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_abetrubric_outcomes`
--

LOCK TABLES `core_abetrubric_outcomes` WRITE;
/*!40000 ALTER TABLE `core_abetrubric_outcomes` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_abetrubric_outcomes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_academic_record`
--

DROP TABLE IF EXISTS `core_academic_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_academic_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seat_id` int(11) DEFAULT NULL,
  `campus_id` int(11),
  `faculty_id` int(11),
  `carreer_code_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_academic_record_campus_id_38f56056_fk_core_campus_id` (`campus_id`),
  KEY `core_academic_record_faculty_id_6043236a_fk_core_faculty_id` (`faculty_id`),
  KEY `core_academic_record_seat_id_fc0ed636_fk_core_seat_id` (`seat_id`),
  KEY `core_academic_r_carreer_code_id_83b2e7a5_fk_core_carreer_code_id` (`carreer_code_id`),
  CONSTRAINT `core_academic_r_carreer_code_id_83b2e7a5_fk_core_carreer_code_id` FOREIGN KEY (`carreer_code_id`) REFERENCES `core_carreer_code` (`id`),
  CONSTRAINT `core_academic_record_campus_id_38f56056_fk_core_campus_id` FOREIGN KEY (`campus_id`) REFERENCES `core_campus` (`id`),
  CONSTRAINT `core_academic_record_faculty_id_6043236a_fk_core_faculty_id` FOREIGN KEY (`faculty_id`) REFERENCES `core_faculty` (`id`),
  CONSTRAINT `core_academic_record_seat_id_fc0ed636_fk_core_seat_id` FOREIGN KEY (`seat_id`) REFERENCES `core_seat` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_academic_record`
--

LOCK TABLES `core_academic_record` WRITE;
/*!40000 ALTER TABLE `core_academic_record` DISABLE KEYS */;
INSERT INTO `core_academic_record` VALUES (5,1,1,1,15),(6,1,1,1,16),(8,1,1,1,14);
/*!40000 ALTER TABLE `core_academic_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_activityevaluation`
--

DROP TABLE IF EXISTS `core_activityevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_activityevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `student_evaluation_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_activityevalua_evaluation_id_73521ef4_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_student_evaluation_id_2bba3083_fk_core_studentevaluation_id` (`student_evaluation_id`),
  CONSTRAINT `core_activityevalua_evaluation_id_73521ef4_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_student_evaluation_id_2bba3083_fk_core_studentevaluation_id` FOREIGN KEY (`student_evaluation_id`) REFERENCES `core_studentevaluation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_activityevaluation`
--

LOCK TABLES `core_activityevaluation` WRITE;
/*!40000 ALTER TABLE `core_activityevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_activityevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_campus`
--

DROP TABLE IF EXISTS `core_campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_campus`
--

LOCK TABLES `core_campus` WRITE;
/*!40000 ALTER TABLE `core_campus` DISABLE KEYS */;
INSERT INTO `core_campus` VALUES (1,'Antonio Varas');
/*!40000 ALTER TABLE `core_campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_carreer`
--

DROP TABLE IF EXISTS `core_carreer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_carreer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_carreer`
--

LOCK TABLES `core_carreer` WRITE;
/*!40000 ALTER TABLE `core_carreer` DISABLE KEYS */;
INSERT INTO `core_carreer` VALUES (5,'Ingeniería en Computación e Informática');
/*!40000 ALTER TABLE `core_carreer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_carreer_code`
--

DROP TABLE IF EXISTS `core_carreer_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_carreer_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(45) DEFAULT NULL,
  `shift` varchar(45) DEFAULT NULL,
  `carreer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_carreer_code_carreer_id_03ebc434_fk_core_carreer_id` (`carreer_id`),
  CONSTRAINT `core_carreer_code_carreer_id_03ebc434_fk_core_carreer_id` FOREIGN KEY (`carreer_id`) REFERENCES `core_carreer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_carreer_code`
--

LOCK TABLES `core_carreer_code` WRITE;
/*!40000 ALTER TABLE `core_carreer_code` DISABLE KEYS */;
INSERT INTO `core_carreer_code` VALUES (14,'','Advance',5),(15,'UNAB11500','Diurno',5),(16,'UNAB21500','Vespertino',5);
/*!40000 ALTER TABLE `core_carreer_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_course`
--

DROP TABLE IF EXISTS `core_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nrc` varchar(32) NOT NULL,
  `code` varchar(10),
  `rubric_id` int(11),
  `subject` varchar(60) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `abet_rubric_id` int(11),
  `carreer_code_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_course_rubric_id_86099e9e_uniq` (`rubric_id`),
  UNIQUE KEY `core_course_abet_rubric_id_e1eec6dc_uniq` (`abet_rubric_id`),
  KEY `core_course_carreer_code_id_7b7cbb5a_fk_core_carreer_code_id` (`carreer_code_id`),
  CONSTRAINT `core_course_abet_rubric_id_e1eec6dc_fk_core_abetrubric_id` FOREIGN KEY (`abet_rubric_id`) REFERENCES `core_abetrubric` (`id`),
  CONSTRAINT `core_course_carreer_code_id_7b7cbb5a_fk_core_carreer_code_id` FOREIGN KEY (`carreer_code_id`) REFERENCES `core_carreer_code` (`id`),
  CONSTRAINT `core_course_rubric_id_86099e9e_fk_core_rubric_id` FOREIGN KEY (`rubric_id`) REFERENCES `core_rubric` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_course`
--

LOCK TABLES `core_course` WRITE;
/*!40000 ALTER TABLE `core_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_crit_info`
--

DROP TABLE IF EXISTS `core_crit_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_crit_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suficient` longtext NOT NULL,
  `inprocess` longtext NOT NULL,
  `inscipient` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_crit_info`
--

LOCK TABLES `core_crit_info` WRITE;
/*!40000 ALTER TABLE `core_crit_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_crit_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_criteria`
--

DROP TABLE IF EXISTS `core_criteria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_criteria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` longtext NOT NULL,
  `criteria_info_id` int(11) DEFAULT NULL,
  `outcome_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `criteria_info_id` (`criteria_info_id`),
  KEY `core_criteria_outcome_id_04b764f1_fk_core_outcome_id` (`outcome_id`),
  CONSTRAINT `core_criteria_criteria_info_id_a1b2dcc8_fk_core_crit_info_id` FOREIGN KEY (`criteria_info_id`) REFERENCES `core_crit_info` (`id`),
  CONSTRAINT `core_criteria_outcome_id_04b764f1_fk_core_outcome_id` FOREIGN KEY (`outcome_id`) REFERENCES `core_outcome` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_criteria`
--

LOCK TABLES `core_criteria` WRITE;
/*!40000 ALTER TABLE `core_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_criteria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_designation`
--

DROP TABLE IF EXISTS `core_designation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(2) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `target_teacher_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_assignment_project_id_b889ae2b_fk_core_project_id` (`project_id`),
  KEY `core_assignment_target_teacher_id_2c68dafe_fk_core_teacher_id` (`target_teacher_id`),
  CONSTRAINT `core_assignment_project_id_b889ae2b_fk_core_project_id` FOREIGN KEY (`project_id`) REFERENCES `core_project` (`id`),
  CONSTRAINT `core_assignment_target_teacher_id_2c68dafe_fk_core_teacher_id` FOREIGN KEY (`target_teacher_id`) REFERENCES `core_teacher` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_designation`
--

LOCK TABLES `core_designation` WRITE;
/*!40000 ALTER TABLE `core_designation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_designation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_evaluation`
--

DROP TABLE IF EXISTS `core_evaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_evaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `type_evaluation` varchar(2) NOT NULL,
  `week_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_evaluation_week_id_4ed77828_fk_core_week_id` (`week_id`),
  CONSTRAINT `core_evaluation_week_id_4ed77828_fk_core_week_id` FOREIGN KEY (`week_id`) REFERENCES `core_week` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_evaluation`
--

LOCK TABLES `core_evaluation` WRITE;
/*!40000 ALTER TABLE `core_evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_faculty`
--

DROP TABLE IF EXISTS `core_faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_faculty`
--

LOCK TABLES `core_faculty` WRITE;
/*!40000 ALTER TABLE `core_faculty` DISABLE KEYS */;
INSERT INTO `core_faculty` VALUES (1,'Ingenieria');
/*!40000 ALTER TABLE `core_faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_hito`
--

DROP TABLE IF EXISTS `core_hito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_hito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) DEFAULT NULL,
  `total_memory_percentage` int(11) NOT NULL,
  `total_presentation_percentage` int(11) NOT NULL,
  `total_technique_percentage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  CONSTRAINT `core_hito_session_id_0d1d89b5_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_hito`
--

LOCK TABLES `core_hito` WRITE;
/*!40000 ALTER TABLE `core_hito` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_hito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_mail`
--

DROP TABLE IF EXISTS `core_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_mail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext NOT NULL,
  `date_sent` datetime(6) DEFAULT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `url` varchar(100),
  `mail_type` varchar(2) NOT NULL,
  `status` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_mail_sender_id_57dcfba0_fk_auth_user_id` (`sender_id`),
  CONSTRAINT `core_mail_sender_id_57dcfba0_fk_auth_user_id` FOREIGN KEY (`sender_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_mail`
--

LOCK TABLES `core_mail` WRITE;
/*!40000 ALTER TABLE `core_mail` DISABLE KEYS */;
INSERT INTO `core_mail` VALUES (1,'Tu proyecto Proyecto master Supreme con el profesor guia Por Por Definir ha sido aprobado!','2018-01-30 17:58:33.935655',NULL,'\'project_propose\'','SA','AC');
/*!40000 ALTER TABLE `core_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_mail_receipts`
--

DROP TABLE IF EXISTS `core_mail_receipts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_mail_receipts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_mail_receipts_mail_id_8b6cd983_uniq` (`mail_id`,`user_id`),
  KEY `core_mail_receipts_user_id_c71cf055_fk_auth_user_id` (`user_id`),
  CONSTRAINT `core_mail_receipts_mail_id_6bfe0c50_fk_core_mail_id` FOREIGN KEY (`mail_id`) REFERENCES `core_mail` (`id`),
  CONSTRAINT `core_mail_receipts_user_id_c71cf055_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_mail_receipts`
--

LOCK TABLES `core_mail_receipts` WRITE;
/*!40000 ALTER TABLE `core_mail_receipts` DISABLE KEYS */;
INSERT INTO `core_mail_receipts` VALUES (1,1,2);
/*!40000 ALTER TABLE `core_mail_receipts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_memory`
--

DROP TABLE IF EXISTS `core_memory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_memory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `hito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_memory_hito_id_140d845b_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_memory_hito_id_140d845b_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_memory`
--

LOCK TABLES `core_memory` WRITE;
/*!40000 ALTER TABLE `core_memory` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_memory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_memoryevaluation`
--

DROP TABLE IF EXISTS `core_memoryevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_memoryevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `memory_id` int(11) DEFAULT NULL,
  `student_hito_evaluation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_memoryevaluation_memory_id_7274b4cc_fk_core_memory_id` (`memory_id`),
  KEY `D0c362693de977c13830f13ecfb4ed98` (`student_hito_evaluation_id`),
  CONSTRAINT `D0c362693de977c13830f13ecfb4ed98` FOREIGN KEY (`student_hito_evaluation_id`) REFERENCES `core_studenthitoevaluation` (`id`),
  CONSTRAINT `core_memoryevaluation_memory_id_7274b4cc_fk_core_memory_id` FOREIGN KEY (`memory_id`) REFERENCES `core_memory` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_memoryevaluation`
--

LOCK TABLES `core_memoryevaluation` WRITE;
/*!40000 ALTER TABLE `core_memoryevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_memoryevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_objetiveevaluation`
--

DROP TABLE IF EXISTS `core_objetiveevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_objetiveevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `student_evaluation_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_objetiveevalua_evaluation_id_e79a1fc6_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_student_evaluation_id_d23ae725_fk_core_studentevaluation_id` (`student_evaluation_id`),
  CONSTRAINT `core_objetiveevalua_evaluation_id_e79a1fc6_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_student_evaluation_id_d23ae725_fk_core_studentevaluation_id` FOREIGN KEY (`student_evaluation_id`) REFERENCES `core_studentevaluation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_objetiveevaluation`
--

LOCK TABLES `core_objetiveevaluation` WRITE;
/*!40000 ALTER TABLE `core_objetiveevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_objetiveevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_outcome`
--

DROP TABLE IF EXISTS `core_outcome`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_outcome` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `letter` varchar(1) NOT NULL,
  `description` longtext NOT NULL,
  `abet_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_outcome_abet_id_2b7d68c3_fk_core_abet_id` (`abet_id`),
  CONSTRAINT `core_outcome_abet_id_2b7d68c3_fk_core_abet_id` FOREIGN KEY (`abet_id`) REFERENCES `core_abet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_outcome`
--

LOCK TABLES `core_outcome` WRITE;
/*!40000 ALTER TABLE `core_outcome` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_outcome` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_period`
--

DROP TABLE IF EXISTS `core_period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_period` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(6) DEFAULT NULL,
  `user_friendly` varchar(60) NOT NULL,
  `finish` date DEFAULT NULL,
  `start` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_period`
--

LOCK TABLES `core_period` WRITE;
/*!40000 ALTER TABLE `core_period` DISABLE KEYS */;
INSERT INTO `core_period` VALUES (1,'201810','Primer Semestre 2018','2018-07-24','2018-01-30');
/*!40000 ALTER TABLE `core_period` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_personal_record`
--

DROP TABLE IF EXISTS `core_personal_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_personal_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_personal_record`
--

LOCK TABLES `core_personal_record` WRITE;
/*!40000 ALTER TABLE `core_personal_record` DISABLE KEYS */;
INSERT INTO `core_personal_record` VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12),(13),(14),(15),(16),(17),(18),(19),(20),(21),(22),(23),(24),(25),(26),(27),(28),(29),(30),(31),(32),(33),(34),(35),(36),(37),(38),(39),(40),(41),(42),(43),(44),(45);
/*!40000 ALTER TABLE `core_personal_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_personal_record_academic_record`
--

DROP TABLE IF EXISTS `core_personal_record_academic_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_personal_record_academic_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_record_id` int(11) NOT NULL,
  `academic_record_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_personal_record_academic_r_personal_record_id_d7f0e819_uniq` (`personal_record_id`,`academic_record_id`),
  KEY `core_pers_academic_record_id_78023a42_fk_core_academic_record_id` (`academic_record_id`),
  CONSTRAINT `core_pers_academic_record_id_78023a42_fk_core_academic_record_id` FOREIGN KEY (`academic_record_id`) REFERENCES `core_academic_record` (`id`),
  CONSTRAINT `core_pers_personal_record_id_db74724d_fk_core_personal_record_id` FOREIGN KEY (`personal_record_id`) REFERENCES `core_personal_record` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_personal_record_academic_record`
--

LOCK TABLES `core_personal_record_academic_record` WRITE;
/*!40000 ALTER TABLE `core_personal_record_academic_record` DISABLE KEYS */;
INSERT INTO `core_personal_record_academic_record` VALUES (1,2,6),(2,3,5),(3,4,5),(4,5,6),(5,6,5),(6,7,6),(7,8,5),(8,9,5),(9,10,5),(10,11,5),(11,12,5),(12,13,5),(13,14,5),(14,15,5),(15,16,5),(16,17,5),(17,18,5),(18,19,5),(19,20,5),(20,21,5),(21,22,5),(22,23,5),(23,24,5),(24,25,5),(25,26,5),(26,27,5),(27,28,5),(28,29,5),(29,30,5),(30,31,6),(31,32,6),(32,33,6),(33,34,6),(34,35,6),(35,36,6),(36,37,6),(37,38,6),(38,39,6),(39,40,6),(40,41,6),(41,42,6),(42,43,6),(43,44,6),(44,45,6);
/*!40000 ALTER TABLE `core_personal_record_academic_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_personal_record_course`
--

DROP TABLE IF EXISTS `core_personal_record_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_personal_record_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personal_record_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_personal_record_course_personal_record_id_e15ff75d_uniq` (`personal_record_id`,`course_id`),
  KEY `core_personal_record_course_course_id_818e4938_fk_core_course_id` (`course_id`),
  CONSTRAINT `core_pers_personal_record_id_d10d2328_fk_core_personal_record_id` FOREIGN KEY (`personal_record_id`) REFERENCES `core_personal_record` (`id`),
  CONSTRAINT `core_personal_record_course_course_id_818e4938_fk_core_course_id` FOREIGN KEY (`course_id`) REFERENCES `core_course` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_personal_record_course`
--

LOCK TABLES `core_personal_record_course` WRITE;
/*!40000 ALTER TABLE `core_personal_record_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_personal_record_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_postulation`
--

DROP TABLE IF EXISTS `core_postulation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_postulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime(6) DEFAULT NULL,
  `postulant_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `postulation_type` varchar(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_postulation_postulant_id_514d2b4e_fk_core_student_id` (`postulant_id`),
  KEY `core_postulation_project_id_6a22363d_fk_core_project_id` (`project_id`),
  CONSTRAINT `core_postulation_postulant_id_514d2b4e_fk_core_student_id` FOREIGN KEY (`postulant_id`) REFERENCES `core_student` (`id`),
  CONSTRAINT `core_postulation_project_id_6a22363d_fk_core_project_id` FOREIGN KEY (`project_id`) REFERENCES `core_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_postulation`
--

LOCK TABLES `core_postulation` WRITE;
/*!40000 ALTER TABLE `core_postulation` DISABLE KEYS */;
INSERT INTO `core_postulation` VALUES (1,'2018-01-30 17:53:52.465743',92,1,'PR'),(2,'2018-01-30 17:53:52.472324',94,1,'PR'),(3,'2018-01-30 17:53:52.484974',95,1,'PR'),(4,'2018-01-30 17:53:52.491188',96,1,'PR'),(5,'2018-02-01 14:39:20.130144',92,3,'PR'),(6,'2018-02-01 14:40:04.487645',92,4,'PR');
/*!40000 ALTER TABLE `core_postulation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_presentationevaluation`
--

DROP TABLE IF EXISTS `core_presentationevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_presentationevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `presentation_id` int(11) DEFAULT NULL,
  `student_hito_evaluation_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_presentationev_presentation_id_3f749e8a_fk_core_subtopic_id` (`presentation_id`),
  KEY `eb0fc41b06d9572cedfe28477f3c1881` (`student_hito_evaluation_id`),
  CONSTRAINT `core_presentationev_presentation_id_3f749e8a_fk_core_subtopic_id` FOREIGN KEY (`presentation_id`) REFERENCES `core_subtopic` (`id`),
  CONSTRAINT `eb0fc41b06d9572cedfe28477f3c1881` FOREIGN KEY (`student_hito_evaluation_id`) REFERENCES `core_studenthitoevaluation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_presentationevaluation`
--

LOCK TABLES `core_presentationevaluation` WRITE;
/*!40000 ALTER TABLE `core_presentationevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_presentationevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_progressevaluation`
--

DROP TABLE IF EXISTS `core_progressevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_progressevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `evaluation_id` int(11) DEFAULT NULL,
  `student_evaluation_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_progressevalua_evaluation_id_9a5bb308_fk_core_evaluation_id` (`evaluation_id`),
  KEY `core_student_evaluation_id_786d9438_fk_core_studentevaluation_id` (`student_evaluation_id`),
  CONSTRAINT `core_progressevalua_evaluation_id_9a5bb308_fk_core_evaluation_id` FOREIGN KEY (`evaluation_id`) REFERENCES `core_evaluation` (`id`),
  CONSTRAINT `core_student_evaluation_id_786d9438_fk_core_studentevaluation_id` FOREIGN KEY (`student_evaluation_id`) REFERENCES `core_studentevaluation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_progressevaluation`
--

LOCK TABLES `core_progressevaluation` WRITE;
/*!40000 ALTER TABLE `core_progressevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_progressevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_project`
--

DROP TABLE IF EXISTS `core_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) NOT NULL,
  `description` longtext NOT NULL,
  `guide_teacher_id` int(11) DEFAULT NULL,
  `status` varchar(2) NOT NULL,
  `author_id` int(11),
  `period_id` int(11) DEFAULT NULL,
  `faculty_id` int(11),
  `date` datetime(6),
  `vacancy_available` int(10) unsigned DEFAULT NULL,
  `carreer_id` int(11),
  `vacancy_total` int(10) unsigned,
  `section_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_project_guide_teacher_id_1ada4ba2_fk_core_teacher_id` (`guide_teacher_id`),
  KEY `core_project_author_id_3b8caba5_fk_auth_user_id` (`author_id`),
  KEY `core_project_faculty_id_e0fdca41_fk_core_faculty_id` (`faculty_id`),
  KEY `core_project_period_id_8a2201bb_uniq` (`period_id`),
  KEY `core_project_carreer_id_202b678e_fk_core_carreer_id` (`carreer_id`),
  KEY `core_project_section_id_50345325_fk_core_section_id` (`section_id`),
  CONSTRAINT `core_project_author_id_3b8caba5_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `core_project_carreer_id_202b678e_fk_core_carreer_id` FOREIGN KEY (`carreer_id`) REFERENCES `core_carreer` (`id`),
  CONSTRAINT `core_project_faculty_id_e0fdca41_fk_core_faculty_id` FOREIGN KEY (`faculty_id`) REFERENCES `core_faculty` (`id`),
  CONSTRAINT `core_project_guide_teacher_id_1ada4ba2_fk_core_teacher_id` FOREIGN KEY (`guide_teacher_id`) REFERENCES `core_teacher` (`id`),
  CONSTRAINT `core_project_period_id_8a2201bb_fk_core_period_id` FOREIGN KEY (`period_id`) REFERENCES `core_period` (`id`),
  CONSTRAINT `core_project_section_id_50345325_fk_core_section_id` FOREIGN KEY (`section_id`) REFERENCES `core_section` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_project`
--

LOCK TABLES `core_project` WRITE;
/*!40000 ALTER TABLE `core_project` DISABLE KEYS */;
INSERT INTO `core_project` VALUES (1,'Proyecto master Supreme','Nuestro proyecto maestro',21,'AC',2,1,1,'2018-01-30 17:53:52.442658',20,5,20,NULL),(2,'Proyecto master Supreme','123eqweqwe',20,'AC',2,1,1,'2018-01-30 17:58:11.422083',2,5,2,NULL),(3,'Proyecto master Supreme w','asdasdasdas',21,'ER',2,1,1,'2018-02-01 14:39:20.099637',1,5,1,NULL),(4,'Proyecto master Supreme w','bmnbnm j nmnuiuhnkj',22,'AC',2,1,1,'2018-02-01 14:40:04.459284',1,5,1,NULL);
/*!40000 ALTER TABLE `core_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_project_tags`
--

DROP TABLE IF EXISTS `core_project_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_project_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_project_tags_project_id_acb3f7ea_uniq` (`project_id`,`tag_id`),
  KEY `core_project_tags_tag_id_08e3de74_fk_core_tag_id` (`tag_id`),
  CONSTRAINT `core_project_tags_project_id_33f10e84_fk_core_project_id` FOREIGN KEY (`project_id`) REFERENCES `core_project` (`id`),
  CONSTRAINT `core_project_tags_tag_id_08e3de74_fk_core_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `core_tag` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_project_tags`
--

LOCK TABLES `core_project_tags` WRITE;
/*!40000 ALTER TABLE `core_project_tags` DISABLE KEYS */;
INSERT INTO `core_project_tags` VALUES (1,1,1),(2,2,2),(3,3,3),(4,4,4);
/*!40000 ALTER TABLE `core_project_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_retro`
--

DROP TABLE IF EXISTS `core_retro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_retro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` varchar(60) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_retro_project_id_35c04465_fk_core_project_id` (`project_id`),
  KEY `core_retro_student_id_b0697b8a_fk_core_student_id` (`student_id`),
  CONSTRAINT `core_retro_project_id_35c04465_fk_core_project_id` FOREIGN KEY (`project_id`) REFERENCES `core_project` (`id`),
  CONSTRAINT `core_retro_student_id_b0697b8a_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_retro`
--

LOCK TABLES `core_retro` WRITE;
/*!40000 ALTER TABLE `core_retro` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_retro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_rubric`
--

DROP TABLE IF EXISTS `core_rubric`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_rubric` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `carreer_id` int(11) DEFAULT NULL,
  `description` varchar(300),
  `status` varchar(2) NOT NULL,
  `author_id` int(11),
  `date` datetime(6),
  PRIMARY KEY (`id`),
  KEY `core_rubric_carreer_id_75674cdd_fk_core_carreer_id` (`carreer_id`),
  KEY `core_rubric_author_id_bc332b9a_fk_auth_user_id` (`author_id`),
  CONSTRAINT `core_rubric_author_id_bc332b9a_fk_auth_user_id` FOREIGN KEY (`author_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `core_rubric_carreer_id_75674cdd_fk_core_carreer_id` FOREIGN KEY (`carreer_id`) REFERENCES `core_carreer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_rubric`
--

LOCK TABLES `core_rubric` WRITE;
/*!40000 ALTER TABLE `core_rubric` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_rubric` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_seat`
--

DROP TABLE IF EXISTS `core_seat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_seat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_seat`
--

LOCK TABLES `core_seat` WRITE;
/*!40000 ALTER TABLE `core_seat` DISABLE KEYS */;
INSERT INTO `core_seat` VALUES (1,'Santiago');
/*!40000 ALTER TABLE `core_seat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_section`
--

DROP TABLE IF EXISTS `core_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n_nrc` varchar(32) NOT NULL,
  `course_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `code` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_section_course_id_b447b4aa_fk_core_course_id` (`course_id`),
  KEY `core_section_teacher_id_c9bc37fa_fk_core_teacher_id` (`teacher_id`),
  CONSTRAINT `core_section_course_id_b447b4aa_fk_core_course_id` FOREIGN KEY (`course_id`) REFERENCES `core_course` (`id`),
  CONSTRAINT `core_section_teacher_id_c9bc37fa_fk_core_teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `core_teacher` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_section`
--

LOCK TABLES `core_section` WRITE;
/*!40000 ALTER TABLE `core_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_session`
--

DROP TABLE IF EXISTS `core_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `rubric_id` int(11) DEFAULT NULL,
  `abet` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `core_session_rubric_id_194d6c8a_fk_core_rubric_id` (`rubric_id`),
  CONSTRAINT `core_session_rubric_id_194d6c8a_fk_core_rubric_id` FOREIGN KEY (`rubric_id`) REFERENCES `core_rubric` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_session`
--

LOCK TABLES `core_session` WRITE;
/*!40000 ALTER TABLE `core_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_shift`
--

DROP TABLE IF EXISTS `core_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_shift`
--

LOCK TABLES `core_shift` WRITE;
/*!40000 ALTER TABLE `core_shift` DISABLE KEYS */;
INSERT INTO `core_shift` VALUES (1,'Diurno'),(2,'Vespertino');
/*!40000 ALTER TABLE `core_shift` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_student`
--

DROP TABLE IF EXISTS `core_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` int(11) NOT NULL,
  `dv` varchar(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `pga` decimal(5,1) NOT NULL,
  `transcript` varchar(100) NOT NULL,
  `personal_record_id` int(11),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `personal_record_id` (`personal_record_id`),
  KEY `core_student_project_id_6ca8238b_fk_core_project_id` (`project_id`),
  CONSTRAINT `core_stud_personal_record_id_229f5d79_fk_core_personal_record_id` FOREIGN KEY (`personal_record_id`) REFERENCES `core_personal_record` (`id`),
  CONSTRAINT `core_student_project_id_6ca8238b_fk_core_project_id` FOREIGN KEY (`project_id`) REFERENCES `core_project` (`id`),
  CONSTRAINT `core_student_user_id_666ccffd_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_student`
--

LOCK TABLES `core_student` WRITE;
/*!40000 ALTER TABLE `core_student` DISABLE KEYS */;
INSERT INTO `core_student` VALUES (85,19187619,'9',106,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(86,17404741,'3',107,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(87,17319710,'1',108,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(88,10630146,'8',109,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(89,19036132,'2',110,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(90,15331180,'3',111,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',NULL),(91,18408651,'4',112,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',2),(92,19137373,'1',122,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',12),(93,19319828,'7',123,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',13),(94,19112551,'7',124,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',14),(95,18676359,'9',125,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',15),(96,18759919,'9',126,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',16),(97,19137505,'K',127,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',17),(98,19391690,'2',128,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',18),(99,19544660,'1',129,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',19),(100,18954684,'K',130,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',20),(101,18022940,'K',131,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',21),(102,18457109,'9',132,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',22),(103,19213133,'2',133,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',23),(104,18378964,'3',134,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',24),(105,19068311,'7',135,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',25),(106,19571033,'3',136,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',26),(107,19182986,'7',137,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',27),(108,18936069,'K',138,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',28),(109,19137576,'9',139,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',29),(110,18697836,'6',140,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',30),(111,19208774,'0',141,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',31),(112,18740278,'6',142,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',32),(113,17239596,'1',143,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',33),(114,19230916,'6',144,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',34),(115,18408472,'4',145,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',35),(116,17509942,'5',146,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',36),(117,17419450,'5',147,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',37),(118,17904961,'9',148,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',38),(119,18662249,'9',149,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',39),(120,22804875,'5',150,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',40),(121,16661891,'6',151,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',41),(122,17771123,'3',152,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',42),(123,18274467,'0',153,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',43),(124,17684457,'4',154,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',44),(125,17871406,'6',155,NULL,'avatar/profile-pictures.png',0.0,'transcript/no-transcript.pdf',45);
/*!40000 ALTER TABLE `core_student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_studentevaluation`
--

DROP TABLE IF EXISTS `core_studentevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_studentevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_progress` longtext NOT NULL,
  `comment_activity` longtext NOT NULL,
  `comment_accomplishment` longtext NOT NULL,
  `student_id` int(11),
  `activity_qualification` double NOT NULL,
  `objetive_qualification` double NOT NULL,
  `progress_qualification` double NOT NULL,
  `session_qualification` double NOT NULL,
  `session_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_studentevaluation_student_id_dda8d737_fk_core_student_id` (`student_id`),
  KEY `core_studentevaluation_session_id_24797739_fk_core_session_id` (`session_id`),
  CONSTRAINT `core_studentevaluation_session_id_24797739_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`),
  CONSTRAINT `core_studentevaluation_student_id_dda8d737_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_studentevaluation`
--

LOCK TABLES `core_studentevaluation` WRITE;
/*!40000 ALTER TABLE `core_studentevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_studentevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_studenthitoevaluation`
--

DROP TABLE IF EXISTS `core_studenthitoevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_studenthitoevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `technique_qualification` double NOT NULL,
  `presentation_qualification` double NOT NULL,
  `memory_qualification` double NOT NULL,
  `hito_qualification` double NOT NULL,
  `comment_progress` longtext NOT NULL,
  `comment_activity` longtext NOT NULL,
  `comment_accomplishment` longtext NOT NULL,
  `session_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_studenthitoevaluatio_session_id_cde838f9_fk_core_session_id` (`session_id`),
  KEY `core_studenthitoevaluatio_student_id_06593249_fk_core_student_id` (`student_id`),
  CONSTRAINT `core_studenthitoevaluatio_session_id_cde838f9_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`),
  CONSTRAINT `core_studenthitoevaluatio_student_id_06593249_fk_core_student_id` FOREIGN KEY (`student_id`) REFERENCES `core_student` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_studenthitoevaluation`
--

LOCK TABLES `core_studenthitoevaluation` WRITE;
/*!40000 ALTER TABLE `core_studenthitoevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_studenthitoevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_subtopic`
--

DROP TABLE IF EXISTS `core_subtopic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_subtopic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `topic_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_subtopic_topic_id_bd1bd532_fk_core_topic_id` (`topic_id`),
  CONSTRAINT `core_subtopic_topic_id_bd1bd532_fk_core_topic_id` FOREIGN KEY (`topic_id`) REFERENCES `core_topic` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_subtopic`
--

LOCK TABLES `core_subtopic` WRITE;
/*!40000 ALTER TABLE `core_subtopic` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_subtopic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_tag`
--

DROP TABLE IF EXISTS `core_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_tag`
--

LOCK TABLES `core_tag` WRITE;
/*!40000 ALTER TABLE `core_tag` DISABLE KEYS */;
INSERT INTO `core_tag` VALUES (1,'Supreme'),(2,'123123'),(3,'qdasd'),(4,'6575765');
/*!40000 ALTER TABLE `core_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_teacher`
--

DROP TABLE IF EXISTS `core_teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` int(11) DEFAULT NULL,
  `dv` varchar(1) NOT NULL,
  `teacher_type` varchar(2) NOT NULL,
  `description` longtext NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_grade` varchar(2) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `cv` varchar(100) NOT NULL,
  `personal_record_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `personal_record_id` (`personal_record_id`),
  CONSTRAINT `core_teac_personal_record_id_0c5748ee_fk_core_personal_record_id` FOREIGN KEY (`personal_record_id`) REFERENCES `core_personal_record` (`id`),
  CONSTRAINT `core_teacher_user_id_0d56ab99_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_teacher`
--

LOCK TABLES `core_teacher` WRITE;
/*!40000 ALTER TABLE `core_teacher` DISABLE KEYS */;
INSERT INTO `core_teacher` VALUES (1,12,'1','DC','',2,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',1),(20,88888888,'8','DO','',113,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',3),(21,13035820,'9','DO','',114,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',4),(22,15021085,'2','DO','',115,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',5),(23,16662072,'4','DO','',116,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',6),(24,7645824,'3','DO','',117,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',7),(25,6028684,'1','DO','',118,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',8),(26,14401515,'0','DO','',119,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',9),(27,7671021,'K','DO','',120,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',10),(28,6924565,'K','DO','',121,'LI','avatar/profile-pictures.png','cv/no-cv.pdf',11);
/*!40000 ALTER TABLE `core_teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_technique`
--

DROP TABLE IF EXISTS `core_technique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_technique` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item` varchar(300) DEFAULT NULL,
  `percentage` int(11) NOT NULL,
  `hito_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_technique_hito_id_7eb542e5_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_technique_hito_id_7eb542e5_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_technique`
--

LOCK TABLES `core_technique` WRITE;
/*!40000 ALTER TABLE `core_technique` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_technique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_techniqueevaluation`
--

DROP TABLE IF EXISTS `core_techniqueevaluation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_techniqueevaluation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nota` int(11) DEFAULT NULL,
  `student_hito_evaluation_id` int(11) DEFAULT NULL,
  `technique_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `core_techniqueevaluat_technique_id_b3c0676f_fk_core_technique_id` (`technique_id`),
  KEY `D25f4eb80c36f4fef16aa4c1dd42648a` (`student_hito_evaluation_id`),
  CONSTRAINT `D25f4eb80c36f4fef16aa4c1dd42648a` FOREIGN KEY (`student_hito_evaluation_id`) REFERENCES `core_studenthitoevaluation` (`id`),
  CONSTRAINT `core_techniqueevaluat_technique_id_b3c0676f_fk_core_technique_id` FOREIGN KEY (`technique_id`) REFERENCES `core_technique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_techniqueevaluation`
--

LOCK TABLES `core_techniqueevaluation` WRITE;
/*!40000 ALTER TABLE `core_techniqueevaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_techniqueevaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_topic`
--

DROP TABLE IF EXISTS `core_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `hito_id` int(11),
  PRIMARY KEY (`id`),
  KEY `core_topic_hito_id_cc4fcfe8_fk_core_hito_id` (`hito_id`),
  CONSTRAINT `core_topic_hito_id_cc4fcfe8_fk_core_hito_id` FOREIGN KEY (`hito_id`) REFERENCES `core_hito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_topic`
--

LOCK TABLES `core_topic` WRITE;
/*!40000 ALTER TABLE `core_topic` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `core_week`
--

DROP TABLE IF EXISTS `core_week`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_week` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11),
  `topic` varchar(200) DEFAULT NULL,
  `total_activity_percentage` int(11) NOT NULL,
  `total_objetive_percentage` int(11) NOT NULL,
  `total_progress_percentage` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  CONSTRAINT `core_week_session_id_658f7705_fk_core_session_id` FOREIGN KEY (`session_id`) REFERENCES `core_session` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_week`
--

LOCK TABLES `core_week` WRITE;
/*!40000 ALTER TABLE `core_week` DISABLE KEYS */;
/*!40000 ALTER TABLE `core_week` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_sidebar`
--

DROP TABLE IF EXISTS `dashboard_sidebar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboard_sidebar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `href` varchar(144) NOT NULL,
  `access` varchar(1) NOT NULL,
  `lft` int(10) unsigned NOT NULL,
  `rght` int(10) unsigned NOT NULL,
  `tree_id` int(10) unsigned NOT NULL,
  `level` int(10) unsigned NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `dashboard_sidebar_caf7cc51` (`lft`),
  KEY `dashboard_sidebar_3cfbd988` (`rght`),
  KEY `dashboard_sidebar_656442a0` (`tree_id`),
  KEY `dashboard_sidebar_c9e9a848` (`level`),
  KEY `dashboard_sidebar_6be37982` (`parent_id`),
  CONSTRAINT `dashboard_sidebar_parent_id_967105b5_fk_dashboard_sidebar_id` FOREIGN KEY (`parent_id`) REFERENCES `dashboard_sidebar` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_sidebar`
--

LOCK TABLES `dashboard_sidebar` WRITE;
/*!40000 ALTER TABLE `dashboard_sidebar` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_sidebar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2018-01-29 21:17:00.272470','3','Ingenieria en Computacio e Informatica',3,'',38,1),(2,'2018-01-29 21:17:00.276310','2','Ingenier\\u00eda en Computaci\\u00f3n e Inform\\u00e1tica',3,'',38,1),(3,'2018-01-29 21:17:00.281229','1','Ingenieria en Computacion e Informatica',3,'',38,1),(4,'2018-01-30 16:57:05.646513','26','a.gonzalezjara',3,'',2,1),(5,'2018-01-30 16:57:05.649364','29','a.mendezjimenez',3,'',2,1),(6,'2018-01-30 16:57:05.654772','23','c.fuentescorts',3,'',2,1),(7,'2018-01-30 16:57:05.657798','27','c.herreratapia',3,'',2,1),(8,'2018-01-30 16:57:05.660699','28','c.mancillagonzlez',3,'',2,1),(9,'2018-01-30 16:57:05.662525','30','c.ordoezvargas',3,'',2,1),(10,'2018-01-30 16:57:05.664755','34','c.ramrezcisternas',3,'',2,1),(11,'2018-01-30 16:57:05.666801','37','c.vsquezcaballero',3,'',2,1),(12,'2018-01-30 16:57:05.669196','39','d.espinozamaldonado',3,'',2,1),(13,'2018-01-30 16:57:05.670908','33','d.pradosoto',3,'',2,1),(14,'2018-01-30 16:57:05.673480','21','d.rodrguezsaavedra',3,'',2,1),(15,'2018-01-30 16:57:05.677839','36','d.urrasalinas',3,'',2,1),(16,'2018-01-30 16:57:05.681160','20','e.arnguizsanhueza',3,'',2,1),(17,'2018-01-30 16:57:05.683032','7','e.cceresosorio',3,'',2,1),(18,'2018-01-30 16:57:05.685786','13','ed.quiroga.aguilera',3,'',2,1),(19,'2018-01-30 16:57:05.688256','24','f.galleguillosvalden',3,'',2,1),(20,'2018-01-30 16:57:05.690827','32','f.pealozamadrid',3,'',2,1),(21,'2018-01-30 16:57:05.694084','42','fra.armijo',3,'',2,1),(22,'2018-01-30 16:57:05.697354','17','gu.gatica.gonzalez',3,'',2,1),(23,'2018-01-30 16:57:05.699016','15','he.puente.triantafilo',3,'',2,1),(24,'2018-01-30 16:57:05.702416','40','i.montecinossolar',3,'',2,1),(25,'2018-01-30 16:57:05.708076','48','j.obandogaray',3,'',2,1),(26,'2018-01-30 16:57:05.711322','31','j.ovalleovalle',3,'',2,1),(27,'2018-01-30 16:57:05.713803','51','j.santisecheverria',3,'',2,1),(28,'2018-01-30 16:57:05.716326','18','je.meza.jaque',3,'',2,1),(29,'2018-01-30 16:57:05.718258','19','jo.souza.ordenes',3,'',2,1),(30,'2018-01-30 16:57:05.721038','46','jorges.munoz',3,'',2,1),(31,'2018-01-30 16:57:05.722962','12','ju.tapia.farias',3,'',2,1),(32,'2018-01-30 16:57:05.726270','43','k.espinozacabrera',3,'',2,1),(33,'2018-01-30 16:57:05.731767','3','l.aguileravera',3,'',2,1),(34,'2018-01-30 16:57:05.735974','8','l.iniguezsanchez',3,'',2,1),(35,'2018-01-30 16:57:05.738827','47','l.muozlobos',3,'',2,1),(36,'2018-01-30 16:57:05.741918','6','m.abarcatapia',3,'',2,1),(37,'2018-01-30 16:57:05.745942','44','m.lizamaacevedo',3,'',2,1),(38,'2018-01-30 16:57:05.748907','45','m.maltezfigueroa',3,'',2,1),(39,'2018-01-30 16:57:05.750689','52','m.sotovidal',3,'',2,1),(40,'2018-01-30 16:57:05.752956','53','m.villanuevagajardo',3,'',2,1),(41,'2018-01-30 16:57:05.754563','22','n.floresmuoz',3,'',2,1),(42,'2018-01-30 16:57:05.758793','35','n.rosrojas',3,'',2,1),(43,'2018-01-30 16:57:05.764265','49','p.reyesgonzalez',3,'',2,1),(44,'2018-01-30 16:57:05.766874','5','patric.ramirez',3,'',2,1),(45,'2018-01-30 16:57:05.769760','10','po.por.definir',3,'',2,1),(46,'2018-01-30 16:57:05.773408','14','ro.caballero.vivanco',3,'',2,1),(47,'2018-01-30 16:57:05.778458','4','rodrig.alvarez',3,'',2,1),(48,'2018-01-30 16:57:05.782278','41','s.abarcagonzalez1',3,'',2,1),(49,'2018-01-30 16:57:05.785113','25','s.garayvallejos',3,'',2,1),(50,'2018-01-30 16:57:05.786900','50','S.SALGADO.SEBASTIAN',3,'',2,1),(51,'2018-01-30 16:57:05.789605','9','v.jofremiranda',3,'',2,1),(52,'2018-01-30 16:57:05.797096','38','v.vsqueztoledo',3,'',2,1),(53,'2018-01-30 16:57:05.800559','16','vi.aranda.chacon',3,'',2,1),(54,'2018-01-30 17:02:34.596942','1','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Diurno',1,'[{\"added\": {}}]',21,1),(55,'2018-01-30 17:16:49.026351','2','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Diurno',1,'[{\"added\": {}}]',21,1),(56,'2018-01-30 17:16:55.230557','1','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Diurno',3,'',21,1),(57,'2018-01-30 17:17:02.813222','3','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Vespertino',1,'[{\"added\": {}}]',21,1),(58,'2018-01-30 17:17:12.986064','4','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Advance',1,'[{\"added\": {}}]',21,1),(59,'2018-01-30 17:26:40.014736','76','a.gonzalezjara',3,'',2,1),(60,'2018-01-30 17:26:40.017944','79','a.mendezjimenez',3,'',2,1),(61,'2018-01-30 17:26:40.020996','73','c.fuentescorts',3,'',2,1),(62,'2018-01-30 17:26:40.025145','77','c.herreratapia',3,'',2,1),(63,'2018-01-30 17:26:40.026927','78','c.mancillagonzlez',3,'',2,1),(64,'2018-01-30 17:26:40.029203','80','c.ordoezvargas',3,'',2,1),(65,'2018-01-30 17:26:40.030773','84','c.ramrezcisternas',3,'',2,1),(66,'2018-01-30 17:26:40.034247','87','c.vsquezcaballero',3,'',2,1),(67,'2018-01-30 17:26:40.037222','89','d.espinozamaldonado',3,'',2,1),(68,'2018-01-30 17:26:40.038991','83','d.pradosoto',3,'',2,1),(69,'2018-01-30 17:26:40.041230','71','d.rodrguezsaavedra',3,'',2,1),(70,'2018-01-30 17:26:40.042972','86','d.urrasalinas',3,'',2,1),(71,'2018-01-30 17:26:40.045295','70','e.arnguizsanhueza',3,'',2,1),(72,'2018-01-30 17:26:40.046804','58','e.cceresosorio',3,'',2,1),(73,'2018-01-30 17:26:40.051735','63','ed.quiroga.aguilera',3,'',2,1),(74,'2018-01-30 17:26:40.055983','74','f.galleguillosvalden',3,'',2,1),(75,'2018-01-30 17:26:40.058750','82','f.pealozamadrid',3,'',2,1),(76,'2018-01-30 17:26:40.061455','92','fra.armijo',3,'',2,1),(77,'2018-01-30 17:26:40.062966','67','gu.gatica.gonzalez',3,'',2,1),(78,'2018-01-30 17:26:40.065122','65','he.puente.triantafilo',3,'',2,1),(79,'2018-01-30 17:26:40.067365','90','i.montecinossolar',3,'',2,1),(80,'2018-01-30 17:26:40.071464','98','j.obandogaray',3,'',2,1),(81,'2018-01-30 17:26:40.077203','81','j.ovalleovalle',3,'',2,1),(82,'2018-01-30 17:26:40.081280','101','j.santisecheverria',3,'',2,1),(83,'2018-01-30 17:26:40.084514','68','je.meza.jaque',3,'',2,1),(84,'2018-01-30 17:26:40.087492','69','jo.souza.ordenes',3,'',2,1),(85,'2018-01-30 17:26:40.090111','96','jorges.munoz',3,'',2,1),(86,'2018-01-30 17:26:40.092455','62','ju.tapia.farias',3,'',2,1),(87,'2018-01-30 17:26:40.094009','93','k.espinozacabrera',3,'',2,1),(88,'2018-01-30 17:26:40.097034','54','l.aguileravera',3,'',2,1),(89,'2018-01-30 17:26:40.100652','59','l.iniguezsanchez',3,'',2,1),(90,'2018-01-30 17:26:40.104807','97','l.muozlobos',3,'',2,1),(91,'2018-01-30 17:26:40.109409','57','m.abarcatapia',3,'',2,1),(92,'2018-01-30 17:26:40.111074','94','m.lizamaacevedo',3,'',2,1),(93,'2018-01-30 17:26:40.113707','95','m.maltezfigueroa',3,'',2,1),(94,'2018-01-30 17:26:40.117286','102','m.sotovidal',3,'',2,1),(95,'2018-01-30 17:26:40.121580','103','m.villanuevagajardo',3,'',2,1),(96,'2018-01-30 17:26:40.125203','72','n.floresmuoz',3,'',2,1),(97,'2018-01-30 17:26:40.126925','85','n.rosrojas',3,'',2,1),(98,'2018-01-30 17:26:40.129155','99','p.reyesgonzalez',3,'',2,1),(99,'2018-01-30 17:26:40.130736','56','patric.ramirez',3,'',2,1),(100,'2018-01-30 17:26:40.134238','61','po.por.definir',3,'',2,1),(101,'2018-01-30 17:26:40.139143','64','ro.caballero.vivanco',3,'',2,1),(102,'2018-01-30 17:26:40.141890','55','rodrig.alvarez',3,'',2,1),(103,'2018-01-30 17:26:40.143551','91','s.abarcagonzalez1',3,'',2,1),(104,'2018-01-30 17:26:40.145811','75','s.garayvallejos',3,'',2,1),(105,'2018-01-30 17:26:40.148745','100','S.SALGADO.SEBASTIAN',3,'',2,1),(106,'2018-01-30 17:26:40.150951','60','v.jofremiranda',3,'',2,1),(107,'2018-01-30 17:26:40.155878','88','v.vsqueztoledo',3,'',2,1),(108,'2018-01-30 17:26:40.160080','66','vi.aranda.chacon',3,'',2,1),(109,'2018-01-30 17:28:34.863809','12','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(110,'2018-01-30 17:28:34.867056','11','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(111,'2018-01-30 17:28:34.869056','10','Ingeniería en Computación e Informática - Advance',3,'',25,1),(112,'2018-01-30 17:28:34.870926','9','Ingeniería en Computación e Informática - Advance',3,'',25,1),(113,'2018-01-30 17:28:34.873501','8','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(114,'2018-01-30 17:28:34.877956','7','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(115,'2018-01-30 17:28:34.880247','6','Ingeniería en Computación e Informática - Advance',3,'',25,1),(116,'2018-01-30 17:28:34.883213','5','Ingeniería en Computación e Informática - Advance',3,'',25,1),(117,'2018-01-30 17:28:34.887688','4','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(118,'2018-01-30 17:28:34.890821','3','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(119,'2018-01-30 17:28:34.896501','2','Ingeniería en Computación e Informática - Advance',3,'',25,1),(120,'2018-01-30 17:28:34.900420','1','Ingeniería en Computación e Informática - Advance',3,'',25,1),(121,'2018-01-30 17:28:47.825748','4','Ingeniería en Computación e Informática',3,'',38,1),(122,'2018-01-30 17:28:58.902951','104','l.aguileravera',3,'',2,1),(123,'2018-01-30 17:28:58.909347','105','rodrig.alvarez',3,'',2,1),(124,'2018-01-30 17:30:05.317025','5','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Diurno',1,'[{\"added\": {}}]',21,1),(125,'2018-01-30 17:30:17.609353','6','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Vespertino',1,'[{\"added\": {}}]',21,1),(126,'2018-01-30 17:30:47.583304','7','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Advance',1,'[{\"added\": {}}]',21,1),(127,'2018-01-30 17:31:15.281494','13','Ingeniería en Computación e Informática - Advance',3,'',25,1),(128,'2018-01-30 17:31:27.003941','8','Ingenieria-Santiago-Antonio Varas-Ingeniería en Computación e Informática-Advance',1,'[{\"added\": {}}]',21,1),(129,'2018-01-30 17:32:15.501499','20','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(130,'2018-01-30 17:32:15.504435','19','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(131,'2018-01-30 17:32:15.507261','18','Ingeniería en Computación e Informática - Advance',3,'',25,1),(132,'2018-01-30 17:32:15.512114','17','Ingeniería en Computación e Informática - Advance',3,'',25,1),(133,'2018-01-30 17:35:52.022491','22','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(134,'2018-01-30 17:38:08.401831','26','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(135,'2018-01-30 17:38:08.405650','25','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(136,'2018-01-30 17:38:08.408231','24','Ingeniería en Computación e Informática - Vespertino',3,'',25,1),(137,'2018-01-30 17:38:08.409876','23','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(138,'2018-01-30 17:38:08.414183','21','Ingeniería en Computación e Informática - Diurno',3,'',25,1),(139,'2018-01-30 17:42:21.101504','2','Vespertino',1,'[{\"added\": {}}]',10,1),(140,'2018-01-30 17:52:58.538022','1','Primer Semestre 2018',1,'[{\"added\": {}}]',30,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(4,'auth','permission'),(2,'auth','user'),(5,'contenttypes','contenttype'),(24,'core','abet'),(12,'core','abetrubric'),(21,'core','academic_record'),(32,'core','activityevaluation'),(43,'core','campus'),(38,'core','carreer'),(25,'core','carreer_code'),(40,'core','course'),(27,'core','criteria'),(34,'core','crit_info'),(50,'core','designation'),(42,'core','evaluation'),(19,'core','faculty'),(23,'core','hito'),(18,'core','mail'),(26,'core','memory'),(29,'core','memoryevaluation'),(28,'core','objetiveevaluation'),(39,'core','outcome'),(30,'core','period'),(20,'core','personal_record'),(41,'core','postulation'),(37,'core','presentationevaluation'),(16,'core','progressevaluation'),(31,'core','project'),(14,'core','retro'),(17,'core','rubric'),(45,'core','seat'),(22,'core','section'),(33,'core','session'),(10,'core','shift'),(47,'core','student'),(11,'core','studentevaluation'),(46,'core','studenthitoevaluation'),(36,'core','subtopic'),(13,'core','tag'),(35,'core','teacher'),(15,'core','technique'),(48,'core','techniqueevaluation'),(49,'core','topic'),(51,'core','userprofile'),(44,'core','week'),(9,'dashboard','sidebar'),(7,'django_summernote','attachment'),(54,'import_banner_data','syainsc'),(53,'import_banner_data','syaproa'),(52,'import_banner_data','syastdn'),(6,'sessions','session'),(8,'thumbnail','kvstore');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=161 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2018-01-29 19:52:50.136388'),(2,'auth','0001_initial','2018-01-29 19:52:50.918103'),(3,'admin','0001_initial','2018-01-29 19:52:51.087029'),(4,'admin','0002_logentry_remove_auto_add','2018-01-29 19:52:51.107092'),(5,'contenttypes','0002_remove_content_type_name','2018-01-29 19:52:51.223531'),(6,'auth','0002_alter_permission_name_max_length','2018-01-29 19:52:51.243169'),(7,'auth','0003_alter_user_email_max_length','2018-01-29 19:52:51.270216'),(8,'auth','0004_alter_user_username_opts','2018-01-29 19:52:51.286032'),(9,'auth','0005_alter_user_last_login_null','2018-01-29 19:52:51.345457'),(10,'auth','0006_require_contenttypes_0002','2018-01-29 19:52:51.349372'),(11,'auth','0007_alter_validators_add_error_messages','2018-01-29 19:52:51.364552'),(12,'auth','0008_alter_user_username_max_length','2018-01-29 19:52:51.394812'),(13,'core','0001_initial','2018-01-29 19:52:51.604776'),(14,'core','0002_auto_20170123_2111','2018-01-29 19:52:51.866797'),(15,'core','0003_auto_20170123_2112','2018-01-29 19:52:51.888763'),(16,'core','0004_auto_20170123_2136','2018-01-29 19:52:52.247030'),(17,'core','0005_project','2018-01-29 19:52:52.427101'),(18,'core','0006_auto_20170124_1526','2018-01-29 19:52:52.662509'),(19,'core','0007_auto_20170124_1957','2018-01-29 19:52:53.006617'),(20,'core','0008_auto_20170202_1758','2018-01-29 19:52:53.156033'),(21,'core','0009_auto_20170204_2023','2018-01-29 19:52:53.334429'),(22,'core','0010_project_author','2018-01-29 19:52:53.469508'),(23,'core','0011_project_image','2018-01-29 19:52:53.543469'),(24,'core','0012_remove_project_image','2018-01-29 19:52:53.616885'),(25,'core','0013_auto_20170206_1723','2018-01-29 19:52:54.511130'),(26,'core','0014_auto_20170206_1808','2018-01-29 19:52:54.814903'),(27,'core','0015_auto_20170206_1818','2018-01-29 19:52:55.161044'),(28,'core','0016_auto_20170206_1829','2018-01-29 19:52:56.164181'),(29,'core','0017_auto_20170207_1409','2018-01-29 19:52:56.273299'),(30,'core','0009_userprofile','2018-01-29 19:52:56.306836'),(31,'core','0018_merge_20170208_0119','2018-01-29 19:52:56.310328'),(32,'core','0019_project_date','2018-01-29 19:52:56.393724'),(33,'core','0020_auto_20170208_1221','2018-01-29 19:52:56.434235'),(34,'core','0021_auto_20170208_1539','2018-01-29 19:52:56.736529'),(35,'core','0022_teacher_post_grade','2018-01-29 19:52:56.832317'),(36,'core','0023_auto_20170209_1437','2018-01-29 19:52:56.869130'),(37,'core','0021_auto_20170208_1458','2018-01-29 19:52:57.140815'),(38,'core','0022_remove_project_international_vacancy','2018-01-29 19:52:57.225470'),(39,'core','0024_merge_20170209_1738','2018-01-29 19:52:57.232034'),(40,'core','0025_auto_20170209_1803','2018-01-29 19:52:57.477022'),(41,'core','0026_auto_20170209_1853','2018-01-29 19:52:57.796139'),(42,'core','0027_auto_20170209_1853','2018-01-29 19:52:57.989864'),(43,'core','0028_project_carreer','2018-01-29 19:52:58.167255'),(44,'core','0029_teacher_carrer','2018-01-29 19:52:58.344562'),(45,'core','0030_auto_20170210_0044','2018-01-29 19:52:58.429201'),(46,'core','0031_auto_20170210_1340','2018-01-29 19:52:58.771603'),(47,'core','0032_auto_20170210_1341','2018-01-29 19:52:58.943547'),(48,'core','0033_auto_20170210_1659','2018-01-29 19:52:59.086980'),(49,'core','0021_teacher_avatar','2018-01-29 19:52:59.176807'),(50,'core','0034_merge_20170210_1806','2018-01-29 19:52:59.180574'),(51,'core','0035_postulation','2018-01-29 19:52:59.369952'),(52,'core','0036_postulation_postulation_type','2018-01-29 19:52:59.454165'),(53,'core','0037_auto_20170215_1858','2018-01-29 19:52:59.644184'),(54,'core','0038_auto_20170215_1902','2018-01-29 19:52:59.783753'),(55,'core','0039_auto_20170215_2041','2018-01-29 19:53:00.603221'),(56,'core','0035_student_avatar','2018-01-29 19:53:00.708280'),(57,'core','0040_merge_20170215_2134','2018-01-29 19:53:00.711804'),(58,'core','0041_auto_20170216_1302','2018-01-29 19:53:00.759865'),(59,'core','0042_auto_20170216_1304','2018-01-29 19:53:00.955882'),(60,'core','0043_student_pga','2018-01-29 19:53:01.073709'),(61,'core','0044_auto_20170216_1858','2018-01-29 19:53:01.198487'),(62,'core','0040_assignment','2018-01-29 19:53:01.402438'),(63,'core','0041_auto_20170216_1520','2018-01-29 19:53:01.672076'),(64,'core','0045_merge_20170216_2114','2018-01-29 19:53:01.675625'),(65,'core','0046_mail_mailbox','2018-01-29 19:53:02.264395'),(66,'core','0047_auto_20170217_2205','2018-01-29 19:53:02.594537'),(67,'core','0048_auto_20170217_2219','2018-01-29 19:53:02.816893'),(68,'core','0049_mail_url','2018-01-29 19:53:02.909611'),(69,'core','0050_auto_20170217_2320','2018-01-29 19:53:03.784180'),(70,'core','0051_auto_20170218_0004','2018-01-29 19:53:04.128412'),(71,'core','0052_auto_20170218_2237','2018-01-29 19:53:04.174496'),(72,'core','0052_auto_20170218_0214','2018-01-29 19:53:04.219315'),(73,'core','0053_merge_20170219_0056','2018-01-29 19:53:04.223128'),(74,'core','0054_mail_status','2018-01-29 19:53:04.316062'),(75,'core','0046_auto_20170217_1542','2018-01-29 19:53:04.501776'),(76,'core','0055_merge_20170219_1959','2018-01-29 19:53:04.506035'),(77,'core','0056_auto_20170220_0120','2018-01-29 19:53:04.557151'),(78,'core','0057_auto_20170220_1814','2018-01-29 19:53:04.669866'),(79,'core','0058_teacher_transcript','2018-01-29 19:53:04.759276'),(80,'core','0059_auto_20170220_2312','2018-01-29 19:53:04.811558'),(81,'core','0060_teacher_shift','2018-01-29 19:53:04.961951'),(82,'core','0061_auto_20170221_1802','2018-01-29 19:53:05.170705'),(83,'core','0055_auto_20170219_0623','2018-01-29 19:53:05.826957'),(84,'core','0057_merge_20170221_1404','2018-01-29 19:53:05.833362'),(85,'core','0062_merge_20170222_1603','2018-01-29 19:53:05.837953'),(86,'core','0063_auto_20170223_0231','2018-01-29 19:53:06.099406'),(87,'core','0064_auto_20170223_0317','2018-01-29 19:53:06.509598'),(88,'core','0065_auto_20170223_0331','2018-01-29 19:53:06.758288'),(89,'core','0066_auto_20170223_1119','2018-01-29 19:53:07.009903'),(90,'core','0067_auto_20170225_0403','2018-01-29 19:53:07.999708'),(91,'core','0068_auto_20170226_0445','2018-01-29 19:53:08.574551'),(92,'core','0069_auto_20170226_0500','2018-01-29 19:53:08.637976'),(93,'core','0070_auto_20170226_1947','2018-01-29 19:53:09.157485'),(94,'core','0071_abet','2018-01-29 19:53:09.293901'),(95,'core','0072_subtopic_hito','2018-01-29 19:53:09.463522'),(96,'core','0062_auto_20170223_1209','2018-01-29 19:53:09.802504'),(97,'core','0063_auto_20170223_1821','2018-01-29 19:53:10.939092'),(98,'core','0064_auto_20170223_1943','2018-01-29 19:53:12.791911'),(99,'core','0065_auto_20170223_1943','2018-01-29 19:53:12.990138'),(100,'core','0066_auto_20170223_1959','2018-01-29 19:53:13.266745'),(101,'core','0067_auto_20170224_0118','2018-01-29 19:53:13.599692'),(102,'core','0073_merge_20170228_2225','2018-01-29 19:53:13.603395'),(103,'core','0074_week_topic','2018-01-29 19:53:13.740917'),(104,'core','0075_auto_20170303_1525','2018-01-29 19:53:14.070710'),(105,'core','0076_auto_20170306_1744','2018-01-29 19:53:14.350558'),(106,'core','0077_auto_20170306_2115','2018-01-29 19:53:14.590207'),(107,'core','0074_auto_20170302_2328','2018-01-29 19:53:15.870739'),(108,'core','0075_auto_20170303_1625','2018-01-29 19:53:16.681361'),(109,'core','0076_auto_20170303_1829','2018-01-29 19:53:16.937794'),(110,'core','0078_merge_20170310_1756','2018-01-29 19:53:16.944729'),(111,'core','0074_auto_20170313_1808','2018-01-29 19:53:17.296681'),(112,'core','0079_merge_20170313_2038','2018-01-29 19:53:17.301195'),(113,'core','0080_auto_20170314_1216','2018-01-29 19:53:19.118901'),(114,'core','0081_auto_20170314_1411','2018-01-29 19:53:19.665955'),(115,'core','0082_auto_20170314_1415','2018-01-29 19:53:19.949124'),(116,'core','0083_auto_20170314_1417','2018-01-29 19:53:20.121649'),(117,'core','0084_auto_20170314_1418','2018-01-29 19:53:20.294706'),(118,'core','0085_course_name','2018-01-29 19:53:20.400393'),(119,'core','0086_section_code','2018-01-29 19:53:20.504060'),(120,'core','0087_auto_20170317_1335','2018-01-29 19:53:20.663612'),(121,'core','0088_auto_20170320_1235','2018-01-29 19:53:21.724478'),(122,'core','0089_studentevaluation_student','2018-01-29 19:53:21.923596'),(123,'core','0087_auto_20170315_1857','2018-01-29 19:53:22.189474'),(124,'core','0088_course_active','2018-01-29 19:53:22.306761'),(125,'core','0090_merge_20170320_1630','2018-01-29 19:53:22.310637'),(126,'core','0079_remove_abetrubric_course','2018-01-29 19:53:22.419401'),(127,'core','0080_auto_20170315_2115','2018-01-29 19:53:22.989182'),(128,'core','0085_merge_20170316_1028','2018-01-29 19:53:22.992712'),(129,'core','0086_auto_20170316_1103','2018-01-29 19:53:23.397855'),(130,'core','0087_auto_20170318_1547','2018-01-29 19:53:23.679956'),(131,'core','0088_auto_20170320_0409','2018-01-29 19:53:24.066116'),(132,'core','0091_merge_20170321_1341','2018-01-29 19:53:24.069868'),(133,'core','0092_auto_20170321_1446','2018-01-29 19:53:25.301413'),(134,'core','0093_auto_20170321_2013','2018-01-29 19:53:25.686556'),(135,'core','0094_auto_20170322_1512','2018-01-29 19:53:25.990379'),(136,'core','0095_auto_20170322_1948','2018-01-29 19:53:26.333751'),(137,'core','0096_auto_20170323_1451','2018-01-29 19:53:26.616188'),(138,'core','0097_auto_20170323_1508','2018-01-29 19:53:26.960002'),(139,'core','0098_session_abet','2018-01-29 19:53:27.065874'),(140,'core','0099_auto_20170324_1156','2018-01-29 19:53:27.387252'),(141,'core','0100_studentevaluation_session_qualification','2018-01-29 19:53:27.489609'),(142,'core','0101_auto_20170324_1348','2018-01-29 19:53:27.762804'),(143,'core','0102_auto_20170327_1726','2018-01-29 19:53:28.760368'),(144,'core','0103_auto_20170327_2051','2018-01-29 19:53:29.098351'),(145,'core','0104_auto_20170327_2058','2018-01-29 19:53:29.377689'),(146,'core','0105_auto_20170327_2103','2018-01-29 19:53:29.589943'),(147,'core','0106_auto_20171023_1537','2018-01-29 19:53:29.669241'),(148,'core','0107_auto_20171023_1848','2018-01-29 19:53:29.858563'),(149,'core','0108_retro','2018-01-29 19:53:30.075248'),(150,'dashboard','0001_initial','2018-01-29 19:53:30.276010'),(151,'django_summernote','0001_initial','2018-01-29 19:53:30.305005'),(152,'import_banner_data','0001_initial','2018-01-29 19:53:30.426385'),(153,'import_banner_data','0002_auto_20170301_1427','2018-01-29 19:53:30.445475'),(154,'import_banner_data','0003_auto_20170301_1829','2018-01-29 19:53:30.465889'),(155,'import_banner_data','0004_auto_20170302_0349','2018-01-29 19:53:30.482412'),(156,'import_banner_data','0005_auto_20170314_1548','2018-01-29 19:53:30.630138'),(157,'import_banner_data','0006_auto_20170316_1723','2018-01-29 19:53:30.989936'),(158,'import_banner_data','0007_auto_20170328_0403','2018-01-29 19:53:31.047459'),(159,'sessions','0001_initial','2018-01-29 19:53:31.102385'),(160,'thumbnail','0001_initial','2018-01-29 19:53:31.131804');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('9bmgjdmjllozwduiplct2sg9zzkdpuhq','Njc5Mzc1NzNjN2RmNWExYjc4MmFlMGZlZTU4NmRkMzNlYjMxZGZhNDp7Im5hbWUiOiJFZHVhcmRvIFF1aXJvZ2EgQWd1aWxlcmEiLCJfYXV0aF91c2VyX2lkIjoiMiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwidG9rZW4iOiIyMjg2NWFhYjg0ZTkxYjQwZTc0OTAyODMzZTY4ZWJlMjcyNDc3ZThiIiwiX2F1dGhfdXNlcl9oYXNoIjoiZGUwOWJlZjEzYTQxODRhZTRiZmZkNGRlYTZjZGUyZjkxN2NjMmYwNyIsInR5cGUiOiJEQyIsInJ1dCI6IjEyLTEifQ==','2018-02-15 13:41:55.943804'),('jaritkv1nrizu6v3vobarmkq9xzzthwu','N2U5ZWIwZTI1M2JhMTAxZmI0NjBmYWM2ZTkwNjNiNGM5YTQ0ZWNlZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImM4ZjQzZmI4NDRhOWMyNTFkZmM3M2EyZDU1MzU5ZDZhNmQ5ODQxYzIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-02-13 16:52:55.682238'),('yd6kgdaz0z02gjnfhcnerc87udcym6jl','N2U5ZWIwZTI1M2JhMTAxZmI0NjBmYWM2ZTkwNjNiNGM5YTQ0ZWNlZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImM4ZjQzZmI4NDRhOWMyNTFkZmM3M2EyZDU1MzU5ZDZhNmQ5ODQxYzIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=','2018-02-12 21:19:14.734455');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_summernote_attachment`
--

DROP TABLE IF EXISTS `django_summernote_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_summernote_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `file` varchar(100) NOT NULL,
  `uploaded` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_summernote_attachment`
--

LOCK TABLES `django_summernote_attachment` WRITE;
/*!40000 ALTER TABLE `django_summernote_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_summernote_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_banner_data_syainsc`
--

DROP TABLE IF EXISTS `import_banner_data_syainsc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_banner_data_syainsc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n` int(11) NOT NULL,
  `campus` varchar(100) NOT NULL,
  `escuela` varchar(100) NOT NULL,
  `programa` varchar(100) NOT NULL,
  `nivel` varchar(100) NOT NULL,
  `rut` varchar(20) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `estatus` varchar(100) NOT NULL,
  `tipo_alumno` varchar(100) NOT NULL,
  `via_de_ingreso` varchar(100) NOT NULL,
  `ticket_inscripcion` int(11) NOT NULL,
  `fecha_inscripcion` datetime(6) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `fono_celular` varchar(100) NOT NULL,
  `fono_particular` varchar(100) NOT NULL,
  `matriculado` varchar(100) NOT NULL,
  `deuda_finanzas` varchar(100) NOT NULL,
  `cantidad_nrc_inscritos_inb` int(11) NOT NULL,
  `cantidad_nrc_Inscritos_web` int(11) NOT NULL,
  `total_nrc_inscritos` int(11) NOT NULL,
  `total_de_asignaturas_proyectadas_mas_probable` int(11) NOT NULL,
  `total_de_asignaturas_inscritas` int(11) NOT NULL,
  `detalle_nrc_inscritos_web_rw` varchar(1000) NOT NULL,
  `detalle_nrc_inscritos_inb_re` varchar(1000) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `id_user` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_banner_data_syainsc`
--

LOCK TABLES `import_banner_data_syainsc` WRITE;
/*!40000 ALTER TABLE `import_banner_data_syainsc` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_banner_data_syainsc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_banner_data_syaproa`
--

DROP TABLE IF EXISTS `import_banner_data_syaproa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_banner_data_syaproa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facultad` varchar(100) NOT NULL,
  `campus` varchar(100) NOT NULL,
  `unidad_academica` varchar(100) NOT NULL,
  `periodo` int(11) NOT NULL,
  `materia` varchar(100) NOT NULL,
  `curso` int(11) NOT NULL,
  `seccion` int(11) NOT NULL,
  `jornada` varchar(100) NOT NULL,
  `nrc` int(11) NOT NULL,
  `listas_cruzadas` varchar(100) NOT NULL,
  `nrc_ligados` varchar(100) NOT NULL,
  `id_listas_cruzadas` varchar(100) NOT NULL,
  `calificable` varchar(100) NOT NULL,
  `minor` varchar(100) NOT NULL,
  `rut_Profesor` varchar(20) NOT NULL,
  `nombre_profesor` varchar(100) NOT NULL,
  `nombre_asignatura` varchar(100) NOT NULL,
  `horas_a_pagar` int(11) NOT NULL,
  `tipo_actividad` varchar(100) NOT NULL,
  `modalidad` varchar(100) NOT NULL,
  `horario` varchar(100) NOT NULL,
  `vacantes` int(11) NOT NULL,
  `inscritos` int(11) NOT NULL,
  `edificio` varchar(100) NOT NULL,
  `sala` varchar(100) NOT NULL,
  `capacidad_sala` varchar(100) NOT NULL,
  `restric_cod_programa` varchar(1000) NOT NULL,
  `restric_desc_programa` varchar(1000) NOT NULL,
  `restricciones_campus` varchar(1000) NOT NULL,
  `nivel_del_curso` varchar(100) NOT NULL,
  `unidad_que_paga` varchar(100) NOT NULL,
  `semestre_que_paga` int(11) NOT NULL,
  `nivel_del_programa` varchar(100) NOT NULL,
  `fecha_inicio` datetime(6) NOT NULL,
  `fecha_fin` datetime(6) NOT NULL,
  `id_user` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_banner_data_syaproa`
--

LOCK TABLES `import_banner_data_syaproa` WRITE;
/*!40000 ALTER TABLE `import_banner_data_syaproa` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_banner_data_syaproa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_banner_data_syastdn`
--

DROP TABLE IF EXISTS `import_banner_data_syastdn`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_banner_data_syastdn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `n` int(11) NOT NULL,
  `rut` varchar(20) NOT NULL,
  `nombre_alumno` varchar(200) NOT NULL,
  `fecha_nacimiento` datetime(6) NOT NULL,
  `campus` varchar(100) NOT NULL,
  `unidad_academica` varchar(100) NOT NULL,
  `cod_programa` varchar(100) NOT NULL,
  `programa` varchar(100) NOT NULL,
  `especialidad` varchar(100) NOT NULL,
  `mencion` varchar(100) NOT NULL,
  `estado` varchar(100) NOT NULL,
  `matriculado` varchar(2) NOT NULL,
  `bloqueo_sin_matricula_bi` varchar(2) NOT NULL,
  `retencion_financiera` varchar(2) NOT NULL,
  `periodo_ultima_actualizacion` int(11) NOT NULL,
  `periodo_ctlg` int(11) NOT NULL,
  `periodo_admision` int(11) NOT NULL,
  `tipo_alumno` varchar(100) NOT NULL,
  `ppa` decimal(5,1) NOT NULL,
  `pga_programa` decimal(5,1) NOT NULL,
  `pga_historico` decimal(5,1) NOT NULL,
  `nro_asignaturas_aprobadas_programa` int(11) NOT NULL,
  `nro_creditos_aprobados_programa` int(11) NOT NULL,
  `via_titulacion` varchar(100) NOT NULL,
  `domicilio` varchar(100) NOT NULL,
  `fonos_celular` varchar(100) NOT NULL,
  `fonos_particular` varchar(100) NOT NULL,
  `correo_unab` varchar(100) NOT NULL,
  `correo_personal` varchar(100) NOT NULL,
  `id_user` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_banner_data_syastdn`
--

LOCK TABLES `import_banner_data_syastdn` WRITE;
/*!40000 ALTER TABLE `import_banner_data_syastdn` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_banner_data_syastdn` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thumbnail_kvstore`
--

DROP TABLE IF EXISTS `thumbnail_kvstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thumbnail_kvstore` (
  `key` varchar(200) NOT NULL,
  `value` longtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thumbnail_kvstore`
--

LOCK TABLES `thumbnail_kvstore` WRITE;
/*!40000 ALTER TABLE `thumbnail_kvstore` DISABLE KEYS */;
/*!40000 ALTER TABLE `thumbnail_kvstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-02-01 11:59:35
